﻿using UnityEngine;
using System.Collections;
using Achievement;
using FullSerializer;
using System.Collections.Generic;
using System;
using System.Text;

public class AchievementHelper : MonoSingleton<AchievementHelper> {
    private static readonly fsSerializer _serializer = new fsSerializer();

    AchievementManager ach = new AchievementManager();
    public AchievementManager AchievementManager {
        get { return ach; }
    }

    private static List<PropertyModel> listProperties = new List<PropertyModel>(25);
    private static List<AchievementModel> listAchievements = new List<AchievementModel>(50);

    public void InitializeWithCSV(string propertyData, string achievementData) {
        CSVReader.LoadFromString(propertyData, PropertyDataLineRead);
        CSVReader.LoadFromString(achievementData, AchievementDataLineRead);
        DoInitialize();
    }

    private void DoInitialize() {
        ach.Initialize(listProperties, listAchievements);
    }

    private void AchievementDataLineRead(int line_index, List<string> line) {
        if(line_index > 0) {
            //ID,title,description,conditions,ishidden,isactive
            var achievement = CreateAchievementModel(
                line[0],
                line[1],
                line[2],
                line[3],
                !string.IsNullOrEmpty(line[4]),
                !string.IsNullOrEmpty(line[5])
                );
            listAchievements.Add(achievement);
        }
    }

    private void PropertyDataLineRead(int line_index, List<string> line) {
        if(line_index > 0) {
            var property = CreatePropertyModel(line[0], int.Parse(line[1]));
            listProperties.Add(property);
        }
    }

    public string DumpProperties() {
        var allProperties = ach.GetAllPropertiesData();
        StringBuilder res = new StringBuilder(1000);
        //header row
        res.Append("ID,init value,current value");
        foreach(var property in allProperties) {
            res.Append('\n');
            res.Append(property.ID);
            res.Append(',');
            res.Append(property.initValue);
            res.Append(',');
            res.Append(property.currentValue);
        }
        return res.ToString();
    }

    public string DumpAchievements() {
        var allAchievements = ach.GetAllAchievementsData();
        StringBuilder res = new StringBuilder(1000);
        //header row
        res.Append("ID,title,description,conditions,ishidden,isactive,isunlocked");
        foreach (var achievement in allAchievements) {
            res.Append('\n');
            res.Append(achievement.ID);
            res.Append(',');
            res.Append(achievement.title);
            res.Append(',');
            res.Append(achievement.description);
            res.Append(',');

            //serialize conditions
            res.Append("\"[");            
            foreach(var condition in achievement.listConditions) {
                res.Append('[');
                //the target property
                res.Append("\"\"");
                res.Append(condition.propertyID);
                res.Append("\"\"");
                res.Append(',');
                //the expression string
                res.Append("\"\"");
                res.Append(condition.expressionString);
                res.Append("\"\"");
                res.Append(',');
                //the target value
                res.Append(condition.targetValue);
                res.Append(']');
                res.Append(',');
            }
            //remove last comma
            res.Remove(res.Length - 1, 1);
            res.Append("]\"");

            res.Append(',');
            //is hidden?
            if (achievement.isHidden) {
                res.Append('x');
            }
            res.Append(',');
            //is active?
            if (achievement.isActive) {
                res.Append('x');
            }
            res.Append(',');
            //is unlocked?
            if (achievement.isUnlocked) {
                res.Append('x');
            }            
        }
        return res.ToString();
    }

    public static PropertyModel CreatePropertyModel(string ID, int initValue, int currentValue = 0) {
        PropertyModel prop = new PropertyModel();
        prop.ID = ID;
        prop.initValue = initValue;
        prop.currentValue = initValue;

        return prop;
    }

    public static AchievementModel CreateAchievementModel(string ID, string title, string description, string condition, bool isActive, bool isHidden) {
        //Condition conds = Deserialize(typeof(Condition), condition) as Condition;
        fsData data = fsJsonParser.Parse(condition);
        
        List<OperatorModel> listOperators = new List<OperatorModel>(3);
        foreach(var cond in data.AsList) {
            var conditionParts = cond.AsList;
            OperatorModel om = new OperatorModel();
            om.propertyID = conditionParts[0].AsString;
            om.expressionString = conditionParts[1].AsString;
            om.targetValue = (int) conditionParts[2].AsInt64;
            listOperators.Add(om);
        }

        if (listOperators.Count > 0) {
            AchievementModel am = new AchievementModel();
            am.ID = ID;
            am.title = title;
            am.description = description;
            am.isActive = isActive;
            am.isHidden = isHidden;
            am.isUnlocked = false;
            am.listConditions = listOperators;
            return am;
        }

        return null;
    }

    public static object Deserialize(Type type, string serializedState) {
        // step 1: parse the JSON data
        fsData data = fsJsonParser.Parse(serializedState);
        //data.
        // step 2: deserialize the data
        object deserialized = null;
        _serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();

        return deserialized;
    }
}

﻿using System;
using UnityEngine;

namespace Achievement {
    /// <summary>
    /// An achievement 
    /// </summary>
    public class Achievement {
        public event Action<Achievement> OnAchievementCompleted;
        private AchievementModel m_Data;
        public AchievementModel Data {
            get { return m_Data; }
            private set { m_Data = value; }
        }
         
        /// <summary>
        /// Name of this achievement
        /// </summary>
        public string DisplayName {
            get { return Data.title; }
        }

        public string ID {
            get { return Data.ID; }
        }

        public string Description {
            get { return Data.description; }
        }
        public bool IsUnlocked {
            get { return Data.isUnlocked; }
        }

        public bool IsActive {
            get { return Data.isActive; }
        }

        public bool IsHidden {
            get { return Data.isHidden; }
        }

        private int m_OperatorCompleted = 0;
        public int NumOperatorCompleted {
            get { return m_OperatorCompleted; }
        }

        private Achievement(AchievementModel data) {
            m_Data = data;
            m_OperatorCompleted = 0;
        }

        private void OnOperatorCompleted(Operator op) {
            ++m_OperatorCompleted;
            if (NumOperatorCompleted >= Data.listConditions.Count) {
                UnlockAchievement();
            }
        }

        public void UnlockAchievement() {
            Data.isUnlocked = true;

            if(OnAchievementCompleted != null) {
                OnAchievementCompleted(this);
            }
        }

        public static Achievement CreateAchievement(AchievementModel data, AchievementManager manager) {
            Achievement ach = new Achievement(data);
            var listOperators = data.listConditions;
            Debug.Log("Creating achievement: " + data.ID + ", num condition: " + data.listConditions.Count);
            foreach(var opModel in listOperators) {
                Operator op = manager.AddOperator(opModel);
                if(op != null) {
                    op.SetOperatorCompleteCallBack(ach.OnOperatorCompleted);
                }
                else {
                    Debug.Log("Can NOT find suitable operator, creating Achievement failed for " + data.ID);
                    return null;
                }
            }

            return ach;
        }
    }
}
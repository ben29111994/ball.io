﻿/***************************************************************************\
Project:      Daily Rewards
Copyright (c) Niobium Studios.
Author:       Guilherme Nunes Barbosa (gnunesb@gmail.com)
\***************************************************************************/

using TMPro;
using UnityEngine;
using UnityEngine.UI;

/* 
 * Daily Reward Object UI representation
 */
namespace NiobiumStudios
{
    /** 
     * The UI Representation of a Daily Reward.
     * 
     *  There are 3 states:
     *  
     *  1. Unclaimed and available:
     *  - Shows the Color Claimed
     *  
     *  2. Unclaimed and Unavailable
     *  - Shows the Color Default
     *  
     *  3. Claimed
     *  - Shows the Color Claimed
     *  
     **/
    public class DailyRewardUI : MonoBehaviour
    {
        public bool showRewardName;

        [Header("UI Elements")]
        public Text textDay;                // Text containing the Day text eg. Day 12
        public TextMeshProUGUI textReward;             // The Text containing the Reward amount
        public Image imageRewardBackground; // The Reward Image Background
        public Image imageRewardCover; // The Reward Image Cover
        public Image imageReward;           // The Reward Image
        public Image checkMark;           // The CheckMark appear when claimed
        public Color colorClaimed;            // The Color of the background when claimed
        public Color colorUnclaimUnavailable;       // The Color of the background when not claimed
        public Color colorUnclaimAvailable;       // The Color of the background when claim available

        [Header("Internal")]
        public int day;

        [HideInInspector]
        public Reward reward;

        public DailyRewardState state;

        // The States a reward can have
        public enum DailyRewardState
        {
            UNCLAIMED_AVAILABLE,
            UNCLAIMED_UNAVAILABLE,
            CLAIMED
        }

        void Awake()
        {
//            colorUnclaimUnavailable = imageReward.color;
            checkMark.gameObject.SetActive(false);
        }

        public void Initialize()
        {
            textDay.text = string.Format("Day {0}", day.ToString());
            if (reward.reward > 0)
            {
                if (showRewardName)
                {
                    textReward.text = reward.reward + " " + reward.unit;
                }
                else
                {
                    textReward.text = reward.reward.ToString();
                }
            }
            else
            {
                textReward.text = reward.unit.ToString();
            }
            imageReward.sprite = reward.sprite;
        }

        // Refreshes the UI
        public void Refresh()
        {
            switch (state)
            {
                case DailyRewardState.UNCLAIMED_AVAILABLE:
                    imageRewardCover.color = colorUnclaimAvailable;
                    checkMark.gameObject.SetActive(false);
                    break;
                case DailyRewardState.UNCLAIMED_UNAVAILABLE:
                    imageRewardCover.color = colorUnclaimUnavailable;
                    checkMark.gameObject.SetActive(false);
                    break;
                case DailyRewardState.CLAIMED:
                    imageRewardCover.color = colorClaimed;
                    checkMark.gameObject.SetActive(true);
                    break;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ringPar : MonoBehaviour {

	float duration,speed;
	// Use this for initialization
	void Start () {
		speed = 15;

	}
	
	// Update is called once per frame
	void Update () {
		ring1 ();
		Invoke ("ring2", .2f);
	}

	void ring1(){
		if (gameObject.tag == "ring1") {
			Vector3 temp = transform.localScale;
			if (transform.localScale.x < 14 && transform.localScale.y < 14) {
				temp.x += Time.deltaTime * speed;
				temp.y += Time.deltaTime * speed;
			} else {
				Destroy (gameObject);
			}
			transform.localScale = temp;
		} 
	}
	void ring2(){
		if (gameObject.tag == "ring2") {
			Vector3 temp = transform.localScale;
			if (transform.localScale.x < 14 && transform.localScale.y < 14) {
				temp.x += Time.deltaTime * speed;
				temp.y += Time.deltaTime * speed;
			} else {
				Destroy (gameObject);
			}
			transform.localScale = temp;
		}
	}

}

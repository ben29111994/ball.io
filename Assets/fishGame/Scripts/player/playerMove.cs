﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;
using UnityEngine.UI;

public class playerMove : MonoBehaviour {

	public GameObject point,point2;
	public UnityArmatureComponent uac;
	Vector3 mousePosition;
	bool a;
	float x , xx  , speedUp , timeDelay;
	public float up,lowSpeed;
	public GameObject coin;
	public GameObject particleUpLv,ring1,ring2;
	public GameObject fishOne;
	public GameObject sf;
	public Image power;
	public Camera cameraMain;
	private GameObject gm;
	public Vector3 dir;
	Vector3 target_point;
	Vector3 posPlayer,posPlayerAgo;
	Vector3 stopScale , delta;
	public static playerMove instance;
	public float minimumDelta;
	public float speedMove;
	// Update is called once per frame
	void Awake(){

		instance = this;
		gm = GameObject.Find ("GM");

	}

	void Start(){

		power.fillAmount = 0;

		StartCoroutine (coinSpawn ());


	}

	void Update () {

		if (!uac.animation.isPlaying) {
			uac.animation.Play ("quayduoi");
		}
		scaleLv ();
		//outSide();


		transform.position = new Vector3 (Mathf.Clamp(transform.position.x,-44.8f,41),Mathf.Clamp(transform.position.y,-17,21.4f),transform.position.z);
	
		if(!GameManeger.instance.isDead)
		{	
			if (moveDirection.instance.isDrag) {
				dir = point.transform.position - transform.position;
				transform.Translate (dir * Time.deltaTime * speedMove);
				fishOne.transform.eulerAngles = new Vector3 (0, 0, Mathf.Atan2 ((dir.y), (dir.x)) * Mathf.Rad2Deg);
				lowSpeed = 1.8f;
			} else {
				if (lowSpeed > 0)
					lowSpeed -= Time.deltaTime;
				transform.Translate ( dir * Time.deltaTime * speedMove / 2 * lowSpeed);
			}


		//	delta.y = posPlayer.y - posPlayerAgo.y;
		//	delta.x = posPlayer.x - posPlayerAgo.x;
			//Debug.Log (delta);
		//	if (delta.sqrMagnitude != 0) 
			//fishOne.transform.eulerAngles = new Vector3 (0, 0, Mathf.Atan2 ((delta.y), (delta.x)) * Mathf.Rad2Deg);
//			Debug.DrawRay (posPlayerAgo, posPlayer, Color.green);
		

		
			if (fishOne.transform.eulerAngles.z > 110 && fishOne.transform.eulerAngles.z < 260) {
				Vector3 scale = fishOne.transform.localScale;
				if (scale.y > 0) {
					scale.y = -scale.y;
				}
				fishOne.transform.localScale = scale;
			} else{
				Vector3 scale = fishOne.transform.localScale;
				if (scale.y < 0) {
					scale.y = -scale.y;
				}
				fishOne.transform.localScale = scale;
			}
			
//			if (moveDirection.instance.swipeDelta.x < 0) {
//				Vector3 scale = transform.localScale;
//				if (scale.x > 0) {
//					scale.x = -scale.x;
//				}
//				transform.localScale = scale;
//			} 
//			else if(moveDirection.instance.swipeDelta.x > 0)   {
//				Vector3 scale = transform.localScale;
//				if (scale.x < 0) {
//					//transform.eulerAngles += new Vector3 (0, 0, 180);
//					scale.x = -scale.x;
//				}
//				transform.localScale = scale;
//			}

			

//			transform.Translate ( 2.5f * uac.timeScale*moveDirection.instance.swipeDelta.x * GameManeger.instance.moveSpeed* Time.deltaTime,
//				GameManeger.instance.moveSpeed*Time.deltaTime*	 moveDirection.instance.swipeDelta.y, 0);

		
//			if (moveDirection.instance.swipeDelta != Vector2.zero) {
//				Vector3 ve = new Vector3 (moveDirection.instance.swipeDelta.x, moveDirection.instance.swipeDelta.y, transform.position.z);

//				transform.position = Vector3.Lerp (transform.position, ve, 5 * Time.deltaTime);

//			}

				
//			uac.timeScale = Mathf.Clamp (GameManeger.instance.speed, .6f, 2.2f);
//			transform.Translate (Vector3.right * GameManeger.instance.moveSpeed  * uac.timeScale * Time.deltaTime);

//			if (GetMouseRotation.instance.isR) {
//				if (uac.timeScale < 2.5f){
//					GameManeger.instance.speed += 0.9f * Time.deltaTime;
//				}
//				mousePosition = GetMouseRotation.instance.mousePosition;
//			
				//transform.eulerAngles = new Vector3 (0, 0, Mathf.Atan2 ((moveDirection.instance.swipeDelta.y - transform.position.y), (moveDirection.instance.swipeDelta.x - transform.position.x)) * Mathf.Rad2Deg);
//				transform.eulerAngles = new Vector3 (0, 0, Mathf.Atan2 ((mousePosition.y - transform.position.y), (mousePosition.x - transform.position.x)) * Mathf.Rad2Deg);
//				Vector3 a = new Vector3 (0, 0, Mathf.Atan2 ((mousePosition.y - transform.position.y), (mousePosition.x - transform.position.x)) * Mathf.Rad2Deg);
//				stopScale = a;
//
//	
//			
//			} else {
//				if (uac.timeScale > 0.6f) {
//					GameManeger.instance.speed -= 0.9f * Time.deltaTime;
//
//				}
//			}
		}
	}




	void scaleLv(){

	
		Vector3 scale = transform.localScale;
		if (up == 1) {
			speedMove = 4;
			if (scale.x < 0.14f && scale.y < .14f) {
				scale.x += Time.deltaTime * 0.05f;
				scale.y += Time.deltaTime * 0.05f;
			}
		} else if (up == 2) {
			speedMove = 4;
			if (scale.x < 0.19f && scale.y < .19f) {
				scale.x += Time.deltaTime * 0.05f;
				scale.y += Time.deltaTime * 0.05f;
			}
		} else if (up == 3) {
			speedMove = 4;
			if (scale.x < 0.24f && scale.y < .24) {
				scale.x += Time.deltaTime * 0.05f;
				scale.y += Time.deltaTime * 0.05f;
			}
		} else if (up == 4) {
			speedMove = 4;
			if (scale.x < 0.3f && scale.y < .3f) {
				scale.x += Time.deltaTime * 0.05f;
				scale.y += Time.deltaTime * 0.05f;

			}
		} else if (up == 5) {
			speedMove = 4;
			if (scale.x < 0.35f && scale.y < .35f) {
				scale.x += Time.deltaTime * 0.05f;
				scale.y += Time.deltaTime * 0.05f;
	
			}
		}
		//transform.localScale = scale;
//		else if  (up == 6) {
//			if (scale.x < 0.55f && scale.y < .55f) {
//				scale.x += Time.deltaTime * 0.1f;
//				scale.y += Time.deltaTime * 0.1f;
//			}
//		}


		if (up == 0 && power.fillAmount == 1) {
			effect.instance.runEf = true;
			camShake.instance.isShaking = true;
			Instantiate (ring1, transform.position, Quaternion.identity);
			Instantiate (ring2, transform.position, Quaternion.identity);
			power.fillAmount = 0f;
			Instantiate (particleUpLv, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (1);
			up++;
		} else if (up == 1 && power.fillAmount == 1) {
			effect.instance.runEf = true;
			camShake.instance.isShaking = true;
			Instantiate (ring1, transform.position, Quaternion.identity);
			Instantiate (ring2, transform.position, Quaternion.identity);
			power.fillAmount = 0f;
			Instantiate (particleUpLv, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (1);
			up++;
		} else if (up == 2 && power.fillAmount == 1) {
			effect.instance.runEf = true;
			camShake.instance.isShaking = true;
			Instantiate (ring1, transform.position, Quaternion.identity);
			Instantiate (ring2, transform.position, Quaternion.identity);
			power.fillAmount = 0f;
			Instantiate (particleUpLv, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (1);
			up++;
		} else if (up == 3 && power.fillAmount == 1) {
			effect.instance.runEf = true;
			camShake.instance.isShaking = true;
			Instantiate (ring1, transform.position, Quaternion.identity);
			Instantiate (ring2, transform.position, Quaternion.identity);
			power.fillAmount = 0f;
			Instantiate (particleUpLv, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (1);
			up++;
		} else if (up == 4 && power.fillAmount == 1) {
			effect.instance.runEf = true;
			camShake.instance.isShaking = true;
			Instantiate (ring1, transform.position, Quaternion.identity);
			Instantiate (ring2, transform.position, Quaternion.identity);
			power.fillAmount = 0f;	
			Instantiate (particleUpLv, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (1);
			up++;
		}
//		}else if (up == 5 && power.fillAmount == 1) {
//			//scale.x = .7f;
//			//scale.y = .7f;
//			Instantiate (particleUpLv, transform.position, Quaternion.identity);
//			PlaySound.instance.playSd (1);
//			up++;
//		}
		transform.localScale = scale;


		if (stopScale.z > 104 || stopScale.z < -84) {
			Vector3 scalee = transform.localScale;
			if (scalee.y > 0){
				scalee.y = -scalee.y;
			}
			transform.localScale = scalee;
		} else {
			Vector3 scaleee = transform.localScale;
			if (scaleee.y < 0) {
				scaleee.y = -scaleee.y ;
			}
			transform.localScale = scaleee;
		}
	}
		

	void outSide(){

		if (transform.position.x < -31.5f) {
			if (transform.localScale.y < 0) {
				Vector3 sca = transform.localScale;
				sca.y = -sca.y;
				transform.localScale = sca;
				Vector3 temp = transform.eulerAngles;
				temp.z += 180;
				transform.eulerAngles = temp;

			}	
		} else if (transform.position.x > 34) {
			if (transform.localScale.y > 0) {
				Vector3 sca = transform.localScale;
				sca.y = -sca.y;
				transform.localScale = sca;
				Vector3 temp = transform.eulerAngles;
				temp.z += 180;
				transform.eulerAngles = temp;

			}
		} else if (transform.position.x > 37) {
			if (transform.localScale.y > 0) {
				Vector3 sca = transform.localScale;
				sca.y = -sca.y;
				transform.localScale = sca;
				Vector3 temp = transform.eulerAngles;
				temp.z += 180;
				transform.eulerAngles = temp;

			}
		} else if (transform.position.y > 18) {
			Vector3 temp = transform.eulerAngles;
			if (transform.localScale.y > 0) {
				temp.z -= 90;
			} else {
				temp.z += 90;
			}
			transform.eulerAngles = temp;
		}else if (transform.position.y < -16) {
			Vector3 temp = transform.eulerAngles;
			if (transform.localScale.y > 0) {
				temp.z += 90;
			} else {
				temp.z -= 90;
			}
			transform.eulerAngles = temp;
		}

	}



	 IEnumerator coinSpawn(){
		Instantiate (coin, transform.position + new Vector3 (Random.Range (-35, 35), Random.Range (-35, 35), 0), Quaternion.identity,gm.transform);
		yield return new WaitForSeconds ( 2 );
		StartCoroutine(coinSpawn());

			
	}




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class effect : MonoBehaviour {
	Image ima;
	public bool runEf;
	public static effect instance;
	// Use this for initialization
	void Start () {
		instance = this;
		ima = GetComponent <Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
		var image = ima.color;
		if (runEf) {
			if (image.a < 0.7f) {
				image.a += Time.deltaTime * 5f;
				ima.color = image;
			} else {
				runEf = false;

			}
				
		} else {
			if (image.a > 0) {
				image.a -= Time.deltaTime * 5f;
				ima.color = image;
			}
		}
	}
}

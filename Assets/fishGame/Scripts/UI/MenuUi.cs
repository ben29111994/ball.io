﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuUi : MonoBehaviour {

	public GameObject taptoplay,buttontaptoplay;
	public Image logo;
	float a;
	// Use this for initialization
	void Start () {
		StartCoroutine (start ());	
	}
	
	// Update is called once per frame
	void Update () {
		a += 0.009f;
		logo.color = new Color (255, 255, 255, a);

	}
	public void TapToPlay(){
		SceneManager.LoadScene ("Lv01");
	}

	IEnumerator start(){
		yield return new WaitForSeconds (1.7f);
		taptoplay.SetActive (true);
		buttontaptoplay.SetActive (true);
	}


}

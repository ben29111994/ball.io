﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManeger : MonoBehaviour {


	public static GameManeger 	instance;
	public float 				moveSpeed= 20;
	private float[]				tempTime = new float[4];
	public GameObject 			heroGO;

	private Transform 			shieldGo;
	private Transform 			SpeedingGo;
	private Transform 			invinGo;
	public bool 				isDead;
	public bool 				isShield;
	public bool 				isSpeeding;
	public bool 				isInvincible;
	public bool 				isHurt;
	private int					hp;
	public int 					mobCreate=0;
	public float 				speed;
	public float 					food;
	public int Hp {
		get {
			return hp;
		}
		set {
			hp = value;
			if(hp<=0)
			{
				isDead = true;
				heroGO.SetActive (false);
				effect.instance.runEf = true;
				StartCoroutine (wait2ActiveDeadMenu());
//				save.instance.SaveAdd (coin,"coin");

			}
			else
			{
				isHurt = true;
			}
		}
	}

	IEnumerator wait2ActiveDeadMenu()
	{
		yield return new WaitForSeconds (2);
//		UIGame.instance.menuLost.SetActive (true);
	}

//	public int Coin {
//		get {
//			return coin;
//		}
//		set {			
//			coin = value;
//			UIGame.instance.coinText.text = coin.ToString ();
//		}
//	}

	// Use this for initialization
	void Start () {
		instance	= this;	
		heroGO 		= Resources.Load ("hero") as GameObject;
		heroGO 		= Instantiate (heroGO, new Vector3(0,-6,0), Quaternion.identity);

	}
	
	// Update is called once per frame
	void Update () {







		if(isShield)
		{			
			caculateTime (ref isShield,10,ref shieldGo);
		}
		if(isSpeeding)
		{
			caculateTime (ref isSpeeding,10,ref moveSpeed,5,ref SpeedingGo);
		}
		if(isInvincible)
		{
			caculateTime (ref isInvincible,10,ref invinGo);
		}
		if(isHurt)
		{
			caculateTime (ref isInvincible,3);
		}
	}

	/// <summary>
	/// Caculates skill item  time.
	/// </summary>
	/// <param name="isState">Is state.</param>
	/// <param name="CostTime">Cost time.</param>
	/// <param name="go">Go.</param>
	void  caculateTime(ref bool isState,float CostTime,ref Transform go)
	{
		tempTime[0] += Time.deltaTime;
		if(tempTime[0]>CostTime)
		{
			tempTime[0] = 0;
			isState = false;
			go.gameObject.SetActive (false);
			go = null;
		}
	}

	void  caculateTime(ref bool isState,float CostTime)
	{
		tempTime[1] += Time.deltaTime;
		if(tempTime[1]>CostTime)
		{
			tempTime[1] = 0;
			isState = false;
		}
	}

	void  caculateTime(ref bool isState,float CostTime,ref float setData ,float oriData)
	{
		tempTime[2] += Time.deltaTime;
		if(tempTime[2]>CostTime)
		{
			tempTime[2] = 0;
			isState = false;
			setData = oriData;
		}
	}

	void  caculateTime(ref bool isState,float CostTime,ref float setData ,float oriData,ref Transform go)
	{
		tempTime[3] += Time.deltaTime;
		if(tempTime[3]>CostTime)
		{
			tempTime[3] = 0;
			isState = false;
			setData = oriData;
			go.gameObject.SetActive (false);
			go = null;
		}
	}


	public void CreateShield (string goName, Vector3 pos, Transform parent)
	{
		shieldGo = spawnObject.instance.GetCreatFx (goName, pos);
		shieldGo.parent = parent;
	}

	public void CreateSpeeding (string goName, Vector3 pos, Transform parent)
	{
		SpeedingGo = spawnObject.instance.GetCreatFx (goName, pos);
		SpeedingGo.rotation = transform.rotation;
		SpeedingGo.parent = parent;
	}

	public void CreateinvinGo (string goName, Vector3 pos, Transform parent)
	{
		invinGo = spawnObject.instance.GetCreatFx (goName, pos);
		invinGo.rotation = transform.rotation;
		invinGo.parent = parent;
	}


}

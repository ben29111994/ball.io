﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shop : MonoBehaviour {

	public static shop instance  ;

	// Use this for initialization
	void Start () {
		instance = this;
	}


	/// <summary>
	/// Checks the coin.
	/// </summary>
	/// <returns><c>true</c>, if coin was checked, <c>false</c> otherwise.</returns>
	/// <param name="cost">Cost.</param>
	public bool checkCoin(int cost)
	{
		int tempCo = save.instance.load("coin");

		if(tempCo>=cost)
		{
			tempCo -= cost;
			save.instance.Save (tempCo, "coin");
			return true;
		}
		else
		{
			return false;
		}
	}

}

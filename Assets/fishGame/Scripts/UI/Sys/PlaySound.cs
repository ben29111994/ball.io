﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour {

	public static PlaySound instance;

	AudioSource audioComp;

	public AudioClip[]	soundClip;


	// Use this for initialization
	void Start () {

		instance = this;
		audioComp = GetComponent<AudioSource> ();
	}


	public void playSd(int index)
	{
		audioComp.PlayOneShot (soundClip [index]);
	}


}




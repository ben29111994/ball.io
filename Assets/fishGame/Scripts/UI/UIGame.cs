﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGame : MonoBehaviour {

	public static UIGame 	instance;
	public  Text			coinText;
	public GameObject 		menuLost;
	public Slider slideFood;
	GameObject 		Icoshield;
	GameObject 		IcoSpeeding;
	GameObject 		IcoInvincible;
	GameObject 		btQuit;
	GameObject 		btRevive;

	// Use this for initialization
	void Start () {


		instance 		= this;
//		slideFood		= GameObject.Find ("slideFood").GetComponent<Slider> ();;
//		coinText 		= GameObject.Find ("coinText").GetComponent<Text> ();
//		menuLost 		= GameObject.Find ("menuLost");
//		btQuit 			= GameObject.Find ("btQuit");
//		btRevive		= GameObject.Find ("btRevive");
//		Icoshield 		= GameObject.Find ("Icoshield");
//		IcoSpeeding 	= GameObject.Find ("IcoSpeeding");
//		IcoInvincible 	= GameObject.Find ("IcoInvincible");


//		EventTriggerListener.Get(Icoshield).onDown = OnButtonClick;
//		EventTriggerListener.Get(IcoSpeeding).onDown = OnButtonClick;
////		EventTriggerListener.Get(IcoInvincible).onDown = OnButtonClick;
//		EventTriggerListener.Get(btQuit).onDown = OnButtonClick;
//		EventTriggerListener.Get(btRevive).onDown = OnButtonClick;

//		coinText.text 	= "0";
//		menuLost.SetActive (false);
//		IcoSpeeding.SetActive (false);
//		IcoInvincible.SetActive (false);
//		Icoshield.SetActive (false);
//		checkItemCanUse ();

//		slideFood.maxValue = 30;
//		slideFood.minValue = 0;
	}
	
	// Update is called once per frame
	void Update () {
		slidefood ();
	}

	void checkItemCanUse()
	{
		if(save.instance.load("speeding")==1)
		{
			IcoSpeeding.SetActive (true);
		}
		if(save.instance.load("invincible")==1)
		{
			IcoInvincible.SetActive (true);
		}
		if(save.instance.load("shiled")==1)
		{
			Icoshield.SetActive (true);
		}
	}


	void OnButtonClick(GameObject go)
	{

		if(go==IcoSpeeding)
		{
			spawnObject.instance.CreatFx ("speeding", GameManeger.instance.heroGO.transform.position); // use item, and creat item in the player position 
			save.instance.Save(0,"speeding");	
			go.SetActive (false);
		}
		if(go==IcoInvincible)
		{
			spawnObject.instance.CreatFx ("invincible", GameManeger.instance.heroGO.transform.position); // use item, and creat item in the player position 
			save.instance.Save(0,"invincible");	
			go.SetActive (false);
		}
		if(go==Icoshield)
		{
			spawnObject.instance.CreatFx ("shield", GameManeger.instance.heroGO.transform.position); // use item, and creat item in the player position 
			save.instance.Save(0,"shiled");	
			go.SetActive (false);

		}
		if(go==btQuit)
		{
			Application.LoadLevel ("start");
		}
		if(go==btRevive)
		{
//			if(shop.instance.checkCoin(200))
//			{
//				GameManeger.instance.heroGO.SetActive (true);
//			}
			Application.LoadLevel("lv01");
		}

	}
	void slidefood(){
		
//		slideFood.value = GameManeger.instance.food;
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy02 : MonoBehaviour {

	int 	hp =1;
	float 	dis = 0;
	float 	moveSpeed = 2;
	float 	tempT=0;
	float 	RunDirTime = 4;



	public int Hp {
		get {
			return hp;
		}
		set {
			hp = value;
			if(hp<=0)
			{
				Dead ();
//				GameManeger.instance.Coin++;
			}
		}
	}

	void OnEnable()
	{
		
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		if (GameManeger.instance.isDead)		//hero is dead
			return;

	//	dis = Vector3.Distance (transform.position, GameManeger.instance.heroGO.transform.position);
//		if(dis>15)
//		{
//			this.gameObject.SetActive (false);
//		}	
//		transform.LookAt (GameManeger.instance.heroGO.transform);
//		transform.Translate (Vector3.forward * Time.deltaTime * moveSpeed);
		tempT += Time.deltaTime;
		if(tempT>RunDirTime)
		{
			tempT = 0;
			transform.eulerAngles = new Vector3 (0, 0, Random.Range (0, 360));
		}
		moveSpeed = Random.Range (2, 4);
		transform.Translate (Vector3.right * Time.deltaTime *moveSpeed );

	}


	public void Dead()
	{
		GameManeger.instance.mobCreate--;
		this.gameObject.SetActive (false);
	}

//
	public void OnTriggerEnter(Collider other)
	{
//		if(other.CompareTag("bullet"))
//		{
//			//Hp--;
//		}
		if(other.CompareTag("heroFish"))
		{
			if(GameManeger.instance.isInvincible )
			{
				this.gameObject.SetActive (false);
				spawnObject.instance.CreatFx ("deadFx", transform.position);
//				GameManeger.instance.Coin++;
				PlaySound.instance.playSd (2);
			}
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fishOne : MonoBehaviour {


	public GameObject particleDead;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter(Collider other){// triger event

		if (other.CompareTag ("coin")) {
			Destroy (other.gameObject);
			uiController.instance.coin++;
			PlaySound.instance.playSd (0);
		}

		if (other.CompareTag ("enemy1")) {
			playerMove.instance.uac.animation.Play ("cap", 1);
			Instantiate (particleDead, transform.position, Quaternion.identity);
			Destroy (other.gameObject);
			PlaySound.instance.playSd (2);
			uiController.instance.score += 5;
			if (playerMove.instance.up == 0) {
				playerMove.instance.power.fillAmount+= 0.05f;
			} else
				playerMove.instance.power.fillAmount+= 0.01f;

		} else if (other.CompareTag ("enemy2")) {
			if (playerMove.instance.up >= 1) {
				uiController.instance.score += 10;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				playerMove.instance.uac.animation.Play ("cap", 1);
				PlaySound.instance.playSd (2);
				Destroy (other.gameObject);
				if (playerMove.instance.up == 1) {
					playerMove.instance.power.fillAmount += 0.05f;
				} else
					playerMove.instance.power.fillAmount += 0.01f;
			} else {
				PlaySound.instance.playSd (3);
				GameManeger.instance.Hp--;
				Instantiate (particleDead, transform.position, Quaternion.identity);
			}

		} else if (other.CompareTag ("enemy3")) {
			if (playerMove.instance.up >= 2) {
				uiController.instance.score += 20;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				playerMove.instance.uac.animation.Play ("cap", 1);
				PlaySound.instance.playSd (2);
				Destroy (other.gameObject);
				if (playerMove.instance.up == 2) {
					playerMove.instance.power.fillAmount += 0.05f;
				} else
					playerMove.instance.power.fillAmount += 0.01f;

			} else {
				GameManeger.instance.Hp--;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				PlaySound.instance.playSd (3);
		}	} else if (other.CompareTag ("enemy4")) {
			if (playerMove.instance.up >= 3) {
				uiController.instance.score += 40;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				playerMove.instance.uac.animation.Play ("cap", 1);
				PlaySound.instance.playSd (2);
				Destroy (other.gameObject);
				if (playerMove.instance.up == 3) {
					playerMove.instance.power.fillAmount += 0.05f;
				} else
					playerMove.instance.power.fillAmount += 0.01f;

			} else {
				GameManeger.instance.Hp--;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				PlaySound.instance.playSd (3);
			}
		} else if (other.CompareTag ("enemy5")) {
			if (playerMove.instance.up >= 4) {
				uiController.instance.score += 80;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				playerMove.instance.uac.animation.Play ("cap", 1);
				PlaySound.instance.playSd (2);
				Destroy (other.gameObject);
				if (playerMove.instance.up == 4) {
					playerMove.instance.power.fillAmount += 0.05f;
				} else
					playerMove.instance.power.fillAmount += 0.01f;
			} else {
				GameManeger.instance.Hp--;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				PlaySound.instance.playSd (3);
			}
		} else if (other.CompareTag ("enemy6")) {
			if  (playerMove.instance.up >= 5) {
				uiController.instance.score += 160;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				playerMove.instance.uac.animation.Play ("cap", 1);
				PlaySound.instance.playSd (2);
				Destroy (other.gameObject);
				if (playerMove.instance.up == 5) {
					playerMove.instance.power.fillAmount += 0.05f;
				} else
					playerMove.instance.power.fillAmount += 0.01f;
			} else {
				GameManeger.instance.Hp--;
				Instantiate (particleDead, transform.position, Quaternion.identity);
				PlaySound.instance.playSd (3);
			}
		} else if (other.CompareTag ("enemy7")) {
			//			if (up >= 6) {
			//				uiController.instance.score += 320;
			//				uac.animation.Play ("cap", 1);
			//				PlaySound.instance.playSd (2);
			//				Destroy (other.gameObject);
			//				if (up == 6) {
			//					power.fillAmount += 0.05f;
			//				} else
			//					power.fillAmount += 0.01f;

			//			} else {
			GameManeger.instance.Hp--;
			Instantiate (particleDead, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (3);
			//			}





		}else if (other.CompareTag("NE01") ){
			Instantiate (particleDead, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (3);
			//			spawnObject.instance.CreatFx ("deadFx", transform.position);
			GameManeger.instance.Hp--;
		}else if (other.CompareTag("NE02") ){
			Instantiate (particleDead, transform.position, Quaternion.identity);
			PlaySound.instance.playSd (3);
			//			spawnObject.instance.CreatFx ("deadFx", transform.position);
			GameManeger.instance.Hp--;
		}
	}

}

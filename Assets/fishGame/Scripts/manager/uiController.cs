﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class uiController : MonoBehaviour {

	bool checkAds = false;
	public GameObject tapToResume;
	public GameObject panelLost,toolsMenu;
	public Text scoreText1, scoreText2,coinText,bestScore;
	public int score, coin;
	public static uiController instance;
	private const string BEST_SCORE = "best_score";
//	public const string COIN_COIN = "coin_coin";
	public string co = "coin_coin";
	float time;
	// Use this for initialization
	void Start () {

		coin = PlayerPrefs.GetInt (co);

		instance = this;
		panelLost = GameObject.Find ("panelLost");
		toolsMenu = GameObject.Find ("ToolsMenu");
		panelLost.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {

		PlayerPrefs.SetInt (co, coin);

		if (GameManeger.instance.isDead) {
			Invoke ("isDead", 2);
			time += Time.deltaTime;
	
			if (checkAds == false) {
				time += Time.deltaTime;

				if (time > 5.5f) {
					checkAds = true;
				}
			}
			
		}



		if (score > PlayerPrefs.GetInt (BEST_SCORE)) {
			PlayerPrefs.SetInt (BEST_SCORE, score);
		}


		scoreText1.text = "" + score;
		scoreText2.text = "" + score;
		bestScore.text = "Best :" +PlayerPrefs.GetInt (BEST_SCORE);
		coinText.text = "" + PlayerPrefs.GetInt (co);;
	
	}

	public void TapToResume(){
		SceneManager.LoadScene ("lv01");
	}

	void LvSliderFood(){

		if (GameManeger.instance.food > 10 && GameManeger.instance.food <20) {
			GameManeger.instance.food -= 0.2f;
		}else if (GameManeger.instance.food > 20 && GameManeger.instance.food <40) {
			GameManeger.instance.food -= .3f;
		}else if (GameManeger.instance.food > 40 && GameManeger.instance.food <60) {
			GameManeger.instance.food -= .4f;
		}else if (GameManeger.instance.food > 60 && GameManeger.instance.food <80) {
			GameManeger.instance.food -= .5f;
		}else if (GameManeger.instance.food > 80 && GameManeger.instance.food <100) {
			GameManeger.instance.food -= .6f;
		}else if (GameManeger.instance.food > 100 && GameManeger.instance.food <120) {
			GameManeger.instance.food -= .7f;
		}else if (GameManeger.instance.food > 120 && GameManeger.instance.food <140) {
			GameManeger.instance.food -= .8f;
		}

	}

	void isDead(){

			panelLost.SetActive (true);
			toolsMenu.SetActive (false);
	
	
	}



}

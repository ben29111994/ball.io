﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sliderFood : MonoBehaviour {
	public GameObject player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.localScale.x < 0) {
			Vector3 temp = transform.localScale;
			temp.x = -.2f;
			transform.localScale = temp;
		} else {
			Vector3 tempp = transform.localScale;
			tempp.x = .2f;
			transform.localScale = tempp;
		}	

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy4 : MonoBehaviour {

	public Transform[] spawn01;
	public Transform[] spawn02;
	public GameObject enemyNe01,enemyNe02;
	public GameObject[] enemy;
	public GameObject allClone;
	int a;
	int b,c,d,e;
	// Use this for initialization
	void Start () {
//		spawn02 [0] = GameObject.FindWithTag ("spw0").transform;
//		spawn02 [1] = GameObject.FindWithTag ("spw1").transform;
//		spawn02 [2] = GameObject.FindWithTag ("spw2").transform;
//		spawn02 [3] = GameObject.FindWithTag ("spw3").transform;
//		spawn02 [4] = GameObject.FindWithTag ("spw4").transform;
//		spawn02 [5] = GameObject.FindWithTag ("spw5").transform;
//		spawn02 [6] = GameObject.FindWithTag ("spw6").transform;
//		spawn02 [7] = GameObject.FindWithTag ("spw7").transform;
//		spawn02 [8] = GameObject.FindWithTag ("spw").transform;
//		spawn02 [9] = GameObject.FindWithTag ("spw9").transform;

		a = Random.Range (0, 7);
		b = Random.Range (0, 10);
		c = Random.Range (0, 10);
		d = Random.Range (0, 10);
		e = Random.Range (0, 10);
		StartCoroutine (spawnEnemyNe01 ());
		StartCoroutine (spawnEnemyNe02 ());
		StartCoroutine (spawnEnemy1 ());
		StartCoroutine (spawnEnemy2 ());
		StartCoroutine (spawnEnemy3 ());
		StartCoroutine (spawnEnemy4 ());
		StartCoroutine (spawnEnemy5 ());
		StartCoroutine (spawnEnemy6 ());
		StartCoroutine (spawnEnemy7 ());
	}

	// Update is called once per frame
	void Update () {
		a = Random.Range (0, 7);
		b = Random.Range (0, 10);
		c = Random.Range (0, 10);
		d = Random.Range (0, 10);
		e = Random.Range (0, 10);
	}

	IEnumerator spawnEnemyNe01(){
		if (a > -1 && a <= 4) {
			Instantiate (enemyNe01, spawn01 [a].position,Quaternion.Euler(0,0,180), gameObject.transform);
		} else {
			Instantiate (enemyNe01, spawn01 [a].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds (5);
		StartCoroutine( spawnEnemyNe01 ());
	}
	IEnumerator spawnEnemyNe02(){
		Instantiate (enemyNe02, spawn02 [b].position, Quaternion.identity, gameObject.transform);
		yield return new WaitForSeconds (5);
		StartCoroutine (spawnEnemyNe02 ());
	}
	IEnumerator spawnEnemy1(){
		if (c >= 0 && c <= 4) { 
			Instantiate (enemy [0], spawn02 [c].position, Quaternion.Euler(0,0,180),gameObject.transform);
		} else {
			Instantiate (enemy [0], spawn02 [c].position, Quaternion.identity,gameObject.transform);
		}
		yield return new WaitForSeconds (1.5f);
		StartCoroutine (spawnEnemy1 ());
	}
	IEnumerator spawnEnemy2(){
		if (d >= 0 && d <= 4) {
			Instantiate (enemy [1], spawn02 [d].position, Quaternion.Euler (0, 0, 180), gameObject.transform);
		} else {
			Instantiate (enemy [1], spawn02 [d].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds (1);
		StartCoroutine (spawnEnemy2 ());
	}
	IEnumerator spawnEnemy3(){
		if (e >= 0 && e <= 4) {
			Instantiate (enemy [2], spawn02 [e].position, Quaternion.Euler (0, 0, 180), gameObject.transform);
		} else {
			Instantiate (enemy [2], spawn02 [e].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds (2);
		StartCoroutine (spawnEnemy3 ());
	}
	IEnumerator spawnEnemy4(){
		if (c >= 0 && c <= 4) {
			Instantiate (enemy [3], spawn02 [c].position, Quaternion.Euler (0, 0, 180), gameObject.transform);
		} else {
			Instantiate (enemy [3], spawn02 [c].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds (3);
		StartCoroutine (spawnEnemy4 ());
	}
	IEnumerator spawnEnemy5(){
		if (d >= 0 && d <= 4) {
			Instantiate (enemy [4], spawn02 [d].position, Quaternion.Euler (0, 0, 180), gameObject.transform);
		} else {
			Instantiate (enemy [4], spawn02 [d].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds (4);
		StartCoroutine (spawnEnemy5 ());
	}
	IEnumerator spawnEnemy6(){
		if (e >= 0 && e <= 4) {
			Instantiate (enemy [5], spawn02 [e].position, Quaternion.Euler (0, 0, 180), gameObject.transform);
		} else {
			Instantiate (enemy [5], spawn02 [e].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds(5);
		StartCoroutine (spawnEnemy6 ());
	}
	IEnumerator spawnEnemy7(){
		if (c >= 0 && c <=4) {
			Instantiate (enemy [6], spawn02 [c].position, Quaternion.Euler (0, 0, 180), gameObject.transform);
		} else {
			Instantiate (enemy [6], spawn02 [c].position, Quaternion.identity, gameObject.transform);
		}
		yield return new WaitForSeconds (6);
		StartCoroutine (spawnEnemy7 ());
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camShake : MonoBehaviour {

	public static camShake instance;
	public bool isShaking;
	public bool aula = true;
	float duration;

	void Awake(){
		instance = this;
	}

	void Update () {
		if (isShaking) {
			if (duration < 1) {
				duration += Time.deltaTime * 2.5f;
				Vector2 shake = UnityEngine.Random.insideUnitCircle * 0.25f;
				Vector3 newPos = transform.localScale;
				newPos.x = shake.x;
				newPos.y = shake.y;
				transform.localScale = newPos;
				if (aula) {
					float shakes = UnityEngine.Random.Range (-5f, 5f);
					transform.localRotation = Quaternion.Euler (0f, 0f, shakes);
				}
			} else {
				duration = 0f;
				isShaking = false;
			}
		} else {
			transform.localScale = Vector3.zero;
			transform.localRotation = Quaternion.identity;
		}
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwimFish : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int Id;
    [SerializeField]
    private int level;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float range;

    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;
    #endregion

    #region Normal paramters    
    private Vector3 velocity;

    private Vector3 startPosition;
    private Vector3 startEulerAngles;

    private float rangeX;
    private float rangeY;
    #endregion

    #region Encapsulate
    public int ID
    {
        get
        {
            return Id;
        }

        set
        {
            Id = value;
        }
    }

    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }
    #endregion

    public void Initialize()
    {
        startPosition = Vector3.zero;
        startEulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

        var radianZ = startEulerAngles.z * Mathf.Deg2Rad;
        velocity = new Vector3(Mathf.Cos(radianZ), Mathf.Sin(radianZ));

        transform.localEulerAngles = startEulerAngles;
        transform.localPosition = UnityEngine.Random.insideUnitCircle * speed;

        rangeY = Camera.main.orthographicSize;
        rangeX = rangeY * Screen.width / Screen.height;

        armatureComponent.gameObject.SetActive(false);

        armatureComponent = DragonBonesManager.Instance.GeneratePlayer((DragonBonesType)Id, level);
        armatureComponent.transform.SetParent(transform, false);
        armatureComponent.sortingOrder = 1000;
    }

    public void Release()
    {

    }

    public void Refresh()
    {

    }

    public void UpdateStep(float deltaTime)
    {
        var newPosition = transform.localPosition;
        newPosition += velocity * deltaTime * speed;

        if ((newPosition.x > startPosition.x + rangeX) ||
            (newPosition.x < startPosition.x - rangeX) ||
            (newPosition.y > startPosition.y + rangeY - 3) ||
            (newPosition.y < startPosition.y - rangeY))
        {
            startEulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

            var radianZ = startEulerAngles.z * Mathf.Deg2Rad;
            velocity = new Vector3(Mathf.Cos(radianZ), Mathf.Sin(radianZ));

            transform.localEulerAngles = startEulerAngles;
        }
        else
        {
            transform.localPosition = newPosition;
        }

        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360;

        if (angleZ > 90 && angleZ < 270)
        {
            armatureComponent.armature.flipY = true;
        }
        else
        {
            armatureComponent.armature.flipY = false;
        }
    }

    public void Awake()
    {
        Initialize();
    }

    public void Update()
    {
        UpdateStep(Time.deltaTime);
    }

}

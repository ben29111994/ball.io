﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DecorFishState
{
    Normal,
    Hungry,
    Left,
    Feed,

    TOTAL,
    IDLE
}


public class DecorFish : Boid
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int id;
    [SerializeField]
    private string artName;
    [SerializeField]
    private DecorFishState state = DecorFishState.IDLE;
    [SerializeField]
    private System.DateTime buyTime;
    [SerializeField]
    private System.DateTime feedTime;

    [Header("Debug only")]
    [SerializeField]
    private Vector3 debugVelocity;
    #endregion

    #region Normal paramters
    private DragonBones.UnityArmatureComponent armatureComponent;

    private float rangeX;
    private float rangeY;

    private Vector3 startPosition;
    private Vector3 startEulerAngles;

    private float speed = 1;
    #endregion

    #region Encapsulate
    public int ID
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    public string ArtName
    {
        get
        {
            return artName;
        }

        set
        {
            artName = value;
        }
    }

    public DecorFishState State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }
    #endregion

    public void Initialize()
    {
        armatureComponent = DragonBonesManager.Instance.GenerateDecorFish(id);
        if (armatureComponent == null)
        {
            //try to get art from prototype
            armatureComponent = DragonBonesManager.Instance.GeneratePlayer(0);
        }
        armatureComponent.transform.SetParent(transform, false);
        armatureComponent.sortingOrder = 1000;

        rangeY = Camera.main.orthographicSize;
        rangeX = rangeY * Screen.width / Screen.height;

        //boid config
        startEulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

        var radianZ = startEulerAngles.z * Mathf.Deg2Rad;
        velocity = new Vector3(Mathf.Cos(radianZ), Mathf.Sin(radianZ));

        position = Vector3.zero;

        transform.localPosition = position;
        transform.localEulerAngles = startEulerAngles;
        transform.localScale = Vector3.one;
    }

    public void Release()
    {
    }

    public void Refresh()
    {
    }

    public void UpdateStep(float deltaTime)
    {
        var newPosition = transform.localPosition;
        newPosition += velocity * deltaTime * speed;

        if ((newPosition.x > startPosition.x + rangeX) ||
            (newPosition.x < startPosition.x - rangeX) ||
            (newPosition.y > startPosition.y + rangeY - 3) ||
            (newPosition.y < startPosition.y - rangeY))
        {
            startEulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

            var radianZ = startEulerAngles.z * Mathf.Deg2Rad;
            velocity = new Vector3(Mathf.Cos(radianZ), Mathf.Sin(radianZ));

            transform.localEulerAngles = startEulerAngles;
        }
        else
        {
            transform.localPosition = newPosition;
        }

        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360;

        if (angleZ > 90 && angleZ < 270)
        {
            armatureComponent.armature.flipY = true;
        }
        else
        {
            armatureComponent.armature.flipY = false;
        }
    }

    public string Serialize()
    {
        string data = string.Format("{0},{1},{2},{3},{4}", id, artName, (int)state, buyTime.ToString(), feedTime.ToString());
        return data;
    }

    public void Deserialize(string data)
    {
        var array = data.Split(',');
        id = int.Parse(array[0]);
        if (!string.IsNullOrEmpty(array[1]))
            artName = array[1];

        state = (DecorFishState)(int.Parse(array[2]));
        buyTime = System.DateTime.Parse(array[3]);
        feedTime = System.DateTime.Parse(array[4]);
    }

    public void ChangeState(DecorFishState nextState)
    {
        if (state == nextState)
            return;

        state = nextState;
    }

    public void Eat()
    {
        var system = ParticleEffect.Instance.GenerateParticle(ParticleType.Heart);
        system.transform.position = transform.position;

        system.transform.SetParent(transform);
    }

}
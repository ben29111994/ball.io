﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TankState
{
    TankUI,
    HatchUI,

    TOTAL,
    IDLE,
}

public class Tank : MonoSingleton<Tank>
{

    #region Const parameters
    private const string ACTIVE_HATCHING_EGGS_KEY = "ActiveHatchingEggs";
    private const string LIST_UNLOCKED_TANKS_KEY = "ListUnlockedTanks";
    private const string ACTIVE_TANK_ID = "ActiveTankID";

    private const string LIST_ACTIVE_DECOR_FISHES_KEY = "ListActiveDecorFishes";
    private const string LAST_FEED_TIME_KEY = "LastFeedTime";
    private const string LAST_COLLECT_TIME_KEY = "LastCollectTime";
    private const string MONEY_EARN_KEY = "MoneyEarn";

    private const string FEED_BONUS_TIME_LEFT_KEY = "FeedBonusTimeLeft";
    #endregion

    #region Editor paramters
    [SerializeField]
    private TankState tankState = TankState.IDLE;
    [SerializeField]
    private int tankLevel;

    [SerializeField]
    private GameObject podiumContainer;
    [SerializeField]
    private GameObject swimmingFishContainer;
    [SerializeField]
    private GameObject shopContainer;
    [SerializeField]
    private GameObject fishContainer;
    [SerializeField]
    private GameObject playContainer;

    [SerializeField]
    private int totalPodium;
    [SerializeField]
    private List<Podium> podiums;
    [SerializeField]
    private List<SwimFish> swimFishes;

    [SerializeField]
    private List<Sprite> listTankBackgrounds;
    [SerializeField]
    private SpriteRenderer spTankBackground;

    [Header("Decor Fish")]
    [SerializeField]
    private List<DecorFish> listOriginalDecorFishes;
    [SerializeField]
    private List<DecorFish> listActiveDecorFishes;

    [SerializeField]
    private int feedBonusTime = 24 * 60 * 60;
    [SerializeField]
    private int feedBonusTimeLeft;

    [SerializeField]
    private FlockingController flockingController;
    [SerializeField]
    private float showTime = 0.1f;
    [SerializeField]
    private float showTimeCountdown;

    [Header("Debug only")]
    [SerializeField]
    private float foodLeft = 1.0f;
    [SerializeField]
    private int moneyPerMin;
    [SerializeField]
    private int moneyEarn;
    [SerializeField]
    private int moneyLimit = 1000;
    [SerializeField]
    private int fishCounter;
    [SerializeField]
    private int unlockedFishCounter;
    [SerializeField]
    private int decorFishCounter;
    [SerializeField]
    private string lastFeedTimeString;
    [SerializeField]
    private string lastCollectTimeString;
    [SerializeField]
    private string nextCollectTimeString;
    #endregion

    #region Normal paramters
    private List<Podium> activePodiums;
    private List<Egg> activeHatchingEggs;

    private List<int> listUnlockedTanks;
    private int activeTankID;

    private System.DateTime lastFeedTime;
    private System.DateTime lastCollectTime;
    #endregion

    #region Encapsulate
    public int ActiveTankID
    {
        get
        {
            return activeTankID;
        }

        set
        {
            activeTankID = value;
        }
    }
    #endregion

    public override void Initialize()
    {
        activePodiums = new List<Podium>();
        activeHatchingEggs = new List<Egg>();

        for (int i = 0; i < podiums.Count; i++)
        {
            podiums[i].Initialize();
        }

        podiumContainer.SetActive(false);
        swimmingFishContainer.SetActive(false);


        listUnlockedTanks = new List<int>();

        flockingController.Initialize();

        showTimeCountdown = showTime;

        Load();
    }

    public void Release()
    {
        for (int i = 0; i < podiums.Count; i++)
        {
            podiums[i].Release();
        }
    }

    public void Refresh()
    {
        for (int i = 0; i < podiums.Count; i++)
        {
            podiums[i].Refresh();
        }
    }

    public void UpdateStep(float deltaTime)
    {
        // REM to renew collect money logic

        //unlockedFishCounter = CharacterManager.Instance.GetTotalUnlockedCharacter();
        //decorFishCounter = listActiveDecorFishes.Count;

        //var timeSpan = lastFeedTime.AddSeconds(feedBonusTime) - System.DateTime.Now;
        //foodLeft = (float)(timeSpan.TotalSeconds / feedBonusTime);

        //fishCounter = unlockedFishCounter + decorFishCounter;
        //moneyPerMin = (int)(fishCounter * foodLeft);
        //moneyPerMin = Mathf.Max(moneyPerMin, 0);

        //timeSpan = System.DateTime.Now - lastCollectTime;
        //if(timeSpan.TotalSeconds > 60)
        //{
        //    lastCollectTime = lastCollectTime.AddSeconds(60);
        //    lastCollectTimeString = lastCollectTime.ToString();

        //    moneyEarn += moneyPerMin;
        //    moneyEarn = Mathf.Min(moneyEarn, moneyLimit);
        //    Save();           
        //}

        // END


        // new collect money logic
        CalculateMoneyInTank();

        flockingController.UpdateStep(deltaTime);

        for (int i = 0; i < listActiveDecorFishes.Count; i++)
        {
            listActiveDecorFishes[i].UpdateStep(deltaTime);
        }

        if (showTimeCountdown > 0)
        {
            showTimeCountdown -= deltaTime;
        }
        else
        {
            showTimeCountdown = showTime;
            ShowDecorFish();
        }
    }

    public void ChangeState(TankState nextState)
    {
        //if (tankState == nextState)
        //    return;

        tankState = nextState;

        switch (tankState)
        {
            case TankState.TankUI:
                podiumContainer.SetActive(true);
                swimmingFishContainer.SetActive(true);
                fishContainer.SetActive(true);

                RefreshFishes();
                break;
            case TankState.HatchUI:
                podiumContainer.SetActive(true);
                swimmingFishContainer.SetActive(true);
                fishContainer.SetActive(true);

                RefreshPodiums();
                break;
            case TankState.IDLE:
                podiumContainer.SetActive(false);
                swimmingFishContainer.SetActive(false);
                fishContainer.SetActive(false);
                break;
            default:
                break;
        }
    }

    private void RefreshPodiums()
    {

        for (int i = 0; i < podiums.Count; i++)
        {
            podiums[i].gameObject.SetActive(false);
        }

        activePodiums.Clear();

        if (podiums.Count == 3)
        {
            if (tankLevel > 2)
            {
                activePodiums.Add(podiums[1]);
                activePodiums.Add(podiums[2]);
            }
            else
            {
                activePodiums.Add(podiums[0]);
            }
        }
        else
        {
            activePodiums.AddRange(podiums);
        }

        for (int i = 0; i < activePodiums.Count; i++)
        {
            activePodiums[i].Refresh();
        }

        for (int i = 0; i < activeHatchingEggs.Count; i++)
        {
            if (activePodiums[activeHatchingEggs[i].PodiumID].gameObject.activeInHierarchy)
            {
                activePodiums[activeHatchingEggs[i].PodiumID].HatchingEgg(activeHatchingEggs[i]);
            }
            activePodiums[activeHatchingEggs[i].ID].EggID = activeHatchingEggs[i].ID;
        }
    }

    private void RefreshFishes()
    {
        spTankBackground.sprite = listTankBackgrounds[activeTankID];

        for (int i = 0; i < swimFishes.Count; i++)
        {
            swimFishes[i].gameObject.SetActive(CharacterManager.Instance.IsCharacterUnlocked(i));
        }
    }

    public void SpawnEgg(Egg newEgg)
    {
        for (int i = 0; i < activePodiums.Count; i++)
        {
            if (activePodiums[i].EggID == -1)
            {
                newEgg.PodiumID = activePodiums[i].ID;
                break;
            }
        }

        newEgg.ID = activeHatchingEggs.Count;

        if (activePodiums[newEgg.PodiumID].gameObject.activeInHierarchy)
        {
            activePodiums[newEgg.PodiumID].SpawnEgg(newEgg);
        }
        activePodiums[newEgg.ID].EggID = newEgg.ID;

        activeHatchingEggs.Add(newEgg);

        Save();
    }

    public void RemoveEgg(Egg eggToRemove)
    {
        if (activePodiums[eggToRemove.PodiumID].gameObject.activeInHierarchy)
        {
            activePodiums[eggToRemove.PodiumID].RemoveEgg(eggToRemove);
        }
        activePodiums[eggToRemove.PodiumID].EggID = -1;

        activeHatchingEggs.Remove(eggToRemove);
    }

    public bool HasEmptyPodium()
    {
        return activeHatchingEggs.Count < activePodiums.Count;
    }

    public void Load()
    {
        string data = DataManager.Instance.GetString(ACTIVE_HATCHING_EGGS_KEY, string.Empty);

        if (!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if (!string.IsNullOrEmpty(array[i]))
                {
                    Egg egg = JsonUtility.FromJson<Egg>(array[i]);
                    egg.Initialize();

                    activeHatchingEggs.Add(egg);
                }
            }
        }

        data = DataManager.Instance.GetString(LIST_UNLOCKED_TANKS_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if (!string.IsNullOrEmpty(array[i]))
                {
                    listUnlockedTanks.Add(int.Parse(array[i]));
                }
            }
        }

        activeTankID = DataManager.Instance.GetInt(ACTIVE_TANK_ID, -1);
        if (activeTankID == -1)
        {
            activeTankID = 0;
            listUnlockedTanks.Add(activeTankID);
        }


        // decor fish 
        data = DataManager.Instance.GetString(LIST_ACTIVE_DECOR_FISHES_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if (string.IsNullOrEmpty(array[i]))
                {
                    continue;
                }
                GameObject obj = new GameObject();
                obj.transform.SetParent(swimmingFishContainer.transform);

                DecorFish fish = obj.AddComponent<DecorFish>();
                fish.Deserialize(array[i]);
                fish.Initialize();

                listActiveDecorFishes.Add(fish);

                //flockingController.AddBoid(fish);
            }
        }

        data = DataManager.Instance.GetString(LAST_FEED_TIME_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            lastFeedTime = System.DateTime.Parse(data);
        }
        else
        {
            lastFeedTime = System.DateTime.Now;
        }

        lastFeedTimeString = lastFeedTime.ToString();

        data = DataManager.Instance.GetString(LAST_COLLECT_TIME_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            lastCollectTime = System.DateTime.Parse(data);
        }
        else
        {
            lastCollectTime = System.DateTime.Now;
        }

        lastCollectTimeString = lastCollectTime.ToString();

        moneyEarn = DataManager.Instance.GetInt(MONEY_EARN_KEY, 0);

        feedBonusTimeLeft = DataManager.Instance.GetInt(FEED_BONUS_TIME_LEFT_KEY, feedBonusTime);
        
        CalculateMoneyInTank();

        Save();
    }

    public void Save()
    {
        // save list hatching eggs
        string data = string.Empty;
        for (int i = 0; i < activeHatchingEggs.Count; i++)
        {
            data += activeHatchingEggs[i].Serialize();
            data += ";";
        }
        DataManager.Instance.SetString(ACTIVE_HATCHING_EGGS_KEY, data);

        // save list unlocked tank 
        data = string.Empty;
        for (int i = 0; i < listUnlockedTanks.Count; i++)
        {
            data += listUnlockedTanks[i];
            data += ';';
        }
        DataManager.Instance.SetString(LIST_UNLOCKED_TANKS_KEY, data);

        DataManager.Instance.SetInt(ACTIVE_TANK_ID, activeTankID);

        DataManager.Instance.SetString(LAST_FEED_TIME_KEY, lastFeedTime.ToString());

        DataManager.Instance.SetString(LAST_COLLECT_TIME_KEY, lastCollectTime.ToString());

        data = string.Empty;
        for (int i = 0; i < listActiveDecorFishes.Count; i++)
        {
            data += listActiveDecorFishes[i].Serialize();
            data += ";";
        }

        DataManager.Instance.SetString(LIST_ACTIVE_DECOR_FISHES_KEY, data);

        DataManager.Instance.SetInt(MONEY_EARN_KEY, moneyEarn);

        DataManager.Instance.SetInt(FEED_BONUS_TIME_LEFT_KEY, feedBonusTimeLeft);

        DataManager.Instance.Save();
    }

    public Egg GetEgg(int eggID)
    {
        for (int i = 0; i < activeHatchingEggs.Count; i++)
        {
            if (activeHatchingEggs[i].ID == eggID)
                return activeHatchingEggs[i];
        }

        return null;
    }

    public Vector3 GetPodiumContainerPosition()
    {
        return podiumContainer.transform.position;
    }

    public Vector3 GetSwimmingFishContainerPosition()
    {
        return swimmingFishContainer.transform.position;
    }

    public Vector3 GetShopContainerPosition()
    {
        return shopContainer.transform.position;
    }

    public Vector3 GetFishContainerPosition()
    {
        return fishContainer.transform.position;
    }

    public Vector3 GetPlayContainerPosition()
    {
        return playContainer.transform.position;
    }

    public void UnlockTank(int tankID)
    {
        listUnlockedTanks.Add(tankID);
        Save();
    }

    public bool IsUnlockedTank(int tankID)
    {
        return listUnlockedTanks.Contains(tankID);
    }

    public void ChangeTank(int tankID)
    {
        activeTankID = tankID;
        // change layout here ...
        spTankBackground.sprite = listTankBackgrounds[tankID];

        Save();
    }

    public void FeedTheFish()
    {
        SoundManager.Instance.PlaySFX(SFXType.CollectCoin);

        // active effect
        //for (int i = 0; i < listActiveDecorFishes.Count; i++)
        //{
        //    listActiveDecorFishes[i].Eat();
        //}

        for (int j = 0; j < 30; j++)
        {
            var system = ParticleEffect.Instance.GenerateParticle(ParticleType.Heart);
            system.transform.SetParent(swimmingFishContainer.transform, true);
            system.transform.localPosition = UnityEngine.Random.insideUnitCircle * 2;
        }

        // change feed time
        lastFeedTime = System.DateTime.Now;
        feedBonusTimeLeft = feedBonusTime;

        foodLeft = (feedBonusTimeLeft * 1.0f / feedBonusTime);
        moneyPerMin = (int)(fishCounter * foodLeft);

        Save();

        // collect money        
        DataManager.Instance.Food--;
        DataManager.Instance.Money += listActiveDecorFishes.Count * 20;
        DataManager.Instance.Save();
    }

    public float GetFoodLeft()
    {
        return foodLeft;
    }

    public int GetMoneyPerMin()
    {
        return moneyPerMin;
    }

    public int GetFishCounter()
    {
        return fishCounter;
    }

    public int GetMoneyEarn()
    {
        return moneyEarn;
    }

    public void UnlockedDecorFish(int fishID)
    {
        var fish = Instantiate<DecorFish>(listOriginalDecorFishes[fishID], swimmingFishContainer.transform);
        fish.Initialize();

        listActiveDecorFishes.Add(fish);
        //flockingController.AddBoid(fish);

        Save();
    }

    public void Update()
    {
        UpdateStep(Time.deltaTime);
    }

    public void OnApplicationQuit()
    {
        Save();
    }

    public void ShowDecorFish()
    {

    }

    public void CollectMoney()
    {
        SoundManager.Instance.PlaySFX(SFXType.CollectCoin);

        DataManager.Instance.Money += moneyEarn;
        DataManager.Instance.Save();

        moneyEarn = 0;
        lastCollectTime = System.DateTime.Now;
        Save();
    }

    private void CalculateMoneyInTank()
    {
        unlockedFishCounter = CharacterManager.Instance.GetTotalUnlockedCharacter();
        decorFishCounter = listActiveDecorFishes.Count;

        fishCounter = unlockedFishCounter + decorFishCounter;
        foodLeft = (feedBonusTimeLeft * 1.0f / feedBonusTime);

        moneyPerMin = (int)(fishCounter * foodLeft);

        System.DateTime nextCollectTime = System.DateTime.Now;
        int count = (int)(nextCollectTime - lastCollectTime).TotalMinutes;
        
        if(count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                feedBonusTimeLeft = Mathf.Max(feedBonusTimeLeft - 60, 0);
                foodLeft = (feedBonusTimeLeft * 1.0f / feedBonusTime);

                moneyPerMin = Mathf.RoundToInt(fishCounter * foodLeft);
                moneyEarn += Mathf.Max(moneyPerMin, 0);
                moneyEarn = Mathf.Min(moneyEarn, moneyLimit);
                //Debug.Log(string.Format("[Tank] {0} {1} {2} {3} {4}", moneyPerMin, moneyEarn, foodLeft, fishCounter, moneyPerMin));
            }

            lastCollectTime = lastCollectTime.AddMinutes(count);
            lastCollectTimeString = lastCollectTime.ToString();

            Save();
        }

        nextCollectTimeString = nextCollectTime.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Podium : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int Id;
    [SerializeField]
    private int eggID;
    #endregion

    #region Normal paramters
    [SerializeField]
    private SpriteRenderer spPodium;
    [SerializeField]
    private SpriteRenderer spEgg;
    [SerializeField]
    private CircleCollider2D circleCollider2D;

    [SerializeField]
    private Animator eggAnimator;
    #endregion

    #region Encapsulate
    public int ID
    {
        get
        {
            return Id;
        }

        set
        {
            Id = value;
        }
    }

    public int EggID
    {
        get
        {
            return eggID;
        }

        set
        {
            eggID = value;
        }
    }
    #endregion

    public void Initialize()
    {
        spEgg.sprite = null;

        gameObject.SetActive(false);

        circleCollider2D.gameObject.SetActive(false);
    }

    public void Release()
    {
        spEgg.sprite = null;

        gameObject.SetActive(false);
    }

    public void Refresh()
    {
        spEgg.sprite = null;

        gameObject.SetActive(true);

        circleCollider2D.gameObject.SetActive(false);
    }

    public void UpdateStep(float deltaTime)
    {
    }

    public void SpawnEgg(Egg e)
    {
        spEgg.sprite = e.Sprite;
        spEgg.color = Color.white;

        eggAnimator.Play("Podium_Spawn");

        circleCollider2D.gameObject.SetActive(false);
    }

    public void HatchingEgg(Egg e)
    {
        spEgg.sprite = e.Sprite;
        spEgg.color = Color.white;

        eggAnimator.Play("Podium_Hatching");

        circleCollider2D.gameObject.SetActive(true);
    }

    public void RemoveEgg(Egg e)
    {
        spEgg.sprite = null;
        spEgg.color = Color.white;

        //eggAnimator.Play("Podium_Remove");

        circleCollider2D.gameObject.SetActive(false);
    }

    public void OnAnimationCompleted(string animationName)
    {
        circleCollider2D.gameObject.SetActive(true);
    }

}

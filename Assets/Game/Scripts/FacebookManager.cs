﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacebookManager : MonoSingleton<FacebookManager>
{
    #region Const parameters
    #endregion

    #region Editor paramters
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

#if ENABLE_FACEBOOK
        if (Facebook.Unity.FB.IsInitialized)
        {
            Facebook.Unity.FB.ActivateApp();
        }
        else
        {
            Debug.Log("[FacebookManager] Initialize Facebook...");
            Facebook.Unity.FB.Init(
                () =>
                {
                    Facebook.Unity.FB.ActivateApp();
                });
        }
#endif
    }

    public void Release()
    {
    }

    public void OnApplicationPause(bool pause)
    {
        // Check the pauseStatus to see if we are in the foreground
        // or background
        if (!pause)
        {
            //app resume
#if ENABLE_FACEBOOK
            if (Facebook.Unity.FB.IsInitialized)
            {
                Facebook.Unity.FB.ActivateApp();
            }
            else
            {
                Debug.Log("[FacebookManager] Initialize Facebook...");
                Facebook.Unity.FB.Init(
                    () =>
                    {
                        Facebook.Unity.FB.ActivateApp();
                    });
            }
#endif
        }
    }

}

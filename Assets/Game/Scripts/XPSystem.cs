﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XPSystem : MonoSingleton<XPSystem>
{

    #region Const parameters
    public const float XP2Second = 0.1f;
    public const float Second2XP = 10;
    #endregion

    #region Editor paramters
    [SerializeField]
    private int startLevel = 1;
    [SerializeField]
    private int baseFishQuantityToLevelUp = 10;
    [SerializeField]
    private float baseXPDegradeSpeed = 0.15f;
    [SerializeField]
    private float baseXPReviveCost = 0.5f;
    [SerializeField]
    private List<int> baseXPClaimEggByType = new List<int>(4);

    [Header("Debug only")]
    [SerializeField]
    private float playerLevelUpXP;
    [SerializeField]
    private float playerDegradeXP;
    [SerializeField]
    private int playerReviveXP;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        playerLevelUpXP = GetPlayerLevelUpXP(1);
        playerDegradeXP = GetPlayerDegradeXP(1);
        playerReviveXP = GetPlayerReviveXP(1);
    }

    public void Refesh()
    {
        int nextLevel = Player.Instance.Level + 1;

        playerLevelUpXP = GetPlayerLevelUpXP(nextLevel);
        playerDegradeXP = GetPlayerDegradeXP(nextLevel);
        playerReviveXP = GetPlayerReviveXP(nextLevel);
    }

    public int GetEnemyBonusXP(int level)
    {
        return (int)Mathf.Pow(level, 2);
    }

    public float GetPlayerLevelUpXP(int level)
    {
        return (baseFishQuantityToLevelUp + Mathf.Pow((level - startLevel), 2)) * GetEnemyBonusXP(level);
    }

    public float GetPlayerDegradeXP(int level)
    {
        return baseXPDegradeSpeed * Mathf.Pow(level, 2);
    }

    public int GetPlayerReviveXP(int level)
    {
        return (int)(baseXPReviveCost * GetPlayerLevelUpXP(level));
    }

    public int GetEggClaimXP(EggType type)
    {
        int id = (int)type;

        if(id >= baseXPClaimEggByType.Count)
        {
            Debug.Log("[XPSystem] Somethings wrong. Please check EggType again: " + type);
            return int.MaxValue;
        }

        return baseXPClaimEggByType[(int)type];
    }

}

﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NiobiumStudios;

public enum GameState
{
    Start,
    Play,
    Pause,
    End,
    Revive,
    TOTAL,
    IDLE,
}

public class GameManager : MonoSingleton<GameManager>
{

    #region Const parameters
    private const string REMOVE_ADS_KEY = "RemoveAds";
    #endregion

    #region Editor paramters
    [SerializeField]
    private GameState gameState;

    [SerializeField]
    private Fishy fishy;

    [SerializeField]
    private Transform fishContainer;

    [SerializeField]
    private Background background;

    [Header("Revive")]
    [SerializeField]
    private float reviveTImes = 5.0f;
    [SerializeField]
    private int revivePrice = 0;
    [SerializeField]
    private bool isRevived = false;

    [Header("Ads")]
    [SerializeField]
    private int skipSeconds = 1200;
    [SerializeField]
    private int bonusMoney = 1000;
    #endregion

    #region Normal paramters
    private List<Fish> activeFishes;
    private List<Money> activeMoney;
    private List<SwordFish> activeSwordFishes;

    private UIManager ui;
    private DataManager data;

    private KitManager kitManager;
    private EnemyManager enemyManager;
    private CameraController cameraController;
    private Player player;

    private bool isRemoveAds;

    private float gameTimes;
    #endregion

    #region Encapsulate
    public GameState GameState
    {
        get
        {
            return gameState;
        }

        set
        {
            gameState = value;
        }
    }

    public int SkipSeconds
    {
        get
        {
            return skipSeconds;
        }

        set
        {
            skipSeconds = value;
        }
    }

    public int BonusMoney
    {
        get
        {
            return bonusMoney;
        }

        set
        {
            bonusMoney = value;
        }
    }
    #endregion

    public override void Initialize()
    {
        //if (!fishy.gameObject.activeInHierarchy)
        //{
        //    fishy = Instantiate(fishy);
        //    fishy.Initialize();

        //    fishy.transform.SetParent(fishContainer);
        //}

        Application.targetFrameRate = 60;

        gameState = GameState.Start;

        activeFishes = new List<Fish>();
        activeMoney = new List<Money>();
        activeSwordFishes = new List<SwordFish>();

        data = DataManager.Instance;
        ui = UIManager.Instance;

        kitManager = KitManager.Instance;
        enemyManager = EnemyManager.Instance;
        cameraController = CameraController.Instance;

        isRemoveAds = DataManager.Instance.GetInt(REMOVE_ADS_KEY, 0) == 1;

        SoundManager.Instance.PlayBGM(SFXType.StartGameBackgroundSound);
        AdsManager.Instance.OnRewardVideoCallback += OnRewardVideoCallback;

    }

    public void Release()
    {
        ChangeState(GameState.Start);

        StopAllCoroutines();

        for (int i = 0; i < activeFishes.Count; i++)
        {
            var f = activeFishes[i];
            FishManager.Instance.RecycleFish(f);
        }
        activeFishes.Clear();

        for (int i = 0; i < activeMoney.Count; i++)
        {
            var m = activeMoney[i];
            MoneyManager.Instance.RecycleMoney(m);
        }
        activeMoney.Clear();

        for (int i = 0; i < activeSwordFishes.Count; i++)
        {
            var f = activeSwordFishes[i];
            FishManager.Instance.RecycleSwordFish(f);
        }
        activeSwordFishes.Clear();


        fishContainer.localPosition = Vector3.zero;
        fishContainer.localRotation = Quaternion.identity;
        fishContainer.localScale = Vector3.one;

        //fishy.Refresh();
        background.Release();
        cameraController.Release();
        player.Release();
        kitManager.Release();
        enemyManager.Release();
    }

    public void Refresh()
    {

    }

    public void UpdateStep(float deltaTime)
    {
        if (gameState == GameState.Play && isRevived == false)
        {
            UpdateInput();
            //fishy.UpdateStep(deltaTime);

            //for (int i = 0; i < activeFishes.Count; i++)
            //{
            //    var f = activeFishes[i];
            //    f.UpdateStep(deltaTime);

            //    if (!f.gameObject.activeInHierarchy)
            //    {
            //        activeFishes.Remove(f);
            //        i--;

            //        FishManager.Instance.RecycleFish(f);
            //    }
            //}

            gameTimes += deltaTime;

            if((int)gameTimes > data.GameTime)
            {
                data.GameTime = (int)gameTimes;

                data.IncreaseTrackingData("TimePlayed", 1);
                data.IncreaseTrackingData("DailyTimePlayed", 1);
                data.IncreaseTrackingData("TotalTimePlayed", 1);
            }

            for (int i = 0; i < activeMoney.Count; i++)
            {
                var m = activeMoney[i];
                m.UpdateStep(deltaTime);

                if (!m.gameObject.activeInHierarchy)
                {
                    activeMoney.Remove(m);
                    i--;

                    MoneyManager.Instance.RecycleMoney(m);
                }
            }

            //for (int i = 0; i < activeSwordFishes.Count; i++)
            //{
            //    var f = activeSwordFishes[i];
            //    f.UpdateStep(deltaTime);

            //    if (!f.gameObject.activeInHierarchy)
            //    {
            //        activeSwordFishes.Remove(f);
            //        i--;
            //    }
            //}


            player.UpdateStep(deltaTime);
            cameraController.UpdateStep(deltaTime);
            kitManager.UpdateStep(deltaTime);
            enemyManager.UpdateStep(deltaTime);

            ui.UpdateExpFill(player.GetXPPercent());
        }
    }

    public void UpdateInput()
    {
#if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.K))
        {
            Player.Instance.LevelUp();
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            EndGame();
        }

        if (Input.GetKeyUp(KeyCode.R))
        {
            RetryGame();
        }
#endif
    }

    public void PlayGame()
    {
        ChangeState(GameState.Play);

        //int totalLevel = 2;
        //int totalFish = 300;
        //for (int i = 0; i < totalLevel; i++)
        //{
        //    int count = totalFish / totalLevel;
        //    for (int j = 0; j < count; j++)
        //    {
        //        var f = FishManager.Instance.GenerateFishByLevel(i + 1);
        //        var position = new Vector3(UnityEngine.Random.Range(-2, 2), UnityEngine.Random.Range(-5, 5));
        //        var rotation = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

        //        f.transform.localPosition = position;
        //        f.transform.localEulerAngles = rotation;

        //        f.Refresh();

        //        activeFishes.Add(f);
        //    }
        //}

        //for (int i = 0; i < activeFishes.Count; i++)
        //{
        //    var f = activeFishes[i];
        //    FishManager.Instance.RecycleFish(f);
        //}
        //activeFishes.Clear();

        for (int i = 0; i < activeMoney.Count; i++)
        {
            var m = activeMoney[i];
            MoneyManager.Instance.RecycleMoney(m);
        }
        activeMoney.Clear();

        //for (int i = 0; i < activeSwordFishes.Count; i++)
        //{
        //    var f = activeSwordFishes[i];
        //    FishManager.Instance.RecycleSwordFish(f);
        //}
        //activeSwordFishes.Clear();


        //fishContainer.localPosition = Vector3.zero;
        //fishContainer.localRotation = Quaternion.identity;
        //fishContainer.localScale = Vector3.one;

        //StartCoroutine(C_Test());
        //StartCoroutine(C_RANDOM());
        //reset score
        data.Score = 0;

        ui.UpdateScore(data.Score);
        ui.UpdateMoney(data.Money);

        cameraController.Refresh();
        player.Refresh();
        player.ReenableCollison();
        kitManager.Refresh();
        enemyManager.Refresh();

        AlertEffect.Instance.Refresh();
        IncreaseScoreEffect.Instance.Refresh();

        StartCoroutine(C_RANDOM());
        StartCoroutine(C_RandomChasingFish()); 
    }

    public void PauseGame()
    {
        ChangeState(GameState.Pause);
    }

    public void ResumeGame()
    {
        ChangeState(GameState.Play);
    }

    public void RetryGame()
    {
        StopAllCoroutines();

        // reset param
        //fishy.Refresh();
        background.Refresh();

        Camera.main.transform.localPosition = new Vector3(0, 0, -10);

        PlayGame();

        data.SetTrackingData("FishEaten", 0);
        data.SetTrackingData("TimePlayed", 0);
        data.SetTrackingData("CoinCollected", 0);
        data.SetTrackingData("XPGained", 0);
    }

    public void EndGame()
    {
        if (isRevived)
        {
            isRevived = false;

            ChangeState(GameState.End);

            StopAllCoroutines();

            if (data.Score > data.HighScore)
            {
                SoundManager.Instance.PlaySFX(SFXType.NewHighscore);
                data.HighScore = data.Score;
            }

            data.EggEXP += data.Score;
            if (data.EggEXP > data.TotalEggEXP)
            {
                data.EggEXP = data.TotalEggEXP;
            }           

            data.PlayerXP += data.Score;
            data.TotalPlayerXP += data.Score;

            data.TotalGameTime += data.GameTime;
            data.GameTime = 0;

            data.IncreaseTrackingData("TimePlayed", 0);
            data.IncreaseTrackingData("DailyTimePlayed", 0);
            data.IncreaseTrackingData("TotalTimePlayed", 0);

            data.IncreaseTrackingData("XPGained", data.Score);
            data.IncreaseTrackingData("DailyXPGained", data.Score);
            data.IncreaseTrackingData("TotalXPGained", data.Score);

            data.Save();

           if (!isRemoveAds)
               AdsManager.Instance.ShowInterstitialAd();

            player.ResetRotation();

            UIManager.Instance.EndGame();
        }
        else
        {
            isRevived = true;

            revivePrice = player.GetReviveXP();
            
            bool activeWacthAds = AdsManager.Instance.IsRewardVideoAdReady();
            bool activeXP = data.PlayerXP >= revivePrice;

            if (activeWacthAds || activeXP)
            {
                ChangeState(GameState.Revive);
                PopupController.Instance.ShowRevivePopup(reviveTImes, activeWacthAds, activeXP, revivePrice);
            }
            else
            {
                // not enough condition to revive
                EndGame();
            }
        }
    }

    public void ChangeState(GameState state)
    {
        if (gameState == state)
            return;

        gameState = state;
    }

    public void HandleCollideWithPlayer(Fish fish)
    {
        if (fishy.Level >= fish.Level)
        {
            fishy.Eat(fish);
            fish.Dead();

            IncreaseScore(fish.Score);
        }
        else
        {
            fishy.Dead();
        }
    }

    public void Update()
    {
        UpdateStep(Time.deltaTime);
    }

    private IEnumerator C_Test()
    {
        int totalLevel = 8;
        int totalFish = 150;

        while (true)
        {
            if (gameState == GameState.Play)
            {
                RandomFish(UnityEngine.Random.Range(1, totalLevel));

                int bonusFish = UnityEngine.Random.Range(0, totalLevel - fishy.Level);

                for (int i = 0; i < bonusFish; i++)
                {
                    RandomFish(fishy.Level);
                }

                if (UnityEngine.Random.Range(0, 100) < 10)
                {
                    RandomMoney(fishy.Level);
                }

                yield return new WaitForSeconds(0.5f);
            }
            else
            {
                yield return null;
            }

        }
    }

    public void RandomFish(int level)
    {
        var fishyAngleZ = fishy.transform.localEulerAngles.z;
        if (fishyAngleZ < 0)
            fishyAngleZ += 360;

        var f = FishManager.Instance.GenerateFishByLevel(level);
        var rotation = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

        var fPositionAngleZ = UnityEngine.Random.Range(fishyAngleZ - 90, fishyAngleZ + 90);
        var position = new Vector3(
            Mathf.Cos(fPositionAngleZ * Mathf.Deg2Rad) * (7.5f / fishContainer.localScale.x) + fishy.transform.localPosition.x,
            Mathf.Sin(fPositionAngleZ * Mathf.Deg2Rad) * (7.5f / fishContainer.localScale.x) + fishy.transform.localPosition.y
            );

        f.transform.SetParent(fishContainer, false);

        f.transform.localPosition = position;
        f.transform.localEulerAngles = rotation;

        f.Refresh();

        activeFishes.Add(f);
    }

    public void ScaleWorld()
    {
        fishContainer.DOScale(fishContainer.localScale.x * 0.75f, 1.0f).Play();
        fishy.ScaleUpSpeed(1 / 0.75f);
    }

    public void IncreaseScore(int bonus)
    {
        data.Score += bonus;
        ui.UpdateScore(data.Score);
    }

    public void IncreaseMoney(int bonus)
    {
        data.Money += bonus;
        ui.UpdateMoney(data.Money);

        data.IncreaseTrackingData("CoinCollected", bonus);
        data.IncreaseTrackingData("DailyCoinCollected", bonus);
        data.IncreaseTrackingData("TotalCoinCollected", bonus);
    }

    public void RandomMoney(int level)
    {
        var fishyAngleZ = player.transform.localEulerAngles.z;
        if (fishyAngleZ < 0)
            fishyAngleZ += 360;

        int moneyCount = UnityEngine.Random.Range(1, 3);
        bool bVerticalAlign = UnityEngine.Random.Range(0, 1) > 0 ? true : false;

        var fPositionAngleZ = UnityEngine.Random.Range(fishyAngleZ - 90, fishyAngleZ + 90);
        var position = new Vector3(
            Mathf.Cos(fPositionAngleZ * Mathf.Deg2Rad) * (7.5f / fishContainer.localScale.x) + player.transform.localPosition.x,
            Mathf.Sin(fPositionAngleZ * Mathf.Deg2Rad) * (7.5f / fishContainer.localScale.x) + player.transform.localPosition.y
            );

        for (int i = 0; i < moneyCount; i++)
        {
            var m = MoneyManager.Instance.GenerateMoney();
            m.transform.localPosition = position;

            if (bVerticalAlign)
                position += Vector3.down;
            else
                position += Vector3.right;

            activeMoney.Add(m);
        }

    }

    public void RandomSwordFish()
    {
        var f = enemyManager.GenerateSwordFish();

        f.OriginalPosition = player.transform.position;
        f.Size = player.transform.localScale.x;
        f.Speed = player.GetRealSpeed() * 3;

        f.ApplyChanges();
    }

    public IEnumerator C_RANDOM()
    {
        var waitTime = new WaitForSeconds(5.0f);

        yield return waitTime;

        while (true)
        {
            if (gameState == GameState.Play)
            {
                if (UnityEngine.Random.Range(0, 2) > 0)
                    RandomSwordFish();

                if (UnityEngine.Random.Range(0, 4) > 0)
                    RandomGoldenFish();

                RandomShadowFish();
                RandomMoney(player.Level);

                yield return waitTime;
            }
            else
            {
                yield return null;
            }

        }
    }

    public IEnumerator C_RandomChasingFish()
    {
        var waitTime = new WaitForSeconds(30.0f);
        const int maxCount = 4;

        while (true)
        {
            int count = 1 + Player.Instance.Level / Player.Instance.MajorTransformationIntervals;

            if (count > maxCount)
                count = maxCount;

            for (int i = 0; i < count; i++)
            {
                RandomChasingFish();

                yield return new WaitForSeconds(2.0f);
            }

            yield return waitTime;

        }
    }

    public void ChangeBackground()
    {
        background.Change(RemoveSmallFishes);
    }

    public void RemoveSmallFishes()
    {
        //int fishyLevel = fishy.Level;

        //for (int i = 0; i < activeFishes.Count; i++)
        //{
        //    var f = activeFishes[i];
        //    if (f.Level < fishyLevel)
        //    {
        //        activeFishes.Remove(f);
        //        i--;

        //        FishManager.Instance.RecycleFish(f);
        //    }
        //}

        EnemyManager.Instance.FadedEnemiesUnderLevel(player.Level);
    }

    public void GameStart()
    {
        player = CharacterManager.Instance.GetCurrentPlayer();
        player.Initialize();
        PlayGame();

        Tank.Instance.ChangeState(TankState.IDLE);

        SoundManager.Instance.PlayBGM(SFXType.DuringGameBackgroundSound);
    }

    public void GameEnd()
    {
        Destroy(player.gameObject);
        Release();

        cameraController.Refresh();

        Tank.Instance.ChangeState(TankState.TankUI);

        SoundManager.Instance.PlayBGM(SFXType.StartGameBackgroundSound);

    }

    public IEnumerator C_RandomShadowFish()
    {
        while (true)
        {
            if (gameState == GameState.Play)
            {
                RandomShadowFish();
                yield return new WaitForSeconds(5.0f);
            }
            else
            {
                yield return null;
            }

        }
    }

    public void RandomShadowFish()
    {
        var f = enemyManager.GenerateShadowFish();

        Vector3 fPosition = UnityEngine.Random.insideUnitCircle * CameraController.Instance.TargetOrthographicSize;
        fPosition += player.transform.position;

        f.transform.position = fPosition;
    }

    public void RandomGoldenFish()
    {
        var f = enemyManager.GenerateGoldenFish();
        f.Level = player.Level;
        f.Speed = player.GetRealSpeed() * 0.9f;

        Vector3 fPosition = UnityEngine.Random.insideUnitCircle * CameraController.Instance.TargetOrthographicSize;
        fPosition += player.transform.position;

        f.transform.position = fPosition;
    }

    public void RandomChasingFish()
    {
        var f = enemyManager.GenerateChasingFish();
        f.Level = player.Level + 1;
        f.Speed = player.GetRealSpeed() * 0.75f;

        Vector3 fPosition = UnityEngine.Random.onUnitSphere * CameraController.Instance.TargetOrthographicSize * 2;
        fPosition.z = 0;

        fPosition += player.transform.position;

        f.transform.position = fPosition;
    }

    public void HandleIAPPurchase(string itemID)
    {
        switch (itemID)
        {
            case "gopencils.games.fishy.polly.coins1":
                data.Money += 1200;
                break;
            case "gopencils.games.fishy.polly.coins2":
                data.Money += 3800;
                break;
            case "gopencils.games.fishy.polly.coins3":
                data.Money += 13200;
                break;
            case "gopencils.games.fishy.polly.coins4":
                data.Money += 46800;
                break;
            case "gopencils.games.fishy.polly.coins5":
                data.Money += 144000;
                break;
            case "gopencils.games.fishy.polly.removeads":
                DataManager.Instance.SetInt(REMOVE_ADS_KEY, 1);
                DataManager.Instance.Save();
                break;
            default:
                break;
        }

        data.Save();
    }

    /// <summary>
    /// Daily Reward
    /// </summary>

    void OnEnable()
    {
        DailyRewards.onClaimPrize += OnClaimPrizeDailyRewards;
    }

    void OnDisable()
    {
        DailyRewards.onClaimPrize -= OnClaimPrizeDailyRewards;
    }

    // this is your integration function. Can be on Start or simply a function to be called
    public void OnClaimPrizeDailyRewards(int day)
    {
        // This returns a Reward object
        Reward myReward = DailyRewards.instance.GetReward(day);

        //Add bonus Money after claim Reward.
        data.Money += myReward.reward;
        ui.UpdateMoney(data.Money);
    }

    private void OnRewardVideoCallback(RewardVideoType type, RewardVideoResult result)
    {
        // still accept due to server side issues
        if (result == RewardVideoResult.Finished || result == RewardVideoResult.Skipped)
        {
            switch (type)
            {
                case RewardVideoType.Money:
                    data.Money += bonusMoney;
                    data.Save();
                    break;
                case RewardVideoType.Revive:
                    Revive();
                    break;
                case RewardVideoType.Unlock:
                    break;
                case RewardVideoType.Hatch:
                    break;
                default:
                    break;
            }
        }
    }

    public void ReviveAds()
    {
        // show ads with callback
       AdsManager.Instance.ShowRewardVideoAd(RewardVideoType.Revive);
    }

    public void ReviveXP()
    {
        data.PlayerXP -= revivePrice;
        // active number effect here 

        Revive();
    }

    public void Revive()
    {
        var effect = ParticleEffect.Instance.GenerateParticle(ParticleType.LevelUp);
        //effect.transform.position = player.transform.position;
        CameraController.Instance.Shake(0.5f, 0.025f);
        ChangeState(GameState.Play);

        player.Revive();
        player.ReenableCollison();

        isRevived = false;
    }

    public void MoneyAds()
    {
//        AdsManager.Instance.ShowRewardVideoAd(RewardVideoType.Money);
    }


}

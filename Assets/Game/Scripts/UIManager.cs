﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using FTRuntime;

public class UIManager : MonoSingleton<UIManager>
{

    #region Const parameters
    private const string XP_TEXT_FORMAT = "{0} XP";
    private const string MONEY_TEXT_FORMAT = "{0} <sprite=0>";

    private const string BACKGROUND_MUSIC_KEY = "BackgroundMusic";
    private const string EFFECT_SOUND_KEY = "EffectSound";
    #endregion

    #region Editor paramters
    [SerializeField]
    private GameObject startGameUI;
    [SerializeField]
    private GameObject inGameUI;
    [SerializeField]
    private GameObject endGameUI;
    [SerializeField]
    private GameObject pauseGameUI;
    [SerializeField]
    private GameObject settingMenuUI;
    [SerializeField]
    private GameObject logoUI;
    [SerializeField]
    private GameObject HUD;

    [SerializeField]
    private List<RectTransform> listScreens;

    [SerializeField]
    private GameObject btnSettings;

    [SerializeField]
    private Text txtScore;
    [SerializeField]
    private Text txtMoney;

    [SerializeField]
    private Text txtCurrentScore;
    [SerializeField]
    private Text txtHighScore;

    [SerializeField]
    private Image expFill;

    [SerializeField]
    private Slider sliderEggEXP;
    [SerializeField]
    private Text txtEggEXP;
    [SerializeField]
    private ToggleGroup toggleGroup;

    [SerializeField]
    private List<Toggle> listToggles;

    [SerializeField]
    private Animator animatorEndGame;

    [SerializeField]
    private GameObject waveMesh;
    [SerializeField]
    private SwfClipController swfClipController;
    [SerializeField]
    private TMPro.TextMeshProUGUI tmpXP;
    [SerializeField]
    private TMPro.TextMeshProUGUI tmpMoney;

    [Header("Sound config")]
    [SerializeField]
    private Button btnActiveBGM;
    [SerializeField]
    private Button btnDeactiveBGM;
    [SerializeField]
    private Button btnActiveSFX;
    [SerializeField]
    private Button btnDeactiveSFX;
    #endregion

    #region Normal paramters
    private RectTransform rectTransform;

    private bool isActiveBGM;
    private bool isActiveSFX;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        ShowHUD();
        ShowStartGameUI();
        ShowOnlyPlayUI();
        btnSettings.SetActive(true);
        HideSettingsMenu();
        HideInGameUI();
        HideEndGameUI();

        //Reset to PlayUI Screen
        startGameUI.transform.localPosition = Vector3.zero;

        rectTransform = gameObject.GetComponent<RectTransform>();

        for (int i = 0; i < listScreens.Count; i++)
        {
            listScreens[i].anchoredPosition = new Vector3(1440 * (i - 2), 0);
        }

        tmpXP.text = string.Format(XP_TEXT_FORMAT, DataManager.Instance.PlayerXP);

        LoadSoundConfig();
    }

    public void Release()
    {
        
    }

    public void ShowOnlyPlayUI()
    {
        foreach (var screen in listScreens)
        {
            if (screen.tag == "PlayUI")
            {
                screen.gameObject.SetActive(true);
            }
            else
            {
                screen.gameObject.SetActive(false);
            }
        }
    }

    public void ShowStartGameUI()
    {
        startGameUI.SetActive(true);
        toggleGroup.allowSwitchOff = false;
        waveMesh.SetActive(true);
        txtMoney.text = DataManager.Instance.Money.ToString();
        tmpMoney.text = string.Format(MONEY_TEXT_FORMAT, DataManager.Instance.Money);
    }

    public void HideStartGameUI()
    {
        startGameUI.SetActive(false);
        waveMesh.SetActive(false);
    }

    public void ShowInGameUI()
    {
        inGameUI.SetActive(true);
    }

    public void HideInGameUI()
    {
        inGameUI.SetActive(false);
    }

    public void ShowEndGameUI()
    {
        endGameUI.SetActive(true);
    }

    public void HideEndGameUI()
    {
        endGameUI.SetActive(false);
    }

    public void PlayGame()
    {
        HideHUD();
        HideStartGameUI();
        ShowInGameUI();
        HideEndGameUI();
        btnSettings.SetActive(false);
        GameManager.Instance.GameStart();
    }

    public void RetryGameWithAnimation()
    {
        animatorEndGame.SetTrigger("Hide");
        Invoke("RetryGame", animatorEndGame.GetCurrentAnimatorStateInfo(0).length);
    }

    public void RetryGame()
    {
        HideStartGameUI();
        ShowInGameUI();
        HideEndGameUI();

        GameManager.Instance.RetryGame();
    }

    public void ShareGame()
    {
        Application.OpenURL(AppInfo.Instance.FACEBOOK_LINK);
    }

    public void RateGame()
    {
#if UNITY_ANDROID
        Application.OpenURL(AppInfo.Instance.PLAYSTORE_LINK);
#elif UNITY_IOS
        Application.OpenURL(AppInfo.Instance.APPSTORE_LINK);
#endif
    }

    public void EndGame()
    {
        HideStartGameUI();
        HideInGameUI();
        ShowEndGameUI();

        txtCurrentScore.text = DataManager.Instance.Score.ToString();
        txtHighScore.text = "BEST " + DataManager.Instance.HighScore.ToString();

        sliderEggEXP.value = DataManager.Instance.EggEXP * 1.0f / DataManager.Instance.TotalEggEXP;
        txtEggEXP.text = string.Format("{0}/{1}", DataManager.Instance.EggEXP, DataManager.Instance.TotalEggEXP);
    }

    public void UpdateScore(int score)
    {
        txtScore.text = score.ToString();
    }

    public void UpdateMoney(int money)
    {
        txtMoney.text = money.ToString();
        tmpMoney.text = string.Format(MONEY_TEXT_FORMAT, money);
    }

    public void UpdateExpFill(float percent)
    {
        expFill.fillAmount = percent;
    }

    public void UpdateXP(int xp)
    {
        tmpXP.text = string.Format(XP_TEXT_FORMAT, xp);
    }

    public void PauseGame()
    {
        GameManager.Instance.PauseGame();
        HideInGameUI();
        ShowPauseGameUI();
    }

    public void ShowSettingsMenu()
    {
        settingMenuUI.SetActive(true);
        logoUI.SetActive(false);
    }

    public void HideSettingsMenu()
    {
        settingMenuUI.SetActive(false);
        logoUI.SetActive(true);
    }

    public void ShowPauseGameUI()
    {
        pauseGameUI.SetActive(true);
    }

    public void HidePauseGameUI()
    {
        pauseGameUI.SetActive(false);
    }

    public void Resume()
    {
        GameManager.Instance.ResumeGame();

        HidePauseGameUI();
        ShowInGameUI();
    }

    public void Restart()
    {
        //GameManager.Instance.EndGame();
        GameManager.Instance.RetryGame();
        HidePauseGameUI();
        ShowInGameUI();
    }

    public void Exit()
    {
        //GameManager.Instance.EndGame();
        GameManager.Instance.GameEnd();

        ShowHUD();
        ShowStartGameUI();
        btnSettings.SetActive(true);
        HideInGameUI();
        HidePauseGameUI();
    }

    public void ToggleSoundEffect()
    {
        isActiveSFX = !isActiveSFX;

        SoundManager.Instance.ToggleSFX(!isActiveSFX);
        btnActiveSFX.gameObject.SetActive(isActiveSFX);
        btnDeactiveSFX.gameObject.SetActive(!isActiveSFX);

        SaveSoundConfig();
    }

    public void ToggleMusicBackground()
    {
        isActiveBGM = !isActiveBGM;

        SoundManager.Instance.ToggleBGM(!isActiveBGM);
        btnActiveBGM.gameObject.SetActive(isActiveBGM);
        btnDeactiveBGM.gameObject.SetActive(!isActiveBGM);

        SaveSoundConfig();
    }

    public void NormalizeToggle(GameObject toggleObj)
    {
        Toggle toggle = toggleObj.GetComponent<Toggle>();
        Animator animator = toggleObj.GetComponent<Animator>();
        if (!toggle.isOn)
        {
            animator.SetTrigger("ToggleOff");
        }
    }

    /*    //Move up toggle when it's on.
        public void ControlToggleGroup()
        {
            foreach (var toggle in listToggles)
            {
                if (toggle.isOn)
                {
                    toggle.transform.DOLocalMoveY(20.0f, 0.1f, false);
                    toggle.transform.DOScale(1.2f, 0.2f);
                }
                else
                {
                    toggle.transform.DOLocalMoveY(0.0f, 0.1f, false);
                    toggle.transform.DOScale(1.0f, 0.2f);
                }
            }
        }*/

    public void ShowHUD()
    {
        HUD.SetActive(true);
    }

    public void HideHUD()
    {
        HUD.SetActive(false);
    }

    public void TransitionScreen(Transform gameObj)
    {
        //Play Game when PLayUI is already shown
        if (gameObj.tag == "PlayUI")
        {
            btnSettings.SetActive(true);
            if (gameObj.gameObject.activeSelf)
            {
                PlayGame();
            }
        }
        else
        {
            btnSettings.SetActive(false);
        }
        foreach (var screen in listScreens)
        {
            if (gameObj.tag == screen.tag)
            {
                screen.gameObject.SetActive(true);
                startGameUI.transform.DOLocalMoveX(-gameObj.localPosition.x, 0.3f, false);
            }
            else
            {
                screen.gameObject.SetActive(false);
            }
        }

        if (gameObj.CompareTag("PlayUI"))
        {
            CameraController.Instance.LookPosition(Tank.Instance.GetPlayContainerPosition());
            swfClipController.Play(true);
        }
        else if (gameObj.CompareTag("FishUI"))
        {
            CameraController.Instance.LookPosition(Tank.Instance.GetFishContainerPosition());
        }
        else if (gameObj.CompareTag("HatchUI"))
        {
            CameraController.Instance.LookPosition(Tank.Instance.GetPodiumContainerPosition());
        }
        else if (gameObj.CompareTag("TankUI"))
        {
            CameraController.Instance.LookPosition(Tank.Instance.GetSwimmingFishContainerPosition());
        }
        else if (gameObj.CompareTag("ShopUI"))
        {
            CameraController.Instance.LookPosition(Tank.Instance.GetShopContainerPosition());
        }

    }

    public RectTransform GetRectTransform()
    {
        return rectTransform;
    }

    private void LoadSoundConfig()
    {
        // try to get background music is on or off (1 -> on , 0 -> off)
        isActiveBGM = DataManager.Instance.GetInt(BACKGROUND_MUSIC_KEY, 1) == 1 ? true : false;

        SoundManager.Instance.ToggleBGM(!isActiveBGM);
        btnActiveBGM.gameObject.SetActive(isActiveBGM);
        btnDeactiveBGM.gameObject.SetActive(!isActiveBGM);


        // try to get effect sound is on or off (1 -> on , 0 -> off)
        isActiveSFX = DataManager.Instance.GetInt(EFFECT_SOUND_KEY, 1) == 1 ? true : false;

        SoundManager.Instance.ToggleSFX(!isActiveSFX);
        btnActiveSFX.gameObject.SetActive(isActiveSFX);
        btnDeactiveSFX.gameObject.SetActive(!isActiveSFX);
    }

    private void SaveSoundConfig()
    {
        DataManager.Instance.SetInt(BACKGROUND_MUSIC_KEY, isActiveBGM ? 1 : 0);
        DataManager.Instance.SetInt(EFFECT_SOUND_KEY, isActiveSFX ? 1 : 0);
    }

    public void OpenShop()
    {
        TransitionScreen(listScreens[4]);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggCounter
{
    public EggType type;
    public int count;
}

public class EggManager : MonoSingleton<EggManager>
{

    #region Const parameters
    private const string CLAIMED_EGG_COUNT_KEY = "ClaimedEggsCount";
    private const string NEXT_CLAIM_EGG_TYPE_KEY = "NextClaimEggType";
    private const string HATCHING_EGGS_COUNT = "HatchingEggsCount";
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<Egg> prototypeEggs;
    [SerializeField]
    private int claimedEggsCount;
    [SerializeField]
    private EggType nextClaimEggType;
    [SerializeField]
    private List<Sprite> eggs;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        Load();
    }

    public void Load()
    {
        //claimedEggsCount = PlayerPrefs.GetInt(CLAIMED_EGG_COUNT_KEY, 0);
        //nextClaimEggType = (EggType)PlayerPrefs.GetInt(NEXT_CLAIM_EGG_TYPE_KEY, 0);

        //int hatchingEggsCount = PlayerPrefs.GetInt(HATCHING_EGGS_COUNT, 0);

        claimedEggsCount = DataManager.Instance.GetInt(CLAIMED_EGG_COUNT_KEY, 0);
        nextClaimEggType = (EggType)DataManager.Instance.GetInt(NEXT_CLAIM_EGG_TYPE_KEY, 0);

        int hatchingEggsCount = DataManager.Instance.GetInt(HATCHING_EGGS_COUNT, 0);
    }

    public void Save()
    {
        DataManager.Instance.SetInt(CLAIMED_EGG_COUNT_KEY, claimedEggsCount);
        DataManager.Instance.SetInt(NEXT_CLAIM_EGG_TYPE_KEY, (int)nextClaimEggType);

        DataManager.Instance.Save();
    }

    private string GetHatchingEggKey(int id)
    {
        return "HatchingEgg_" + id;
    }

    public int GetNextClaimEggEXP()
    {
        return (claimedEggsCount + 1) * 1000;
    }

    public Egg ClaimNextEgg()
    {
        Egg egg = new Egg();
        for (int i = 0; i < prototypeEggs.Count; i++)
        {
            var originalEgg = prototypeEggs[i];
            if (originalEgg.Type == nextClaimEggType)
            {
                egg.Type = originalEgg.Type;
                egg.HatchTimes = originalEgg.HatchTimes;
                egg.Sprite = originalEgg.Sprite;

                egg.Claim();
            }
        }

        claimedEggsCount++;
        nextClaimEggType++;
        if (nextClaimEggType == EggType.TOTAL)
            nextClaimEggType = EggType.Common;

        if (!CharacterManager.Instance.HasLockedCharacter(nextClaimEggType))
        {
            nextClaimEggType = TryToGetEggType();
        }

        DataManager.Instance.EggEXP = 0;
        DataManager.Instance.TotalEggEXP = GetNextClaimEggEXP();
        DataManager.Instance.Save();

        Save();

        return egg;
    }

    public Sprite GetSprite(EggType type)
    {
        var id = (int)type;

        if (id >= eggs.Count)
            return eggs[0];
        else
            return eggs[id];
    }

    public EggType TryToGetEggType()
    {
        int count = (int)EggType.TOTAL;

        for (int i = 0; i < count; i++)
        {
            EggType type = (EggType)i;
            if (CharacterManager.Instance.HasLockedCharacter(type))
            {
                return type;
            }
        }

        return EggType.IDLE;
    }

    public bool CanClaimEgg()
    {
        // check for new egg
        if(nextClaimEggType == EggType.IDLE)
        {
            nextClaimEggType = TryToGetEggType();
        }
        else
        {
            if (CharacterManager.Instance.HasLockedCharacter(nextClaimEggType))
                return true;
            else
            {
                nextClaimEggType = TryToGetEggType();
            }
        }

        // no new egg
        if (nextClaimEggType == EggType.IDLE)
        {
            return false;
        }

        return false;
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public enum EggType
{
    Common,
    Rare,
    Epic,
    Spooky,

    TOTAL,
    IDLE
}

[System.Serializable]
public class Egg
{

    #region Const parameters
    [SerializeField]
    private EggType type;

    [SerializeField]
    private float hatchTimes;
    [SerializeField]
    private float skipTimes = 0;

    [SerializeField]
    [System.NonSerialized]
    private DateTime claimDateTime;
    [SerializeField]
    [System.NonSerialized]
    private DateTime hatchDateTime;
    #endregion

    #region Editor paramters
    [SerializeField]
    private int Id;
    [SerializeField]
    private int podiumID;
    [SerializeField]
    private string unlockTime;

    [SerializeField]
    private Sprite sprite;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    public EggType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public float HatchTimes
    {
        get
        {
            return hatchTimes;
        }

        set
        {
            hatchTimes = value;
        }
    }

    public DateTime ClaimDateTime
    {
        get
        {
            return claimDateTime;
        }

        set
        {
            claimDateTime = value;
        }
    }

    public DateTime HatchDateTime
    {
        get
        {
            return hatchDateTime;
        }

        set
        {
            hatchDateTime = value;
        }
    }

    public int ID
    {
        get
        {
            return Id;
        }

        set
        {
            Id = value;
        }
    }

    public int PodiumID
    {
        get
        {
            return podiumID;
        }

        set
        {
            podiumID = value;
        }
    }

    public Sprite Sprite
    {
        get
        {
            return sprite;
        }

        set
        {
            sprite = value;
        }
    }
    #endregion

    public void Initialize()
    {
        claimDateTime = DateTime.Parse(unlockTime);
        hatchDateTime = claimDateTime.AddSeconds(hatchTimes);

        sprite = EggManager.Instance.GetSprite(type);
    }

    public void Release()
    {
    }

    public void Refresh()
    {
    }

    public void UpdateStep(float deltaTime)
    {

    }

    public void Claim()
    {
        claimDateTime = System.DateTime.Now;
        hatchDateTime = System.DateTime.Now.AddSeconds(hatchTimes);
    }

    public void Hatch()
    {

    }

    public string Serialize()
    {
        unlockTime = claimDateTime.ToString();
        return JsonUtility.ToJson(this);
    }

    public TimeSpan GetTimeSpanLeft()
    {
        return hatchDateTime.AddSeconds(-skipTimes) - System.DateTime.Now;
    }

    public void AddSkipTime(float secondToSkip)
    {
        skipTimes += secondToSkip;
    }

}

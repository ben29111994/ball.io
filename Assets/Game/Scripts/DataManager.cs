﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoSingleton<DataManager>
{

    #region Const parameters
    private const string SCORE_KEY = "Score";
    private const string HIGH_SCORE_KEY = "HighScore";
    private const string MONEY_KEY = "Money";
    private const string TOTAL_MONEY_KEY = "TotalMoney";
    private const string EGG_EXP_KEY = "EggEXP";
    private const string TOTAL_EGG_EXP_KEY = "TotalEggEXP";
    private const string PLAYER_XP_KEY = "PlayerXP";
    private const string TOTAL_PLAYER_XP_KEY = "TotalPlayerXP";
    private const string FOOD_KEY = "Food";
    private const string LOGIN_DATE_TIME_KEY = "LoginDateTime";

    private const string LIST_DAILY_EVENT_COMPLETED = "ListDailyEventCompleted";
    private const string LIST_DAILY_EVENT_COLLECTED = "ListDailyEventCollected";
    #endregion

    #region Editor paramters
    [SerializeField]
    private int score;
    [SerializeField]
    private int highScore;
    [SerializeField]
    private int money;
    [SerializeField]
    private int totalMoney;
    [SerializeField]
    private int eggEXP;
    [SerializeField]
    private int totalEggEXP;
    [SerializeField]
    private int playerXP;
    [SerializeField]
    private int totalPlayerXP;

    [SerializeField]
    private int food;

    [SerializeField]
    private int gameTime;
    [SerializeField]
    private int totalGameTime;

    [SerializeField]
    private List<string> trackingKeys;
    [SerializeField]
    private TextAsset propertyCSV;
    [SerializeField]
    private TextAsset achievementCSV;
    #endregion

    #region Normal paramters
    private Dictionary<string, int> temporaryData;
    private Dictionary<string, int> dailyData;
    private Dictionary<string, int> totalData;
    private Dictionary<string, int> trackingData;

    private System.DateTime loginDateTime;
    private bool refreshData;

    private List<string> listDailyEventCompleted;
    private List<string> listDailyEventCollected;
    #endregion

    #region Encapsulate
    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int HighScore
    {
        get
        {
            return highScore;
        }

        set
        {
            highScore = value;
        }
    }

    public int Money
    {
        get
        {
            return money;
        }

        set
        {
            money = value;
            UIManager.Instance.UpdateMoney(money);
        }
    }

    public int TotalMoney
    {
        get
        {
            return totalMoney;
        }

        set
        {
            totalMoney = value;
        }
    }

    public int EggEXP
    {
        get
        {
            return eggEXP;
        }

        set
        {
            eggEXP = value;
        }
    }

    public int TotalEggEXP
    {
        get
        {
            return totalEggEXP;
        }

        set
        {
            totalEggEXP = value;
        }
    }

    public int PlayerXP
    {
        get
        {
            return playerXP;
        }

        set
        {
            playerXP = value;
            UIManager.Instance.UpdateXP(playerXP);
        }
    }

    public int TotalPlayerXP
    {
        get
        {
            return totalPlayerXP;
        }

        set
        {
            totalPlayerXP = value;
        }
    }

    public int Food
    {
        get
        {
            return food;
        }

        set
        {
            food = value;
        }
    }

    public bool RefreshData
    {
        get
        {
            return refreshData;
        }

        set
        {
            refreshData = value;
        }
    }

    public int GameTime
    {
        get
        {
            return gameTime;
        }

        set
        {
            gameTime = value;
        }
    }

    public int TotalGameTime
    {
        get
        {
            return totalGameTime;
        }

        set
        {
            totalGameTime = value;
        }
    }
    #endregion

    public override void Initialize()
    { 
        temporaryData = new Dictionary<string, int>();
        dailyData = new Dictionary<string, int>();
        totalData = new Dictionary<string, int>();
        trackingData = new Dictionary<string, int>();

        listDailyEventCollected = new List<string>();
        listDailyEventCompleted = new List<string>();

        Load();
    }

    public void Save()
    {
        PlayerPrefs.SetInt(SCORE_KEY, score);
        PlayerPrefs.SetInt(HIGH_SCORE_KEY, highScore);
        PlayerPrefs.SetInt(MONEY_KEY, money);
        PlayerPrefs.SetInt(TOTAL_MONEY_KEY, totalMoney);
        PlayerPrefs.SetInt(EGG_EXP_KEY, eggEXP);
        PlayerPrefs.SetInt(TOTAL_EGG_EXP_KEY, totalEggEXP);

        PlayerPrefs.SetInt(PLAYER_XP_KEY, playerXP);
        PlayerPrefs.SetInt(TOTAL_PLAYER_XP_KEY, totalPlayerXP);

        PlayerPrefs.SetInt(FOOD_KEY, food);

        PlayerPrefs.SetString(LOGIN_DATE_TIME_KEY, loginDateTime.ToString());

        for (int i = 0; i < trackingKeys.Count; i++)
        {
            var key = trackingKeys[i];

            //PlayerPrefs.SetInt(GetTemporaryDataKey(key), temporaryData[key]);
            PlayerPrefs.SetInt(GetDailyDataKey(key), trackingData[GetDailyDataKey(key)]);
            PlayerPrefs.SetInt(GetTotalDataKey(key), trackingData[GetTotalDataKey(key)]);
        }

        PlayerPrefs.Save();
    }

    public void Load()
    {
        score = PlayerPrefs.GetInt(SCORE_KEY, 0);
        highScore = PlayerPrefs.GetInt(HIGH_SCORE_KEY, 0);
        money = PlayerPrefs.GetInt(MONEY_KEY, 0);
        totalMoney = PlayerPrefs.GetInt(TOTAL_MONEY_KEY, 0);
        eggEXP = PlayerPrefs.GetInt(EGG_EXP_KEY, 0);
        totalEggEXP = PlayerPrefs.GetInt(TOTAL_EGG_EXP_KEY, EggManager.Instance.GetNextClaimEggEXP());

        playerXP = PlayerPrefs.GetInt(PLAYER_XP_KEY, 0);
        totalPlayerXP = PlayerPrefs.GetInt(TOTAL_PLAYER_XP_KEY, 0);

        food = PlayerPrefs.GetInt(FOOD_KEY, 0);

        // try to load daily data
        var data = PlayerPrefs.GetString(LOGIN_DATE_TIME_KEY, string.Empty);
        if(!string.IsNullOrEmpty(data))
        {
            loginDateTime = System.DateTime.Parse(data);
        }
        else
        {
            loginDateTime = System.DateTime.Now;
        }

        // achivement loading....
        //AchievementHelper.Instance.InitializeWithCSV(propertyCSV.text, achievementCSV.text);

        //DailyEventHelper.Instance.InitializeWithCSV(propertyCSV.text, achievementCSV.text);
        //DailyEventHelper.Instance.DailyEventManager.OnDailyEventCollectedCallback += DailyEventManager_OnDailyEventCollectedCallback;
        //DailyEventHelper.Instance.DailyEventManager.OnDailyEventCompletedCallback += DailyEventManager_OnDailyEventCompletedCallback;

        var dateTime = System.DateTime.Now;
        refreshData = dateTime.DayOfYear > loginDateTime.DayOfYear;

        for (int i = 0; i < trackingKeys.Count; i++)
        {
            var key = trackingKeys[i];
            var dailyKey = GetDailyDataKey(key);
            var totalKey = GetTotalDataKey(key);

            trackingData.Add(key, 0);
            trackingData.Add(dailyKey, refreshData ? 0 : PlayerPrefs.GetInt(dailyKey, 0));
            trackingData.Add(totalKey, PlayerPrefs.GetInt(totalKey, 0));

            //AchievementHelper.Instance.AchievementManager.SetPropertyValue(key, trackingData[key]);
            //AchievementHelper.Instance.AchievementManager.SetPropertyValue(dailyKey, trackingData[dailyKey]);
            //AchievementHelper.Instance.AchievementManager.SetPropertyValue(totalKey, trackingData[totalKey]);

            //DailyEventHelper.Instance.DailyEventManager.SetPropertyValue(key, trackingData[key]);
            //DailyEventHelper.Instance.DailyEventManager.SetPropertyValue(dailyKey, trackingData[dailyKey]);
            //DailyEventHelper.Instance.DailyEventManager.SetPropertyValue(totalKey, trackingData[totalKey]);

            Mission.MissionManager.Instance.SetPropertyValue(key, trackingData[key]);
            Mission.MissionManager.Instance.SetPropertyValue(dailyKey, trackingData[dailyKey]);
            Mission.MissionManager.Instance.SetPropertyValue(totalKey, trackingData[totalKey]);
        }

        if (!refreshData)
        {

            data = PlayerPrefs.GetString(LIST_DAILY_EVENT_COMPLETED, string.Empty);
            if (!string.IsNullOrEmpty(data))
            {
                var array = data.Split(';');
                for (int i = 0; i < array.Length; i++)
                {
                    if (!string.IsNullOrEmpty(array[i]))
                    {
                        listDailyEventCompleted.Add(array[i]);
                        //DailyEventHelper.Instance.DailyEventManager.DailyEvents[array[i]].Data.isCompleted = true;
                    }
                }
            }

            data = PlayerPrefs.GetString(LIST_DAILY_EVENT_COLLECTED, string.Empty);
            if (!string.IsNullOrEmpty(data))
            {
                var array = data.Split(';');
                for (int i = 0; i < array.Length; i++)
                {
                    if (!string.IsNullOrEmpty(array[i]))
                    {
                        listDailyEventCollected.Add(array[i]);
                        //DailyEventHelper.Instance.DailyEventManager.DailyEvents[array[i]].Data.isCollected = true;
                    }
                }
            }
        }       

        loginDateTime = dateTime;
    }

    private void DailyEventManager_OnDailyEventCompletedCallback(DailyEvent.DailyEvent dailyEvent)
    {
        if(!listDailyEventCompleted.Contains(dailyEvent.ID))
        {
            listDailyEventCompleted.Add(dailyEvent.ID);
        }

        // save data
        var data = string.Empty;
        for (int i = 0; i < listDailyEventCompleted.Count; i++)
        {
            data += listDailyEventCompleted[i] + ";";
        }
        Debug.Log(data);
        PlayerPrefs.SetString(LIST_DAILY_EVENT_COMPLETED, data);
    }

    private void DailyEventManager_OnDailyEventCollectedCallback(DailyEvent.DailyEvent dailyEvent)
    {
        if (!listDailyEventCollected.Contains(dailyEvent.ID))
        {
            listDailyEventCollected.Add(dailyEvent.ID);
        }

        // save data
        var data = string.Empty;
        for (int i = 0; i < listDailyEventCollected.Count; i++)
        {
            data += listDailyEventCollected[i] + ";";
        }
        PlayerPrefs.SetString(LIST_DAILY_EVENT_COLLECTED, data);
    }

    public void OnDestroy()
    {
        Save();
    }

    public void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }

    public int GetInt(string key, int defaultValue)
    {
        return PlayerPrefs.GetInt(key, defaultValue);
    }

    public void SetFloat(string key, float value)
    {
        PlayerPrefs.SetFloat(key, value);
    }

    public float GetFloat(string key, float defaultValue)
    {
        return PlayerPrefs.GetFloat(key, defaultValue);
    }

    public void SetString(string key, string value)
    {
        PlayerPrefs.SetString(key, value);
    }

    public string GetString(string key, string defaultValue)
    {
        return PlayerPrefs.GetString(key, defaultValue);
    }

    public string GetTemporaryDataKey(string key)
    {
        return string.Empty;
    }

    private string GetDailyDataKey(string key)
    {
        return "Daily" + key;
    }

    private string GetTotalDataKey(string key)
    {
        return "Total" + key;
    }

    public void IncreaseTrackingData(string key, int value)
    {
        trackingData[key] += value;

        //AchievementHelper.Instance.AchievementManager.IncreaseAchievementProperty(key, value);
        //DailyEventHelper.Instance.DailyEventManager.IncreaseAchievementProperty(key, value);
        Mission.MissionManager.Instance.IncreasePropertyValue(key, value);
    }

    public void SetTrackingData(string key, int value)
    {
        trackingData[key] = value;

        //AchievementHelper.Instance.AchievementManager.SetPropertyValue(key, value);
        //DailyEventHelper.Instance.DailyEventManager.SetPropertyValue(key, value);
        Mission.MissionManager.Instance.SetPropertyValue(key, value);

    }

}

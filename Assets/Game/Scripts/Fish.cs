﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private float size;
    [SerializeField]
    private int level;
    [SerializeField]
    private int score;
    [SerializeField]
    private Vector3 velocity;
    [SerializeField]
    private float speed;

    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }
    #endregion

    public void Initialize()
    {
        transform.localScale = Vector3.one;
        transform.localScale *= size;

        armatureComponent.sortingOrder = level - 1;
    }

    public void Release()
    {
    }

    public void Refresh()
    {
        var angleZ = transform.localEulerAngles.z;
        velocity.x = Mathf.Cos(angleZ * Mathf.Deg2Rad);
        velocity.y = Mathf.Sin(angleZ * Mathf.Deg2Rad);
    }

    public void UpdateStep(float deltaTime)
    {
        transform.localPosition += velocity * deltaTime;

        var angleZ = transform.localEulerAngles.z;
        var scale = transform.localScale;

        if (angleZ < 0)
            angleZ += 360;

        if(angleZ > 90 && angleZ < 270)
        {
            armatureComponent.armature.flipY = true;
        }
        else
        {
            armatureComponent.armature.flipY = false;
        }

        transform.localScale = scale;
    }

    public void Dead()
    {
        gameObject.SetActive(false);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.HandleCollideWithPlayer(this);
        }

    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Collector"))
        {
            Dead();
        }
    }

}

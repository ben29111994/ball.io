﻿using System;
using System.Collections;
using System.Collections.Generic;
using DragonBones;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public enum FishyState
{
    Normal,
    Eat,
    Die,

    TOTAL,
    IDLE,
}

public class Fishy : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Basic stats")]
    [SerializeField]
    private Vector3 velocity;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float accelerate;
    [SerializeField]
    private FishyState state;
    [SerializeField]
    private int level;
    [SerializeField]
    private float lifeTimes = 60.0f;
    [SerializeField]
    private float bonusTimes;
    [SerializeField]
    private List<int> fishByLevel;
    [SerializeField]
    private List<float> sizeByLevel;

    [Header("Dragon bones")]
    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;

    [SerializeField]
    private string normalAnimationName;
    [SerializeField]
    private string eatAnimationName;
    [SerializeField]
    private string dieAnimationName;

    [Header("ETC")]
    [SerializeField]
    private GameObject goDot;
    [SerializeField]
    private TrailRenderer trail;
    #endregion

    #region Normal paramters
    private bool isTouch;
    private Vector3 touchPosition;

    private float velocityLength;
    private float curTimes;
    private float curSpeed;
    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }
    #endregion

    public void Initialize()
    {
        isTouch = false;
        touchPosition = Vector3.zero;

        trail.sortingLayerName = "UI";

        armatureComponent.AddEventListener(DragonBones.EventObject.COMPLETE, AnimationEventHandler);
        armatureComponent.AddEventListener(DragonBones.EventObject.LOOP_COMPLETE, AnimationEventHandler);
        armatureComponent.AddEventListener(DragonBones.EventObject.SOUND_EVENT, SoundEventHandler);

        curTimes = lifeTimes / 2.0f;
        curSpeed = speed;
        accelerate = curSpeed / 2;
        CalculateBonusTimes();
    }

    public void Release()
    {
    }

    public void Refresh()
    {
        isTouch = false;
        touchPosition = Vector3.zero;

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;

        level = 1;
        velocity = Vector3.zero;
        velocityLength = 0;
        curTimes = lifeTimes / 2;
        curSpeed = speed;
        accelerate = curSpeed / 2;
        CalculateBonusTimes();
    }

    public void UpdateStep(float deltaTime)
    {
        if (EventSystem.current.currentSelectedGameObject == null)
            UpdateInput();

        transform.localPosition += velocity * deltaTime;

        if (isTouch)
        {
            var anglesZ = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;

            if (anglesZ < 0)
                anglesZ += 360;
            var scale = transform.localScale;

            if (anglesZ > 90 && anglesZ < 270)
            {
                armatureComponent.armature.flipY = true;
            }
            else
            {
                armatureComponent.armature.flipY = false;
            }

            transform.localEulerAngles = new Vector3(0, 0, anglesZ);
        }

        if (!isTouch && velocityLength != 0)
        {
            velocityLength -= deltaTime * accelerate;
            if (velocityLength < 0)
                velocityLength = 0;

            velocity = Vector3.ClampMagnitude(velocity, velocityLength);
        }

        // check life times
        curTimes -= deltaTime;
        if (curTimes <= 0)
        {
            Dead();
        }

        Camera.main.transform.position = transform.position + new Vector3(0, 0, -10);
    }

    private void UpdateInput()
    {
#if !UNITY_EDITOR

        if (Input.touchCount > 0)
        {
            var t = Input.GetTouch(0);

            if (t.phase == TouchPhase.Began && !isTouch)
            {

                isTouch = true;
                touchPosition = t.position;

                return;
            }

            if (t.phase == TouchPhase.Ended || t.phase == TouchPhase.Canceled && isTouch)
            {
                isTouch = false;
                touchPosition = Vector3.zero;

                velocityLength = velocity.magnitude;

                goDot.transform.position = transform.position;

                return;
            }

            if (t.phase == TouchPhase.Moved || t.phase == TouchPhase.Stationary && isTouch)
            {
                Vector3 delta = new Vector3(t.position.x, t.position.y) - touchPosition;
                delta.x /= Screen.width * 0.1f;
                delta.y /= Screen.height * 0.1f;
                delta = Vector3.ClampMagnitude(delta, 1);

                velocity = delta * curSpeed;
                velocity.z = 0;

                goDot.transform.position = transform.position + delta;
            }
        } 

#elif UNITY_EDITOR

        if (Input.GetMouseButtonDown(0) && !isTouch)
        {
            isTouch = true;
            touchPosition = Input.mousePosition;
            return;
        }

        if (Input.GetMouseButtonUp(0) && isTouch)
        {
            isTouch = false;
            touchPosition = Vector3.zero;

            velocityLength = velocity.magnitude;

            goDot.transform.position = transform.position;

            return;
        }

        if (Input.GetMouseButton(0) && isTouch)
        {
            Vector3 delta = Input.mousePosition - touchPosition;
            delta.x /= Screen.width * 0.1f;
            delta.y /= Screen.height * 0.1f;
            delta = Vector3.ClampMagnitude(delta, 1);

            velocity = delta * curSpeed;
            velocity.z = 0;

            goDot.transform.position = transform.position + delta;
        }
#endif
    }

    public void Eat(Fish f)
    {
        ChangeState(FishyState.Eat);
        armatureComponent.timeScale = 4;

        if (f.Level == level)
            curTimes += bonusTimes;
        else
        {
            curTimes += bonusTimes / ((level - f.Level) * 2.0f);
        }

        if (CanUpLevel())
        {
            LevelUp();
        }

    }

    public void Dead()
    {
        GameManager.Instance.EndGame();
    }

    public void LevelUp()
    {
        level++;
        curTimes = lifeTimes / 2;
        CalculateBonusTimes();

        // increase size
        var scaleUp = sizeByLevel.Count <= level ? sizeByLevel[sizeByLevel.Count - 1] : sizeByLevel[level];
        transform.DOScale(scaleUp, 1.0f).Play();
        // scale world
        GameManager.Instance.ScaleWorld();

        if((level - 1) % 3 == 0)
        {
            GameManager.Instance.ChangeBackground();
        }
    }

    public bool CanUpLevel()
    {
        if (curTimes >= lifeTimes)
            return true;

        return false;
    }

    public void ChangeState(FishyState nextState)
    {
        if (state == nextState)
            return;

        state = nextState;
        switch (state)
        {
            case FishyState.Normal:
                armatureComponent.animation.Play(normalAnimationName);
                break;
            case FishyState.Eat:
                armatureComponent.animation.Play(eatAnimationName);
                break;
            case FishyState.Die:
                armatureComponent.animation.Play(dieAnimationName);
                break;
            default:
                Debug.Log("[Fishy] Missing state : " + state);
                armatureComponent.animation.Play(normalAnimationName);
                break;
        }
    }

    private void AnimationEventHandler(string type, EventObject eventObject)
    {
        switch (type)
        {
            case EventObject.LOOP_COMPLETE:

                if (state == FishyState.Eat)
                {
                    ChangeState(FishyState.Normal);
                    armatureComponent.timeScale = 1;
                }

                break;

            default:
                break;
        }
    }

    private void SoundEventHandler(string type, EventObject eventObject)
    {

    }

    private void CalculateBonusTimes()
    {
        if (fishByLevel.Count <= level)
            bonusTimes = lifeTimes / (level * fishByLevel[fishByLevel.Count - 1]);
        else
            bonusTimes = lifeTimes / (level * fishByLevel[level]);
    }

    public float GetExp()
    {
        return curTimes / lifeTimes;
    }


    public void ScaleUpSpeed(float scaleUp)
    {
        curSpeed *= scaleUp;
        accelerate = curSpeed / 2;
    }

    public float GetSpeed()
    {
        return curSpeed;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGeneration : MonoBehaviour {

    [Range(0.1f, 20.0f)]
	public float heightScale = 5.0f;
    [Range(0.1f, 40.0f)]
	public float detailScale = 5.0f;

	private Mesh myMesh;
	private Vector3[] vertices;

	void Update()
	{
		GenerateWave();
	}
	
	void GenerateWave()
	{
		myMesh = gameObject.GetComponent<MeshFilter>().mesh;
		vertices = myMesh.vertices;

		int counter = 0;//i
		int yLevel = 0;//j

		for (int i = 0; i < 11; i++)
		{
			for (int j = 0; j < 11; j++)
			{
				Calculate(counter, yLevel);
				counter++;
			}
			yLevel++;
		}
		myMesh.vertices = vertices;
		myMesh.RecalculateBounds();
		myMesh.RecalculateNormals();
		
//		Destroy(gameObject.GetComponent<MeshCollider>());
//		MeshCollider collider = gameObject.GetComponent<MeshCollider>();
//		collider.sharedMesh = null;
//		collider.sharedMesh = myMesh;

	}

	[Range(0.1f, 2.0f)]
	public float wavesSpeed = 1.0f;
	
	public bool isWaves = false;

	void Calculate(int i, int j)
	{
		if (isWaves)
		{
			vertices[i].z = Mathf.PerlinNoise(Time.time * wavesSpeed + (vertices[i].x + transform.position.x) / detailScale,
				                Time.time * wavesSpeed + (vertices[i].y + transform.position.y) / detailScale) * heightScale;
			vertices[i].z -= j;
		}
		else if (!isWaves)
		{
			vertices[i].z = Mathf.PerlinNoise((vertices[i].x + transform.position.x) / detailScale,
				                (vertices[i].y + transform.position.y) / detailScale) * heightScale;
			vertices[i].z -= j;
		}
	}
}

﻿//using GameFramework.GameStructure;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SFXType
{
    EatFish,
    Die,
    Revive,
    LevelUp,
    NewHighscore,
    EatCoin,
    CollectCoin,
    ChangeFish,
    UnlockNewFish,
    NewEggClaimed,
    EggHatched,
    IncreaseEggProgress,
    StartGameBackgroundSound,
    DuringGameBackgroundSound,
    Purchase,
}

[Serializable]
public struct SFXData
{
    public SFXType fxType;
    public AudioClip clip;
}

public class SoundManager : MonoSingleton<SoundManager>
{
    private Dictionary<string, int> definedEffect;

    //[SerializeField]
    //private AudioClip[] backgroundSources;

    [SerializeField]
    private List<SFXData> sfxList;

    private Dictionary<SFXType, AudioClip> dictClip;
    private AudioSource sfxSource;
    private AudioSource bgmSource;

    // Use this for initialization
    public override void Initialize()
    {
        dictClip = new Dictionary<SFXType, AudioClip>();
        sfxSource = gameObject.AddComponent<AudioSource>();
        sfxSource.loop = sfxSource.playOnAwake = false;

        bgmSource = gameObject.AddComponent<AudioSource>();
        bgmSource.loop = true;
        bgmSource.playOnAwake = false;
        bgmSource.volume = 0.25f;
        for (int i = 0; i < sfxList.Count; i++)
        {
            dictClip.Add(sfxList[i].fxType, sfxList[i].clip);
        }
    }

    //// Update is called once per frame
    //void Update()
    //{

    //}

    //for calling from outside
    public void PlaySFX(SFXType type)
    {
        //Logger.Log("playing");
        //GameManager.Instance.PlayEffect(getEffectByType(type));
        var clip = getEffectByType(type);

        if (clip != null)
        {
            sfxSource.PlayOneShot(clip);
        }
    }

    public void PlayBGM(SFXType type)
    {
        var clip = getEffectByType(type);
        if (clip != null)
        {
            bgmSource.clip = clip;
            bgmSource.loop = true;
            bgmSource.Play();
        }
        else
        {
            bgmSource.Stop();
        }
    }

    public void StopBGM()
    {
        bgmSource.Stop();
    }

    private AudioClip getEffectByType(SFXType type)
    {
        //return sfxList.Find(x => x.fxType == type).clip;
        AudioClip b;
        if (dictClip.TryGetValue(type, out b))
        {
            return b;
        }

        Debug.LogWarning("Can't find audio clip for sfx: " + type);
        return null;
    }

    public void ToggleBGM(bool isMute)
    {
        bgmSource.mute = isMute;
    }

    public void ToggleSFX(bool isMute)
    {
        sfxSource.mute = isMute;
    }
}

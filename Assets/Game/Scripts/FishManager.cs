﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishManager : MonoSingleton<FishManager>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<Fish> originalFishes;

    [SerializeField]
    private SwordFish originalSwordFish;
    #endregion

    #region Normal paramters
    private Dictionary<int, Fish> fishByKey;
    private Dictionary<int, Queue<Fish>> fishesByKey;

    private Queue<SwordFish> inactiveSwordFishes;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        fishByKey = new Dictionary<int, Fish>();
        fishesByKey = new Dictionary<int, Queue<Fish>>();

        for (int i = 0; i < originalFishes.Count; i++)
        {
            Fish f = originalFishes[i];

            fishByKey.Add(f.Level, f);
            fishesByKey.Add(f.Level, new Queue<Fish>());
        }

        inactiveSwordFishes = new Queue<SwordFish>();
    }

    public void Release()
    {
    }

    public Fish GenerateFishByLevel(int level)
    {
        var fishes = fishesByKey[level];

        Fish f = null;

        if(fishes.Count > 0)
        {
            f = fishes.Dequeue();
        }
        else
        {
            f = Instantiate<Fish>(fishByKey[level], transform);
            f.Initialize();
        }

        f.gameObject.SetActive(true);
        return f;
    }

    public void RecycleFish(Fish f)
    {
        var fishes = fishesByKey[f.Level];
        f.gameObject.SetActive(false);

        fishes.Enqueue(f);
    }

    public SwordFish GenerateSwordFish()
    {
        SwordFish f = null;

        if(inactiveSwordFishes.Count > 0)
        {
            f = inactiveSwordFishes.Dequeue();
        }
        else
        {
            f = Instantiate(originalSwordFish);
            f.Initialize();
        }

        f.gameObject.SetActive(true);

        return f;
    }

    public void RecycleSwordFish(SwordFish f)
    {
        f.gameObject.SetActive(false);
        f.Release();

        inactiveSwordFishes.Enqueue(f);
    }

}

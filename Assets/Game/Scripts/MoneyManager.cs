﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyManager : MonoSingleton<MoneyManager>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Money originalMoney;
    [SerializeField]
    private int preInitializeCount = 10;
    #endregion

    #region Normal paramters
    private Queue<Money> inactiveMoney;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        inactiveMoney = new Queue<Money>();

        for (int i = 0; i < preInitializeCount; i++)
        {
            var m = Instantiate(originalMoney, transform);
            m.Initialize();
            m.gameObject.SetActive(false);
            inactiveMoney.Enqueue(m);
        }        
    }

    public void Release()
    {
    }

    public void Refresh()
    {
    }

    public void UpdateStep(float deltaTime)
    {
    }

    public Money GenerateMoney()
    {
        Money m = null;

        if(inactiveMoney.Count > 0)
        {
            m = inactiveMoney.Dequeue();
        }
        else
        {
            m = Instantiate(originalMoney, transform);
            m.Initialize();
        }

        m.Refresh();
        m.gameObject.SetActive(true);

        return m;        
    }

    public void RecycleMoney(Money m)
    {
        m.gameObject.SetActive(false);
        m.Release();

        inactiveMoney.Enqueue(m);
    }

}

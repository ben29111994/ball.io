﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private float changeDirectionTimes = 1.0f;
    [SerializeField]
    private float rotateSpeed;
    [SerializeField]
    private bool clockwise;
    #endregion

    #region Normal paramters
    private float countdown;
    private bool isCollected;
    #endregion

    #region Encapsulate
    #endregion

    public void Initialize()
    {

    }

    public void Release()
    {

    }

    public void Refresh()
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        rotateSpeed = UnityEngine.Random.Range(45.0f, 90.0f);
        clockwise = UnityEngine.Random.Range(0, 2) > 0 ? true : false;
        countdown = changeDirectionTimes;

        isCollected = false;
    }

    public void UpdateStep(float deltaTime)
    {
        if (isCollected)
            return;

        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360;

        if(countdown <= 0)
        {
            clockwise = !clockwise;
            countdown = changeDirectionTimes;
        }
        else
        {
            countdown -= deltaTime;
        }

        if(clockwise)
        {
            angleZ -= rotateSpeed * deltaTime;
        }
        else
        {
            angleZ += rotateSpeed * deltaTime;
        }

        transform.localEulerAngles = new Vector3(0, 0, angleZ);

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && !isCollected)
        {
            transform.DOScale(0, 1.0f)
                .OnStart(delegate { isCollected = true; })
                .OnComplete(delegate { gameObject.SetActive(false); })
                .Play();

            GameManager.Instance.IncreaseMoney(1);
            SoundManager.Instance.PlaySFX(SFXType.EatCoin);
        }
    }

    public void Update()
    {
        UpdateStep(Time.deltaTime);
    }

    public void Awake()
    {
        Refresh();
    }

}

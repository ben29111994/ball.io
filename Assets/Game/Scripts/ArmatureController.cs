﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmatureController : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<DragonBones.UnityArmatureComponent> listArmatureComponents;
    [SerializeField]
    private string swimAnimationName;
    [SerializeField]
    private string eatAnimationName;
    #endregion

    #region Normal paramters
    private int index;
    private DragonBones.UnityArmatureComponent armatureComponent;
    #endregion

    #region Encapsulate
    #endregion

    public void Initialize()
    {

        for (int i = 0; i < listArmatureComponents.Count; i++)
        {
            var armature = listArmatureComponents[i];
            armature.gameObject.SetActive(false);
            armature.AddEventListener(DragonBones.EventObject.COMPLETE, Player.Instance.HandlerAnimationEvent);
            armature.AddEventListener(DragonBones.EventObject.LOOP_COMPLETE, Player.Instance.HandlerAnimationEvent);
        }

        listArmatureComponents.Clear();

        var character = CharacterManager.Instance.SelectedCharacter;

        for (int i = 0; i < character.MaxLevel; i++)
        {
            var component = DragonBonesManager.Instance.GeneratePlayer((DragonBonesType)character.Id, i);
            if (component != null)
            {
                component.transform.SetParent(transform);
                component.gameObject.SetActive(false);
                component.AddEventListener(DragonBones.EventObject.COMPLETE, Player.Instance.HandlerAnimationEvent);
                component.AddEventListener(DragonBones.EventObject.LOOP_COMPLETE, Player.Instance.HandlerAnimationEvent);

                listArmatureComponents.Add(component);
            }
        }

        index = 0;
        armatureComponent = listArmatureComponents[index];
        armatureComponent.gameObject.SetActive(true);

        armatureComponent.sortingOrder = 9000;
    }

    public void Release()
    {
        index = 0;
        armatureComponent.gameObject.SetActive(false);
        armatureComponent = listArmatureComponents[index];
        armatureComponent.gameObject.SetActive(true);
    }

    public void Refresh()
    {
        index = 0;
        armatureComponent.gameObject.SetActive(false);
        armatureComponent = listArmatureComponents[index];
        armatureComponent.gameObject.SetActive(true);
    }

    public void UpdateStep(float deltaTime)
    {
        
    }

    public void Swim()
    {
        if (armatureComponent == null)
            armatureComponent = listArmatureComponents[index];

        armatureComponent.playTimes = -1;
        armatureComponent.timeScale = 1.0f;
        armatureComponent.animation.Play(swimAnimationName);
    }

    public void Eat()
    {
        if (armatureComponent == null)
            armatureComponent = listArmatureComponents[index];

        armatureComponent.playTimes = 3;
        armatureComponent.timeScale = 0.3f;
        armatureComponent.animation.Play(eatAnimationName);
    }

    public void UpgradeArmature()
    {
        index++;
        if(index >= listArmatureComponents.Count)
        {
            index = listArmatureComponents.Count - 1;
            return;
        }
        armatureComponent.gameObject.SetActive(false);
        armatureComponent = listArmatureComponents[index];
        armatureComponent.gameObject.SetActive(true);
        armatureComponent.animation.Play(swimAnimationName);

        armatureComponent.sortingOrder = 9000;
        Swim();
    }

    public void SetFlipY(bool flag)
    {
        armatureComponent.armature.flipY = flag;
    }

    public void SetSortingOrder(int order)
    {
        armatureComponent.sortingOrder = order;
    }
}

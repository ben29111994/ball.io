﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mission
{
    public class MissionExample : MonoBehaviour
    {
        #region Const parameters
        #endregion

        #region Editor paramters
        [SerializeField]
        private MissionExampleItem missionExampleItem;
        #endregion

        #region Normal paramters
        private List<Mission> listMission;
        private List<MissionOperator> listOperator;
        private List<MissionProperty> listProperty;

        private List<MissionExampleItem> listItem;
        #endregion

        #region Encapsulate
        #endregion

        public void Initialize()
        {
            listMission = MissionManager.Instance.GetAllMissions();
            listOperator = MissionManager.Instance.GetAllMissionOperator();
            listProperty = MissionManager.Instance.GetAllMissionProperty();

            listItem = new List<MissionExampleItem>();

            for (int i = 0; i < listMission.Count; i++)
            {
                var mission = listMission[i];
                var item = Instantiate<MissionExampleItem>(missionExampleItem, transform);
                item.Initialize(this, mission);
                item.gameObject.SetActive(true);

                listItem.Add(item);
            }
        }

        public void Release()
        {
        }

        public void Refresh()
        {
        }

        public void UpdateStep(float deltaTime)
        {
        }

        public void HandingMission(Mission mission)
        {
            if(!mission.IsCompleted)
            {
                foreach (var cond in mission.ListCondition)
                {
                    MissionManager.Instance.IncreasePropertyValue(cond.PropertyID, 1000);
                }
            }

            for (int i = 0; i < listItem.Count; i++)
            {
                listItem[i].RefreshUI();
            }
        }

        public void Start()
        {
            Initialize();
        }
    } 
}

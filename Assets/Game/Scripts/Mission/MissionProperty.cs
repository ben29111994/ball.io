﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mission
{
    public class MissionProperty
    {
        #region Const parameters
        #endregion

        #region Editor paramters
        #endregion

        #region Normal paramters
        private string propertyID;
        private int value;
        private int initalValue;

        public event System.Action<MissionProperty> OnValueChanged;
        #endregion

        #region Encapsulate
        public string PropertyID
        {
            get
            {
                return propertyID;
            }

            set
            {
                propertyID = value;
            }
        }

        public int Value
        {
            get
            {
                return value;
            }

            set
            {
                this.value = value;

                if (OnValueChanged != null)
                    OnValueChanged(this);
            }
        }

        public int InitalValue
        {
            get
            {
                return initalValue;
            }

            set
            {
                initalValue = value;
            }
        }
        #endregion

    } 
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mission
{
    [System.Serializable]
    public class Mission
    {
        #region Const parameters
        #endregion

        #region Editor paramters
        #endregion

        #region Normal paramters
        [SerializeField]
        private string missionID;
        [SerializeField]
        private string title;
        [SerializeField]
        private string description;
        [SerializeField]
        private List<MissionOperator> listCondition;
        [SerializeField]
        private bool isCompleted;
        [SerializeField]
        private bool isCollected;
        [SerializeField]
        private bool isHidden;
        [SerializeField]
        private bool isActive;

        public event System.Action<Mission> OnCompleted;
        public event System.Action<Mission> OnCollected;
        #endregion

        #region Encapsulate
        public string MissionID
        {
            get
            {
                return missionID;
            }

            set
            {
                missionID = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public List<MissionOperator> ListCondition
        {
            get
            {
                return listCondition;
            }

            set
            {
                listCondition = value;
            }
        }

        public bool IsCompleted
        {
            get
            {
                return isCompleted;
            }

            set
            {
                isCompleted = value;
            }
        }

        public bool IsCollected
        {
            get
            {
                return isCollected;
            }

            set
            {
                isCollected = value;
            }
        }

        public bool IsHidden
        {
            get
            {
                return isHidden;
            }

            set
            {
                isHidden = value;
            }
        }

        public bool IsActive
        {
            get
            {
                return isActive;
            }

            set
            {
                isActive = value;
            }
        }
        #endregion

        public void OnOperatorCompleted(MissionOperator missionOperator)
        {
            if (isCompleted)
                return;

            isCompleted = true;

            for (int i = 0; i < listCondition.Count; i++)
            {
                var condition = listCondition[i];
                if(condition != missionOperator)
                {
                    if(!condition.IsCompleted)
                    {
                        isCompleted = false;
                        break;
                    }
                }
            }

            if(isCompleted)
            {
                OnCompleted(this);
            }
        }

        public void CollectReward()
        {
            isCollected = true;

            if (OnCollected != null)
                OnCollected(this);
        }
    }
}
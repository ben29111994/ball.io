﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mission
{
    public class MissionExampleItem : MonoBehaviour
    {
        [SerializeField]
        private UnityEngine.UI.Button button;
        [SerializeField]
        private TMPro.TextMeshProUGUI content;

        private Mission mission;
        private MissionExample example;

        public void Initialize(MissionExample example, Mission mission)
        {
            button.onClick.AddListener(OnButtonClicked);

            this.example = example;
            this.mission = mission;

            RefreshUI();
        }

        public void OnButtonClicked()
        {
            example.HandingMission(mission);
        }

        public void RefreshUI()
        {
            var text = string.Empty;
            foreach (var condi in mission.ListCondition)
            {
                if (!condi.IsCompleted)
                {
                    text += condi.OperatorID + " ";
                }
            }

            content.text = text;
        }
    } 
}

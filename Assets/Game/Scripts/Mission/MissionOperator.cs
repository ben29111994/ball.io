﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mission
{
    [System.Serializable]
    public class MissionOperator
    {
        #region Const parameters
        #endregion

        #region Editor paramters
        #endregion

        #region Normal paramters
        [SerializeField]
        private string operatorID;
        [SerializeField]
        private string propertyID;
        [SerializeField]
        private string expression;
        [SerializeField]
        private int targetValue;
        [SerializeField]
        private bool isCompleted;

        public event System.Action<MissionOperator> OnCompleted;
        #endregion

        #region Encapsulate
        public string OperatorID
        {
            get
            {
                return operatorID;
            }

            set
            {
                operatorID = value;
            }
        }

        public string PropertyID
        {
            get
            {
                return propertyID;
            }

            set
            {
                propertyID = value;
            }
        }

        public string Expression
        {
            get
            {
                return expression;
            }

            set
            {
                expression = value;
            }
        }

        public int TargetValue
        {
            get
            {
                return targetValue;
            }

            set
            {
                targetValue = value;
            }
        }

        public bool IsCompleted
        {
            get
            {
                return isCompleted;
            }

            set
            {
                isCompleted = value;
            }
        }
        #endregion

        public void OnPropertyValueChanged(MissionProperty property)
        {
            isCompleted = false;

            switch (Expression)
            {
                case MissionManager.ACTIVE_IF_EQUALS_TO:
                    if (property.Value == TargetValue)
                    {
                        isCompleted = true;
                        OnCompleted(this);
                    }
                    break;

                case MissionManager.ACTIVE_IF_GREATER_THAN:
                    if (property.Value > TargetValue)
                    {
                        isCompleted = true;
                        OnCompleted(this);

                    }
                    break;

                case MissionManager.ACTIVE_IF_LESS_THAN:
                    if (property.Value < TargetValue)
                    {
                        isCompleted = true;
                        OnCompleted(this);
                    }
                    break;
            }
        }

    }
}

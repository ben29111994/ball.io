﻿using FullSerializer;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Mission
{
    public class MissionManager : MonoSingleton<MissionManager>
    {
        #region Const parameters
        // activation rules
        public const string ACTIVE_IF_GREATER_THAN = ">";
        public const string ACTIVE_IF_LESS_THAN = "<";
        public const string ACTIVE_IF_EQUALS_TO = "=";
        #endregion

        #region Editor paramters
        [SerializeField]
        private TextAsset propertyAsset;
        [SerializeField]
        private TextAsset missionAsset;
        #endregion

        #region Normal paramters
        private List<string> listMissionID;
        private List<string> listMissionOperatorID;
        private List<string> listMissionPropertyID;

        private Dictionary<string, Mission> missions;
        private Dictionary<string, MissionOperator> missionOperators;
        private Dictionary<string, MissionProperty> missionProperties;

        public event System.Action<Mission> OnMissionCompletedCallback;
        public event System.Action<Mission> OnMissionCollectedCallback;
        #endregion

        #region Encapsulate
        #endregion

        public override void Initialize()
        {
            base.Initialize();

            listMissionID = new List<string>();
            listMissionOperatorID = new List<string>();
            listMissionPropertyID = new List<string>();

            missions = new Dictionary<string, Mission>();
            missionOperators = new Dictionary<string, MissionOperator>();
            missionProperties = new Dictionary<string, MissionProperty>();

            CSVReader.LoadFromString(missionAsset.text, MissionDataLineRead);
            CSVReader.LoadFromString(propertyAsset.text, PropertyDataLineRead);

            //add callback
            for (int i = 0; i < listMissionID.Count; i++)
            {
                var mission = missions[listMissionID[i]];
                mission.OnCompleted += OnMissionCompleted;
                mission.OnCollected += OnMissionCollected;

                for (int j = 0; j < mission.ListCondition.Count; j++)
                {
                    var condition = mission.ListCondition[j];
                    condition.OnCompleted += mission.OnOperatorCompleted;

                    var property = missionProperties[condition.PropertyID];
                    property.OnValueChanged += condition.OnPropertyValueChanged;
                }
            }

            //cheat code to checking expression "<" of operator for first time
            for (int i = 0; i < listMissionPropertyID.Count; i++)
            {
                Debug.Log(listMissionPropertyID[i]);
                IncreasePropertyValue(listMissionPropertyID[i], 0);
            }
        }

        private Mission CreateMission(string ID, string title, string description, string conditions, bool isHidden, bool isActive)
        {
            fsData data = fsJsonParser.Parse(conditions);

            List<MissionOperator> listCondition = new List<MissionOperator>(3);
            foreach (var cond in data.AsList)
            {
                var conditionParts = cond.AsList;
                var operatorID = conditionParts[0].AsString + conditionParts[1].AsString + conditionParts[2].AsInt64;

                MissionOperator missionOperator = null;
                if (!missionOperators.ContainsKey(operatorID))
                {
                    missionOperator = CreateMissionOperator(conditionParts[0].AsString, conditionParts[1].AsString, (int)conditionParts[2].AsInt64);

                    listMissionOperatorID.Add(missionOperator.OperatorID);
                    missionOperators.Add(missionOperator.OperatorID, missionOperator);
                }
                else
                {
                    missionOperator = missionOperators[operatorID];
                }

                listCondition.Add(missionOperator);
            }

            if (listCondition.Count > 0)
            {
                Mission mission = new Mission();
                mission.MissionID = ID;
                mission.Title = title;
                mission.Description = description;
                mission.ListCondition = listCondition;
                mission.IsHidden = isHidden;
                mission.IsActive = isActive;

                return mission;
            }

            return null;
        }

        private MissionOperator CreateMissionOperator(string propertyID, string expression, int targetValue)
        {
            MissionOperator missionOperator = new MissionOperator();

            missionOperator.OperatorID = propertyID + expression + targetValue;
            missionOperator.PropertyID = propertyID;
            missionOperator.Expression = expression;
            missionOperator.TargetValue = targetValue;

            return missionOperator;
        }

        private MissionProperty CreateMissionProperty(string id, int initalValue)
        {
            MissionProperty property = new MissionProperty();

            property.PropertyID = id;
            property.InitalValue = initalValue;

            return property;
        }

        private void MissionDataLineRead(int line_index, List<string> line)
        {
            if (line_index > 0)
            {
                //ID,title,description,conditions,ishidden,isactive
                var mission = CreateMission(
                    line[0],
                    line[1],
                    line[2],
                    line[3],
                    !string.IsNullOrEmpty(line[4]),
                    !string.IsNullOrEmpty(line[5])
                    );

                if (!missions.ContainsKey(mission.MissionID))
                {
                    listMissionID.Add(mission.MissionID);
                    missions.Add(mission.MissionID, mission);
                }
            }
        }

        private void PropertyDataLineRead(int line_index, List<string> line)
        {
            if (line_index > 0)
            {
                var property = CreateMissionProperty(line[0], int.Parse(line[1]));

                if (!missionProperties.ContainsKey(property.PropertyID))
                {
                    listMissionPropertyID.Add(property.PropertyID);
                    missionProperties.Add(property.PropertyID, property);
                }
            }
        }

        public void IncreasePropertyValue(string propertyID, int propertyValue)
        {
            if (missionProperties.ContainsKey(propertyID))
            {
                missionProperties[propertyID].Value += propertyValue;
            }
            else
            {
                Debug.LogError("IncreasePropertyValue: " + propertyID);
            }
        }

        public void SetPropertyValue(string propertyID, int propertyValue)
        {
            if (missionProperties.ContainsKey(propertyID))
            {
                missionProperties[propertyID].Value = propertyValue;
            }
            else
            {
                Debug.LogError("SetPropertyValue: " + propertyID);
            }
        }

        public void OnMissionCompleted(Mission mission)
        {
            if (OnMissionCompletedCallback != null)
            {
                Debug.Log("[MissionManager] Mission Completed : " + mission.MissionID);
                OnMissionCompletedCallback(mission);
            }
        }

        public void OnMissionCollected(Mission mission)
        {
            if (OnMissionCollectedCallback != null)
            {
                Debug.Log("[MissionManager] Mission Collected : " + mission.MissionID);
                OnMissionCollectedCallback(mission);
            }
        }

        public List<Mission> GetAllMissions()
        {
            List<Mission> result = new List<Mission>(listMissionID.Count);

            for (int i = 0; i < listMissionID.Count; i++)
            {
                result.Add(missions[listMissionID[i]]);
            }

            return result;
        }

        public List<MissionOperator> GetAllMissionOperator()
        {
            List<MissionOperator> result = new List<MissionOperator>(listMissionOperatorID.Count);

            for (int i = 0; i < listMissionOperatorID.Count; i++)
            {
                result.Add(missionOperators[listMissionOperatorID[i]]);
            }

            return result;
        }

        public List<MissionProperty> GetAllMissionProperty()
        {
            List<MissionProperty> result = new List<MissionProperty>(listMissionPropertyID.Count);

            for (int i = 0; i < listMissionPropertyID.Count; i++)
            {
                result.Add(missionProperties[listMissionPropertyID[i]]);
            }

            return result;
        }

        public Mission GetMission(string missionID)
        {
            if (missions.ContainsKey(missionID))
                return missions[missionID];
            else
                return null;
        }

        public MissionOperator GetMissionOperator(string operatorID)
        {
            if (missionOperators.ContainsKey(operatorID))
                return missionOperators[operatorID];
            else
                return null;
        }

        public MissionProperty GetMissionProperty(string propertyID)
        {
            if (missionProperties.ContainsKey(propertyID))
                return missionProperties[propertyID];
            else
                return null;
        }

        #region CSV Reader
        public delegate void ReadLineDelegate(int line_index, List<string> line);

        public static void LoadFromString(string file_contents, ReadLineDelegate line_reader)
        {
            int file_length = file_contents.Length;

            // read char by char and when a , or \n, perform appropriate action
            int cur_file_index = 0; // index in the file
            List<string> cur_line = new List<string>(); // current line of data
            int cur_line_number = 0;
            StringBuilder cur_item = new StringBuilder("");
            bool inside_quotes = false; // managing quotes
            while (cur_file_index < file_length)
            {
                char c = file_contents[cur_file_index++];

                switch (c)
                {
                    case '"':
                        if (!inside_quotes)
                        {
                            inside_quotes = true;
                        }
                        else
                        {
                            if (cur_file_index == file_length)
                            {
                                // end of file
                                inside_quotes = false;
                                goto case '\n';
                            }
                            else if (file_contents[cur_file_index] == '"')
                            {
                                // double quote, save one
                                cur_item.Append("\"");
                                cur_file_index++;
                            }
                            else
                            {
                                // leaving quotes section
                                inside_quotes = false;
                            }
                        }
                        break;
                    case '\r':
                        // ignore it completely
                        break;
                    case ',':
                        goto case '\n';
                    case '\n':
                        if (inside_quotes)
                        {
                            // inside quotes, this characters must be included
                            cur_item.Append(c);
                        }
                        else
                        {
                            // end of current item
                            cur_line.Add(cur_item.ToString());
                            cur_item.Length = 0;
                            if (c == '\n' || cur_file_index == file_length)
                            {
                                // also end of line, call line reader
                                line_reader(cur_line_number++, cur_line);
                                cur_line.Clear();
                            }
                        }
                        break;
                    default:
                        // other cases, add char
                        cur_item.Append(c);

                        if (cur_file_index == file_length)
                        {
                            cur_line.Add(cur_item.ToString());
                            // also end of line, call line reader
                            line_reader(cur_line_number++, cur_line);
                            cur_line.Clear();
                        }
                        break;
                }
            }
        }
        #endregion

    }
}

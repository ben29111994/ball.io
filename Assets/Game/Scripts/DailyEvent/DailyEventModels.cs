﻿using System.Collections.Generic;

namespace DailyEvent
{
    public class DailyEventModel
    {
        public string ID;
        public List<OperatorModel> listConditions;
        public string title;
        public string description;
        public bool isActive;
        public bool isHidden;
        public bool isCompleted;
        public bool isCollected;
    }

    public class OperatorModel
    {
        public string ID;
        public string propertyID;
        public string expressionString;
        public int targetValue;
        public bool isSoloUse;
        public bool isComplete;
    }

    public class PropertyModel
    {
        public string ID;
        public int initValue;
        public int currentValue;
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DailyEvent
{
    public class DailyEventOperator
    {
        private OperatorModel data;

        public OperatorModel Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        public string PropertyID
        {
            get { return Data.propertyID; }
        }

        public string Expression
        {
            get { return Data.expressionString; }
            internal set { Data.expressionString = value; }
        }

        public int TargetValue
        {
            get { return Data.targetValue; }
            //set { m_TargetValue = value; }
        }

        public string ID
        {
            get { return Data.ID; }
        }

        public bool IsSoloUse
        {
            get { return Data.isSoloUse; }
        }

        public bool IsComplete
        {
            get { return Data.isComplete; }
        }
        
        private event Action<DailyEventOperator> OnOperatorCompleted;

        public void SetOperatorCompleteCallBack(Action<DailyEventOperator> callback)
        {
            if (callback != null)
            {
                if (IsSoloUse)
                {
                    if (OnOperatorCompleted == null)
                    {
                        OnOperatorCompleted += callback;
                    }
                    else
                    {
                        Debug.Log(string.Format("You are trying to set another call back for a Solo Operator ({0}), skipping...", ID));
                    }
                }
                else
                {
                    OnOperatorCompleted += callback;
                }
            }
            else
            {
                Debug.Log(string.Format("You are trying to set call back for this Operator ({0}) as null, skipping...", ID));
            }
        }

        public DailyEventOperator(OperatorModel data, DailyEventProperty property)
        {
            if (data != null)
            {
                if ((property != null))
                {
                    //check to make sure the expression string is valid
                    if (data.expressionString.Equals(DailyEventManager.ACTIVE_IF_EQUALS_TO)
                        || data.expressionString.Equals(DailyEventManager.ACTIVE_IF_GREATER_THAN)
                        || data.expressionString.Equals(DailyEventManager.ACTIVE_IF_LESS_THAN))
                    {
                        this.data = data;
                        //data.propertyID = property.ID;
                        Debug.Log("Add callback for property: " + property.ID);
                        property.OnValueChanged -= OnPropertyValueChanged;
                        property.OnValueChanged += OnPropertyValueChanged;
                    }
                    else
                    {
                        Debug.LogError("Trying to initialize an Achievement Operator with an invalid expression string of " + data.expressionString + ". Skipping...");
                    }
                }
                else
                {
                    Debug.LogError("Trying to initialize an Achievement Operator with a null property. Skipping...");
                }
            }
            else
            {
                Debug.LogError("Trying to initialize an Achievement Operator with null data. Skipping...");
            }
        }

        private void OnPropertyValueChanged(DailyEventProperty property)
        {
            //Debug.Log(string.Format("Property changed: {0}, current value {1} in operator {2}", property.ID, property.Value, ID));
            data.isComplete = false;

            switch (Expression)
            {
                case DailyEventManager.ACTIVE_IF_EQUALS_TO:
                    if (property.Value == TargetValue)
                    {
                        data.isComplete = true;
                        FinishOperator();
                    }
                    break;

                case DailyEventManager.ACTIVE_IF_GREATER_THAN:
                    if (property.Value > TargetValue)
                    {
                        data.isComplete = true;
                        FinishOperator();
                    }
                    break;

                case DailyEventManager.ACTIVE_IF_LESS_THAN:
                    if (property.Value < TargetValue)
                    {
                        data.isComplete = true;
                        FinishOperator();
                    }
                    break;
            }
        }

        private void FinishOperator()
        {
            //m_IsCompleted = true;
            if (OnOperatorCompleted != null)
            {
                OnOperatorCompleted(this);
            }
        }
    } 
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DailyEvent
{
    public class DailyEvent
    {
        public event Action<DailyEvent> OnDailyEventCompleted;
        public event Action<DailyEvent> OnDailyEventCollected;

        private DailyEventModel data;

        public DailyEventModel Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        public string ID
        {
            get { return Data.ID; }
        }

        public string Title
        {
            get { return Data.title; }
        }

        public string Description
        {
            get { return Data.description; }
        }

        public bool IsActive
        {
            get { return Data.isActive; }
        }

        public bool IsHidden
        {
            get { return Data.isHidden; }
        }

        public bool IsCompleted
        {
            get { return Data.isCompleted; }
        }

        public bool IsCollected
        {
            get { return Data.isCollected; }
        }

        private DailyEvent(DailyEventModel data)
        {
            this.data = data;
        }

        public static DailyEvent CreateDailyEvent(DailyEventModel data, DailyEventManager manager)
        {
            DailyEvent dailyEvent = new DailyEvent(data);

            var listOperators = data.listConditions;
            Debug.Log("Creating achievement: " + data.ID + ", num condition: " + data.listConditions.Count);
            foreach (var opModel in listOperators)
            {
                DailyEventOperator op = manager.AddOperator(opModel);
                if (op != null)
                {
                    op.SetOperatorCompleteCallBack(dailyEvent.OnOperatorCompleted);
                }
                else
                {
                    Debug.Log("Can NOT find suitable operator, creating Achievement failed for " + data.ID);
                    return null;
                }
            }

            return dailyEvent;
        }

        private void OnOperatorCompleted(DailyEventOperator op)
        {
            if (IsCompleted)
                return;

            bool isCompleted = true;

            for (int i = 0; i < Data.listConditions.Count; i++)
            {
                var condition = Data.listConditions[i];
                if (!condition.isComplete)
                {
                    isCompleted = false;
                    break;
                }
            }

            if (isCompleted)
            {
                data.isCompleted = isCompleted;

                if (OnDailyEventCompleted != null)
                    OnDailyEventCompleted(this);
            }
        }
    }
}

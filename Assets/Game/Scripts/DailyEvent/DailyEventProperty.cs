﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DailyEvent
{
    public class DailyEventProperty
    {
        private PropertyModel data;

        public PropertyModel Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        /// <summary>
        /// Get or set the name of this property
        /// </summary>
        public string ID
        {
            get { return Data.ID; }
            private set { Data.ID = value; }
        }

        /// <summary>
        /// The current value of this property
        /// </summary>
        public int Value
        {
            get { return Data.currentValue; }
            set
            {
                Data.currentValue = value;
                //Debug.Log("Callback when value change: " + OnValueChanged);
                if (OnValueChanged != null)
                {
                    OnValueChanged(this);
                }
            }
        }

        /// <summary>
        /// The initial value of this property
        /// </summary>
        public int InitialValue
        {
            get { return Data.initValue; }
            private set { Data.initValue = value; }
        }

        public event Action<DailyEventProperty> OnValueChanged;

        public DailyEventProperty(string id, int initialValue, int value)
        {
            Data = new PropertyModel();
            this.ID = id;
            this.InitialValue = initialValue;
            this.Value = value;
        }

        public DailyEventProperty(PropertyModel data)
        {
            this.Data = data;
        }

        public void Reset()
        {
            this.Value = this.InitialValue;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DailyEvent
{
    public class DailyEventManager : MonoBehaviour
    {
        // activation rules
        public const string ACTIVE_IF_GREATER_THAN = ">";
        public const string ACTIVE_IF_LESS_THAN = "<";
        public const string ACTIVE_IF_EQUALS_TO = "=";

        public event Action<DailyEvent> OnDailyEventCompletedCallback;
        public event Action<DailyEvent> OnDailyEventCollectedCallback;

        #region Add/Remove Daily Events
        private List<string> listDailyEventID;
        private Dictionary<string, DailyEvent> dailyEvents;

        public Dictionary<string, DailyEvent> DailyEvents
        {
            get
            {
                return dailyEvents;
            }

            set
            {
                dailyEvents = value;
            }
        }

        public List<DailyEventModel> GetAllDailyEventData()
        {
            List<DailyEventModel> res = new List<DailyEventModel>(listDailyEventID.Count);
            foreach (string id in listDailyEventID)
            {
                res.Add(DailyEvents[id].Data);
            }

            return res;
        }

        private void AddDailyEvent(DailyEventModel data)
        {
            if (data != null)
            {
                if (!DailyEvents.ContainsKey(data.ID))
                {
                    DailyEvent dailyEvent = DailyEvent.CreateDailyEvent(data, this);
                    if (dailyEvent != null)
                    {
                        dailyEvent.OnDailyEventCompleted += OnDailyEventCompleted;
                        dailyEvent.OnDailyEventCollected += OnDailyEventCollected;

                        DailyEvents.Add(data.ID, dailyEvent);
                        listDailyEventID.Add(data.ID);
                    }
                }
                else
                {
                    Debug.Log("Trying to add new achievement, but the collection already contains the same property: " + data.ID + ", skipping...");
                }
            }
            else
            {
                Debug.Log("Trying to add new achievement with NULL data, skipping...");
            }
        }

        private void OnDailyEventCompleted(DailyEvent dailyEvent)
        {
            Debug.LogError("Achievement Completed: " + dailyEvent.Title);
            if (OnDailyEventCompletedCallback != null)
            {
                OnDailyEventCompletedCallback(dailyEvent);
            }
        }

        private void OnDailyEventCollected(DailyEvent dailyEvent)
        {
            Debug.LogError("Achievement Completed: " + dailyEvent.Title);
            if (OnDailyEventCollectedCallback != null)
            {
                OnDailyEventCollectedCallback(dailyEvent);
            }
        }

        private DailyEvent GetAchievement(string id)
        {
            if (DailyEvents.ContainsKey(id))
            {
                return DailyEvents[id];
            }

            Debug.Log("Could NOT found achievement: " + id + ", returning null...");
            return null;
        }
        #endregion

        #region Add/Remove Operators
        private List<string> listOperatorID;
        private Dictionary<string, DailyEventOperator> operators;
        public Dictionary<string, DailyEventOperator> Operators
        {
            get
            {
                return operators;
            }

            set
            {
                operators = value;
            }
        }
        
        internal DailyEventOperator AddOperator(OperatorModel data)
        {
            if (data != null)
            {
                data.ID = data.propertyID + data.expressionString + data.targetValue.ToString();
                if (!Operators.ContainsKey(data.ID))
                {
                    DailyEventProperty prop = GetProperty(data.propertyID);
                    if (prop == null)
                    {
                        Debug.Log("Could not find property: " + data.propertyID + ", can't add operator..." + data.ID);
                        return null;
                    }
                    else
                    {
                        Debug.Log("Creating new operator " + data.ID);
                        DailyEventOperator op = new DailyEventOperator(data, prop);

                        Operators.Add(data.ID, op);
                        listOperatorID.Add(data.ID);

                        return op;
                    }
                }
                else
                {
                    Debug.Log("Trying to add new Operator, but the collection already contains the same property: " + data.ID + ", returning existed one...");
                    return Operators[data.ID];
                }
            }
            else
            {
                Debug.LogError("Trying to add new operator with NULL data, returning NULL...");
                return null;
            }
        }
        #endregion

        #region Add/Remove Properties
        private List<string> listPropertiesID;
        private Dictionary<string, DailyEventProperty> properties;
        public Dictionary<string, DailyEventProperty> Properties
        {
            get
            {
                return properties;
            }

            set
            {
                properties = value;
            }
        }

        /// <summary>
        /// Add new achievement property, based on specified model
        /// </summary>
        /// <param name="propData">Model data of achievement property</param>
        /// <returns>The newly created property, or the existed one</returns>
        private DailyEventProperty AddAchievementProperty(PropertyModel propData)
        {
            if (propData != null)
            {
                if (!Properties.ContainsKey(propData.ID))
                {
                    DailyEventProperty property = new DailyEventProperty(propData);
                    Properties.Add(propData.ID, property);
                    listPropertiesID.Add(propData.ID);
                    Debug.Log("Added property: " + propData.ID + " initial value: " + propData.initValue);
                    return property;
                }
                else
                {
                    Debug.Log("Trying to add new achievement property, but the collection already contains the same property: " + propData.ID + ", returning existed one.");
                    return Properties[propData.ID];
                }
            }
            else
            {
                Debug.Log("Trying to add new achievement property with NULL data, returning NULL");
                return null;
            }
        }

        public List<PropertyModel> GetAllPropertiesData()
        {
            List<PropertyModel> res = new List<PropertyModel>(listPropertiesID.Count);
            foreach (string id in listPropertiesID)
            {
                res.Add(Properties[id].Data);
            }

            return res;
        }

        private DailyEventProperty GetProperty(string id)
        {
            if (Properties.ContainsKey(id))
            {
                return Properties[id];
            }

            Debug.Log("Could NOT found achievement property: " + id + ", returning null...");
            return null;
        }

        public int GetPropertyValue(string propertyID)
        {
            if (Properties.ContainsKey(propertyID))
            {
                return Properties[propertyID].Value;
            }

            Debug.Log("Trying to get value of not-existing property: " + propertyID + " returning -1");
            return -1;
        }

        public void IncreaseAchievementProperty(string propertyID, int value = 1)
        {
            if (Properties.ContainsKey(propertyID))
            {
                //Debug.Log("Increasing value of property: " + propertyID + " by " + value);
                Properties[propertyID].Value += value;
            }
            else
            {
                Debug.Log("Trying to increase value of not-existing property: " + propertyID + ", skipping...");
            }
        }
        public void SetPropertyValue(string propertyID, int value)
        {
            if (Properties.ContainsKey(propertyID))
            {
                Properties[propertyID].Value = value;
            }
            else
            {
                Debug.Log("Trying to set value of not-existing property: " + propertyID + ", skipping...");
            }
        }

        internal void SetPropertyValue(List<string> propertiesIDs, List<int> values)
        {
            if (propertiesIDs.Count != values.Count)
            {
                Debug.LogError(string.Format("Size mismatched: List of properties ({0}) and list of values ({1}) are not identical in size, skipping...", propertiesIDs.Count, values.Count));
                return;
            }

            for (int i = 0; i < propertiesIDs.Count; i++)
            {
                SetPropertyValue(propertiesIDs[i], values[i]);
            }
        }
        #endregion

        public void Initialize(List<PropertyModel> props, List<DailyEventModel> achs)
        {
            DailyEvents = new Dictionary<string, DailyEvent>(50);
            listDailyEventID = new List<string>(50);

            Operators = new Dictionary<string, DailyEventOperator>(50);
            listOperatorID = new List<string>(50);

            Properties = new Dictionary<string, DailyEventProperty>(10);
            listPropertiesID = new List<string>(10);

            InitializeProperties(props);
            InitializeDailyEvents(achs);
        }

        private void InitializeProperties(List<PropertyModel> props)
        {
            for (int i = 0; i < props.Count; i++)
            {
                AddAchievementProperty(props[i]);
            }
        }

        private void InitializeDailyEvents(List<DailyEventModel> achs)
        {
            for (int i = 0; i < achs.Count; i++)
            {
                AddDailyEvent(achs[i]);
            }
        }
    }
}

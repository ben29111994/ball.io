﻿using DailyEvent;
using FullSerializer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class DailyEventHelper : MonoSingleton<DailyEventHelper>
{
    private static readonly fsSerializer _serializer = new fsSerializer();

    DailyEventManager ach = new DailyEventManager();
    public DailyEventManager DailyEventManager
    {
        get { return ach; }
    }

    private static List<PropertyModel> listProperties = new List<PropertyModel>(25);
    private static List<DailyEventModel> listDailyEvents = new List<DailyEventModel>(50);

    public void InitializeWithCSV(string propertyData, string dailyEventData)
    {
        CSVReader.LoadFromString(propertyData, PropertyDataLineRead);
        CSVReader.LoadFromString(dailyEventData, DailyEventDataLineRead);
        DoInitialize();
    }

    private void DoInitialize()
    {
        ach.Initialize(listProperties, listDailyEvents);
    }

    private void DailyEventDataLineRead(int line_index, List<string> line)
    {
        if (line_index > 0)
        {
            //ID,title,description,conditions,ishidden,isactive
            var achievement = CreateDailyEventModel(
                line[0],
                line[1],
                line[2],
                line[3],
                !string.IsNullOrEmpty(line[4]),
                !string.IsNullOrEmpty(line[5])
                );
            listDailyEvents.Add(achievement);
        }
    }

    private void PropertyDataLineRead(int line_index, List<string> line)
    {
        if (line_index > 0)
        {
            var property = CreatePropertyModel(line[0], int.Parse(line[1]));
            listProperties.Add(property);
        }
    }

    public static PropertyModel CreatePropertyModel(string ID, int initValue, int currentValue = 0)
    {
        PropertyModel prop = new PropertyModel();
        prop.ID = ID;
        prop.initValue = initValue;
        prop.currentValue = initValue;

        return prop;
    }

    public static DailyEventModel CreateDailyEventModel(string ID, string title, string description, string condition, bool isActive, bool isHidden)
    {
        //Condition conds = Deserialize(typeof(Condition), condition) as Condition;
        fsData data = fsJsonParser.Parse(condition);

        List<OperatorModel> listOperators = new List<OperatorModel>(3);
        foreach (var cond in data.AsList)
        {
            var conditionParts = cond.AsList;
            OperatorModel om = new OperatorModel();
            om.propertyID = conditionParts[0].AsString;
            om.expressionString = conditionParts[1].AsString;
            om.targetValue = (int)conditionParts[2].AsInt64;
            listOperators.Add(om);
        }

        if (listOperators.Count > 0)
        {
            DailyEventModel am = new DailyEventModel();
            am.ID = ID;
            am.title = title;
            am.description = description;
            am.isActive = isActive;
            am.isHidden = isHidden;
            am.isCompleted = false;
            am.isCollected = false;
            am.listConditions = listOperators;
            return am;
        }

        return null;
    }

    public static object Deserialize(Type type, string serializedState)
    {
        // step 1: parse the JSON data
        fsData data = fsJsonParser.Parse(serializedState);
        //data.
        // step 2: deserialize the data
        object deserialized = null;
        _serializer.TryDeserialize(data, type, ref deserialized).AssertSuccessWithoutWarnings();

        return deserialized;
    }
}

﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoSingleton<Background>
{

    #region Const parameters
    private const string LIST_UNLOCKED_BACKGROUNDS = "ListUnlockedBackgrounds";
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<Sprite> listBackgrounds;
    [SerializeField]
    private float fadeTimes;
    [SerializeField]
    private SpriteRenderer spMain;
    [SerializeField]
    private SpriteRenderer spBuffer;
    #endregion

    #region Normal paramters
    private System.Action doneCallback;

    private int index;

    private List<int> listUnlockedBackgrounds;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        listUnlockedBackgrounds = new List<int>();

        Load();
    }

    public void Release()
    {
        index = 0;

        spMain.color = Color.white;
        spMain.sortingOrder = 0;
        spMain.sprite = listBackgrounds[index];
    }

    public void Refresh()
    {
        index = 0;

        spMain.color = Color.white;
        spMain.sortingOrder = 0;
        spMain.sprite = listBackgrounds[index];
    }

    public void UpdateStep(float deltaTime)
    {
        var size = Vector2.zero;
        size.y = Camera.main.orthographicSize * 2;
        size.x = Screen.width * 1.0f / Screen.height * size.y;

        spMain.size = size;
        spBuffer.size = size;
    }

    public void Change(System.Action callback)
    {
        index++;
        if(index >= listUnlockedBackgrounds.Count)
        {
            index = 0;
        }

        var sprite = listBackgrounds[listUnlockedBackgrounds[index]];
        var color = Color.white;
        color.a = 0;

        spBuffer.sprite = sprite;
        spBuffer.color = color;
        spBuffer.sortingOrder = spMain.sortingOrder + Player.Instance.MajorTransformationIntervals;
        spBuffer.sortingLayerName = spMain.sortingLayerName;

        doneCallback = callback;

        spBuffer.DOColor(Color.white, fadeTimes).OnStart(OnFadeStart).OnComplete(OnFadeComplete).Play();
    }
    
    private void OnFadeStart()
    {
        spBuffer.gameObject.SetActive(true);
    }

    private void OnFadeComplete()
    {
        spMain.sprite = spBuffer.sprite;
        spMain.color = spBuffer.color;
        spMain.sortingOrder = spBuffer.sortingOrder;
        spMain.sortingLayerName = spBuffer.sortingLayerName;

        spBuffer.gameObject.SetActive(false);

        if (doneCallback != null)
            doneCallback();
    }

    private void LateUpdate()
    {
        UpdateStep(Time.deltaTime);
    }

    public void UnlockBackground(int themeID)
    {
        listUnlockedBackgrounds.Add(themeID);

        Save();
    }

    public bool IsBackgroundUnlocked(int themeID)
    {
        return listUnlockedBackgrounds.Contains(themeID);
    }

    public void ActiveBackground(int themeID)
    {

    }

    public void DeactiveBackground(int themeID)
    {

    }

    private void Load()
    {
        string data = DataManager.Instance.GetString(LIST_UNLOCKED_BACKGROUNDS, string.Empty);
        Debug.Log(data);
        if(!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if(!string.IsNullOrEmpty(array[i]))
                    listUnlockedBackgrounds.Add(int.Parse(array[i]));
            }
        }

        if(listUnlockedBackgrounds.Count == 0)
        {
            listUnlockedBackgrounds.Add(0);
        }
    }

    private void Save()
    {
        string data = string.Empty;
        for (int i = 0; i < listUnlockedBackgrounds.Count; i++)
        {
            data += listUnlockedBackgrounds[i];
            data += ';';
        }

        DataManager.Instance.SetString(LIST_UNLOCKED_BACKGROUNDS, data);
        DataManager.Instance.Save();
    }

}

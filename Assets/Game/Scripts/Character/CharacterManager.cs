﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoSingleton<CharacterManager>
{

    #region Const parameters
    private const string LIST_UNLOCKED_CHARACTERS_KEY = "ListUnlockedCharacters";
    #endregion

    #region Editor paramters
    [SerializeField]
    private Character selectedCharacter;
    [SerializeField]
    private List<Character> characters;
    [SerializeField]
    private List<Player> players;
	[SerializeField]
	private bool isUnlockAll = false;
    #endregion

    #region Normal paramters
    [SerializeField]
    private List<int> listUnlockedCharacters;
    private Dictionary<EggType, List<int>> dictionaryLockedCharacters;
    #endregion

    #region Encapsulate
    public Character SelectedCharacter
    {
        get
        {
            return selectedCharacter;
        }

        set
        {
            selectedCharacter = value;
        }
    }
    #endregion

    public override void Initialize()
    {
        listUnlockedCharacters = new List<int>();
        dictionaryLockedCharacters = new Dictionary<EggType, List<int>>();

        Load();
    }

    public void Load()
    {
        for (int i = 0; i < characters.Count; i++)
        {
            var character = characters[i];
            var data = DataManager.Instance.GetString(GetCharacterKey(character.Id), string.Empty);
            if (data != string.Empty)
            {
                var loadedCharacter = JsonUtility.FromJson<Character>(data);
                character.Id = loadedCharacter.Id;
                //character.FishName = loadedCharacter.FishName;
                character.Level = loadedCharacter.Level;
                character.MaxLevel = loadedCharacter.MaxLevel;
                character.CurExp = loadedCharacter.CurExp;
                character.TotalExp = loadedCharacter.TotalExp;
                character.Abilities = loadedCharacter.Abilities;
                character.IsBuy = loadedCharacter.IsBuy;
                character.IsSelected = loadedCharacter.IsSelected;

                if (character.IsSelected)
                {
                    selectedCharacter = character;
                }

				if (isUnlockAll)
					character.IsBuy = true;
            }

            if (character.IsBuy)
            {
                listUnlockedCharacters.Add(character.Id);
            }
            else
            {
                if (!dictionaryLockedCharacters.ContainsKey(character.EggType))
                {
                    dictionaryLockedCharacters.Add(character.EggType, new List<int>());
                }

                if (!listUnlockedCharacters.Contains(character.Id))
                {
                    dictionaryLockedCharacters[character.EggType].Add(character.Id);
                }
            }
        }

        if (!selectedCharacter.IsSelected)
        {
            selectedCharacter = characters[0];
        }
    }

    public void Save()
    {
        for (int i = 0; i < characters.Count; i++)
        {
            DataManager.Instance.SetString(GetCharacterKey(characters[i].Id), JsonUtility.ToJson(characters[i]));
        }

        //var data = string.Empty;
        //for (int i = 0; i < listUnlockedCharacters.Count; i++)
        //{
        //    data += listUnlockedCharacters[i];
        //    data += ';';
        //}

        //PlayerPrefs.SetString(LIST_UNLOCKED_CHARACTERS_KEY, data);
        DataManager.Instance.Save();
    }

    private string GetCharacterKey(int id)
    {
        return "CHARACTER_" + id;
    }

    public Character GetCharacterByID(int id)
    {
        if (id >= characters.Count)
            return null;
        return characters[id];
    }

    public Character GetErrorCharacter()
    {
        return null;
    }

    public Player GetCurrentPlayer()
    {
        return Instantiate<Player>(players[selectedCharacter.Id]);
    }

    public Character RandomUnlockCharacter(EggType eggType)
    {
        return null;
    }

    public bool HasLockedCharacter(EggType type)
    {
        if (dictionaryLockedCharacters.ContainsKey(type))
            return dictionaryLockedCharacters[type].Count > 0;
        else
            return false;
    }

    public bool HasLockedCharacter()
    {
        foreach (var item in dictionaryLockedCharacters)
        {
            if (item.Value.Count > 0)
            {
                return true;
            }
        }

        return false;
    }

    public int UnlockCharacter(EggType type)
    {
        int index = UnityEngine.Random.Range(0, dictionaryLockedCharacters[type].Count);
        int characterID = dictionaryLockedCharacters[type][index];

        var characterToUnlock = characters[characterID];
        characterToUnlock.IsBuy = true;

        dictionaryLockedCharacters[type].RemoveAt(index);
        listUnlockedCharacters.Add(characterID);

        return characterID;
    }

    public bool IsCharacterUnlocked(int id)
    {
        return listUnlockedCharacters.Contains(id);
    }

    public int GetTotalUnlockedCharacter()
    {
        return listUnlockedCharacters.Count;
    }

    public void OnApplicationQuit()
    {
        Save();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEnhanceItem : EnhanceItem
{
    [SerializeField]
    private int Id;
    [SerializeField]
    private Character character;
    [SerializeField]
    private DragonBonesType type;
    [SerializeField]
    private DragonBones.UnityArmatureComponent component;

    public Character Character
    {
        get
        {
            return character;
        }

        set
        {
            character = value;
        }
    }

    protected override void OnAwake()
    {
        base.OnAwake();
        character = CharacterManager.Instance.GetCharacterByID(Id);
        if (character == null)
        {
            character = CharacterManager.Instance.GetErrorCharacter();
        }
        component.gameObject.SetActive(false);

        component = DragonBonesManager.Instance.GeneratePlayer((DragonBonesType)character.Id, character.Level);
        component.transform.SetParent(transform, false);
        component.transform.localScale *= 400;

        component.sortingOrder = 8999;
        component.animation.Stop();
    }

    protected override void OnStart()
    {
        base.OnStart();
    }

    protected override void OnClickEnhanceItem()
    {
        base.OnClickEnhanceItem();

        // player some animation when touch
        //component.animation.Play("Bravo");
    }

    protected override void SetItemDepth(float depthCurveValue, int depthFactor, float itemCount)
    {
        base.SetItemDepth(depthCurveValue, depthFactor, itemCount);
        int newDepth = (int)(depthCurveValue * itemCount);
        this.transform.SetSiblingIndex(newDepth);

        if (transform.GetSiblingIndex() == (int)(itemCount - 1))
        {
            UIFishController.Instance.OnScrollCharacter(character);
        }
    }

    public override void SetSelectState(bool isCenter)
    {
        base.SetSelectState(isCenter);

        if (isCenter)
        {
            UIFishController.Instance.OnScrollCharacter(character);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Flags]
public enum CharacterAbilities
{
    None = 1,
    SpeedBoost = 2,
    HealthBoost = 4,
    EggEXPBoost = 8,
}

[System.Serializable]
public class Character
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int id = 0;
    [SerializeField]
    private string fishName;
    [SerializeField]
    private int level = 0;
    [SerializeField]
    private int maxLevel = 3;
    [SerializeField]
    private int curExp = 0;
    [SerializeField]
    private int totalExp = 500;
    [SerializeField]
    [EnumFlags]
    private CharacterAbilities abilities = CharacterAbilities.None;
    [SerializeField]
    private bool isBuy = false;
    [SerializeField]
    private bool isSelected = false;
    [SerializeField]
    private EggType eggType = EggType.Common;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    public int Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    public string FishName
    {
        get
        {
            return fishName;
        }

        set
        {
            fishName = value;
        }
    }

    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    public int MaxLevel
    {
        get
        {
            return maxLevel;
        }

        set
        {
            maxLevel = value;
        }
    }

    public int CurExp
    {
        get
        {
            return curExp;
        }

        set
        {
            curExp = value;
        }
    }

    public int TotalExp
    {
        get
        {
            return totalExp;
        }

        set
        {
            totalExp = value;
        }
    }

    public CharacterAbilities Abilities
    {
        get
        {
            return abilities;
        }

        set
        {
            abilities = value;
        }
    }

    public bool IsBuy
    {
        get
        {
            return isBuy;
        }

        set
        {
            isBuy = value;
        }
    }

    public bool IsSelected
    {
        get
        {
            return isSelected;
        }

        set
        {
            isSelected = value;
        }
    }

    public EggType EggType
    {
        get
        {
            return eggType;
        }

        set
        {
            eggType = value;
        }
    }
    #endregion

    public void GainExp(int exp)
    {
        curExp += exp;
        if(curExp >= totalExp)
        {
            LevelUp();
        }
    }

    private void LevelUp()
    {
        level++;
        abilities |= GetAbilityByLevel(level);

        curExp = 0;
        totalExp = GetTotalExpByLevel(level);
    }

    private CharacterAbilities GetAbilityByLevel(int level)
    {
        switch (level)
        {
            case 1:
                return CharacterAbilities.SpeedBoost;
            case 2:
                return CharacterAbilities.HealthBoost;
            case 3:
                return CharacterAbilities.EggEXPBoost;
            default:
                return CharacterAbilities.None;
        }
    }

    private int GetTotalExpByLevel(int level)
    {
        switch (level)
        {
            case 0:
                return 500;
            case 1:
                return 2000;
            case 2:
                return 4000;
            default:
                return 0;
        }
    }

    public bool IsActiveAbility(CharacterAbilities ability)
    {
        return (abilities & ability) != 0;
    }

}

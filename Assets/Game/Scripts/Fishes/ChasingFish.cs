﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingFish : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int level;
    [SerializeField]
    private float speed = 4;
    [SerializeField]
    private float rotateSpeed = 120;
    [SerializeField]
    private float chaseTime = 60;
    [SerializeField]
    private float chaseTimeCountdown;

    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;
    #endregion

    #region Normal paramters
    private Vector3 velocity;
    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public float RotateSpeed
    {
        get
        {
            return rotateSpeed;
        }

        set
        {
            rotateSpeed = value;
        }
    }

    public float ChaseTime
    {
        get
        {
            return chaseTime;
        }

        set
        {
            chaseTime = value;
        }
    }

    public float ChaseTimeCountdown
    {
        get
        {
            return chaseTimeCountdown;
        }

        set
        {
            chaseTimeCountdown = value;
        }
    }
    #endregion

    public void Initialize()
    {
        //var distance = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        var distance = Player.Instance.transform.position - transform.position;

        var playerAngleZ = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;

        transform.localEulerAngles = new Vector3(0, 0, playerAngleZ);
    }

    public void Release()
    {

    }

    public void Refresh()
    {
        Destroy(armatureComponent.gameObject);
        armatureComponent = DragonBonesManager.Instance.RandomEnemy();
        armatureComponent.transform.SetParent(transform, false);
        armatureComponent.transform.localPosition = Vector3.zero;
        armatureComponent.transform.localEulerAngles = Vector3.zero;
        armatureComponent.transform.localScale = Vector3.one * 0.05f;

        int fishLevel = Player.Instance.Level + 1;
        armatureComponent.sortingOrder = fishLevel;
        //trail.sortingOrder = fishLevel;

        transform.localScale = Vector3.one * Player.Instance.GetSizeAtLevel(fishLevel);



        //Invoke("Recycle", 60);
        ChaseTimeCountdown = chaseTime;
    }

    public void UpdateStep(float deltaTime)
    {
        var direction = GetRotateDirection();
        var angle = transform.localEulerAngles.z;
        if (angle < 0)
            angle += 360;

        angle += rotateSpeed * deltaTime * direction;

        var radian = angle * Mathf.Deg2Rad;

        velocity.x = Mathf.Cos(radian);
        velocity.y = Mathf.Sin(radian);

        transform.localPosition += velocity * speed * deltaTime;
        transform.localEulerAngles = new Vector3(0, 0, angle);

        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360f;

        if (angleZ > 90 && angleZ < 270)
        {
            armatureComponent.armature.flipY = true;
        }
        else
        {
            armatureComponent.armature.flipY = false;
        }

        if(ChaseTimeCountdown > 0)
        {
            ChaseTimeCountdown -= deltaTime;
        }
        else
        {
            if (!CameraController.Instance.CheckContainsInRealCamera(transform.position))
                Recycle();
        }
    }

    /// <summary>
    /// Get rotate direction follow player rotation
    /// </summary>
    /// <returns>
    /// Return -1 if move left
    /// Return 1 if move right
    /// </returns>
    public float GetRotateDirection()
    {
        var distance = Player.Instance.transform.position - transform.position;

        var playerAngleZ = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        if (playerAngleZ < 0)
            playerAngleZ += 360;

        var fishAngleZ = transform.localEulerAngles.z;
        if (fishAngleZ < 0)
            fishAngleZ += 360;

        if (fishAngleZ < 90 && playerAngleZ > 270)
            return -1;
        if (fishAngleZ > 270 && playerAngleZ < 90)
            return 1;

        if (fishAngleZ < playerAngleZ)
            return 1;
        else if (fishAngleZ > playerAngleZ)
            return -1;
        else
            return 0;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (Player.Instance.Level >= level)
                EnemyManager.Instance.RecycleChasingFish(this);

            Player.Instance.EatChasingFish(level);
        }
        else if(collision.gameObject.CompareTag("Enemy"))
        {
            var enemy = collision.gameObject.GetComponent<Enemy>();

            if (enemy.Level > level)
            {
                if (!CameraController.Instance.CheckContainsInRealCamera(transform.position))
                    return;

                var effect = ParticleEffect.Instance.GenerateParticle(ParticleType.Dead);
                effect.gameObject.transform.position = transform.position;
                effect.gameObject.transform.localScale = Vector3.one * Mathf.Pow(2, (Player.Instance.Level % Player.Instance.MajorTransformationIntervals));
                SoundManager.Instance.PlaySFX(SFXType.Die);
                Recycle();
            }
        }
    }

    public void Recycle()
    {
        EnemyManager.Instance.RecycleChasingFish(this);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SwordFishState
{
    Alert,
    Move,

    TOTAL,
    IDLE
}

public class SwordFish : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters    
    [SerializeField]
    private SwordFishState state = SwordFishState.IDLE;
    [SerializeField]
    private Vector3 originalPosition;
    [SerializeField]
    private float speed = 5;
    [SerializeField]
    private Vector3 velocity = Vector3.zero;
    [SerializeField]
    private float size = 1;
    [SerializeField]
    private float alertTimes = 2;

    [SerializeField]
    private GameObject goAlert;
    [SerializeField]
    private GameObject goFish;
    #endregion

    #region Normal paramters
    private float countdownTimes;

    private RectTransform alertRectTransform;
    #endregion

    #region Encapsulate
    public Vector3 OriginalPosition
    {
        get
        {
            return originalPosition;
        }

        set
        {
            originalPosition = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }

    public float Size
    {
        get
        {
            return size;
        }

        set
        {
            size = value;
        }
    }
    #endregion

    public void Initialize()
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        ChangeState(SwordFishState.Alert);
    }

    public void Release()
    {
    }

    public void Refresh()
    {
        ChangeState(SwordFishState.Alert);

        transform.position = Vector3.zero;
        transform.eulerAngles = new Vector3(0, 0, 180);
        transform.localScale = new Vector3(1, -1, 1);
    }

    public void UpdateStep(float deltaTime)
    {
        if (state == SwordFishState.Alert)
        {
            countdownTimes -= deltaTime;
            if (countdownTimes <= 0)
            {
                ChangeState(SwordFishState.Move);
                return;
            }

            var cameraPosition = Camera.main.transform.position;
            var cameraSize = Camera.main.orthographicSize;
            var position = transform.position;

            position.x = Camera.main.orthographicSize * Screen.width / Screen.height + cameraPosition.x;
            position.y = Mathf.Clamp(position.y, cameraPosition.y - cameraSize * 0.75f, cameraPosition.y + cameraSize * 0.75f);

            transform.position = position;

            // Hard code ...
            alertRectTransform.anchoredPosition = new Vector2(-20.0f, (position.y - cameraPosition.y) / cameraSize * (UIManager.Instance.GetRectTransform().sizeDelta.y / 2));
        }
        else if (state == SwordFishState.Move)
        {
            transform.localPosition += velocity * speed * deltaTime;
        }
    }

    public void ChangeState(SwordFishState nextState)
    {
        if (nextState == state)
            return;

        state = nextState;

        if (state == SwordFishState.Alert)
        {
            countdownTimes = alertTimes;
            goAlert = AlertEffect.Instance.AddEffect();
            alertRectTransform = goAlert.GetComponent<RectTransform>();
            goFish.SetActive(false);
        }
        else if (state == SwordFishState.Move)
        {
            //goAlert.SetActive(false);
            AlertEffect.Instance.RemoveEffect(goAlert);
            goAlert = null;
            alertRectTransform = null;

            goFish.SetActive(true);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.EndGame();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Collector") && state == SwordFishState.Move)
        {
            FishManager.Instance.RecycleSwordFish(this);
        }
    }

    public void ApplyChanges()
    {
        ChangeState(SwordFishState.Alert);

        transform.position = originalPosition;
        transform.eulerAngles = new Vector3(0, 0, 180);
        transform.localScale = new Vector3(1, -1, 1) * size;

        var cameraPosition = Camera.main.transform.position;
        var cameraSize = Camera.main.orthographicSize;
        var position = transform.position;

        position.x = Camera.main.orthographicSize * Screen.width / Screen.height + cameraPosition.x;
        position.y = Mathf.Clamp(position.y, cameraPosition.y - cameraSize * 0.75f, cameraPosition.y + cameraSize * 0.75f);

        transform.position = position;
        Debug.Log(Camera.main.WorldToScreenPoint(transform.position));
        Debug.Log(Camera.main.WorldToViewportPoint(transform.position));
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenFish : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int level;
    [SerializeField]
    private float speed = 4;
    [SerializeField]
    private float rotateSpeed = 15;
    [SerializeField]
    private float changeTime = 5;
    [SerializeField]
    private float changeTimeCountdown;
    [SerializeField]
    private float waitTime = 1;
    [SerializeField]
    private float waitTimeCountdown;

    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;
    [SerializeField]
    private TrailRenderer trail;
    #endregion

    #region Normal paramters
    private float nextAngle;

    private Vector3 velocity;

    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
        }
    }
    #endregion

    public void Initialize()
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        nextAngle = Random.Range(0, 360);

        changeTimeCountdown = changeTime;
        waitTimeCountdown = waitTime;

        //armatureComponent = DragonBonesManager.Instance.GenerateEnemy(28);
        //armatureComponent.transform.SetParent(transform, false);

        armatureComponent.transform.localPosition = Vector3.zero;
        armatureComponent.transform.localRotation = Quaternion.identity;
        //armatureComponent.transform.localScale = Vector3.one;


        int fishLevel = Player.Instance.Level + 1;
        armatureComponent.sortingOrder = fishLevel;
        trail.sortingOrder = fishLevel;

        transform.localScale = Vector3.one * Player.Instance.GetSizeAtLevel(fishLevel);
    }

    public void Release()
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        nextAngle = Random.Range(0, 360);

        changeTimeCountdown = changeTime;
        waitTimeCountdown = waitTime;
    }

    public void Refresh()
    {
        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        nextAngle = Random.Range(0, 360);

        changeTimeCountdown = changeTime;
        waitTimeCountdown = waitTime;
    }

    public void UpdateStep(float deltaTime)
    {
        if (waitTimeCountdown > 0)
        {
            waitTimeCountdown -= deltaTime;
            return;
        }

        if (changeTimeCountdown > 0)
        {
            changeTimeCountdown -= deltaTime;
        }
        else
        {
            changeTimeCountdown = changeTime;
            nextAngle = Random.Range(0, 360);
        }

        var angle = transform.localEulerAngles.z;
        if (angle < 0)
            angle += 360;

        if (angle < nextAngle)
        {
            angle += rotateSpeed * deltaTime;
            angle = Mathf.Min(angle, nextAngle);
        }
        else if (angle > nextAngle)
        {
            angle -= rotateSpeed * deltaTime;
            angle = Mathf.Max(angle, nextAngle);
        }

        var radian = angle * Mathf.Deg2Rad;

        velocity.x = Mathf.Cos(radian);
        velocity.y = Mathf.Sin(radian);

        transform.localPosition += velocity * speed * deltaTime;
        transform.localEulerAngles = new Vector3(0, 0, angle);

        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360f;

        if (angleZ > 90 && angleZ < 270)
        {
            armatureComponent.armature.flipY = true;
        }
        else
        {
            armatureComponent.armature.flipY = false;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            EnemyManager.Instance.RecycleGoldenFish(this);
            Player.Instance.EatGoldenFish(level);
        }
    }

}

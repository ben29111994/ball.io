﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShadowFish : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private int level;
    [SerializeField]
    private Vector3 velocity;
    [SerializeField]
    private float changeTime = 1.0f;
    [SerializeField]
    private float lifeTime = 10.0f;

    [Header("References")]
    [SerializeField]
    private SpriteRenderer spImage;
    #endregion

    #region Normal paramters
    private float changeTimeCountdown;

    private bool isFading;

    private float lifeTimeCountdown;
    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }
    #endregion

    public void Initialize()
    {
        velocity = Random.insideUnitCircle;

        if (velocity.x > 0)
            spImage.flipX = true;
        else if (velocity.x < 0)
            spImage.flipX = false;
    }

    public void Release()
    {
    }

    public void Refresh()
    {
        velocity = Random.insideUnitCircle;

        if (velocity.x > 0)
            spImage.flipX = true;
        else if (velocity.x < 0)
            spImage.flipX = false;

        lifeTimeCountdown = lifeTime;
    }

    public void UpdateStep(float deltaTime)
    {
        transform.position += velocity * deltaTime;

        changeTimeCountdown -= deltaTime;
        if (changeTimeCountdown <= 0.0f)
        {
            changeTimeCountdown = changeTime;
            velocity = Random.insideUnitCircle;

            if (spImage.flipX)
            {
                velocity.x = Mathf.Abs(velocity.x);
            }
            else
            {
                velocity.x = -Mathf.Abs(velocity.x);
            }
        }

        lifeTimeCountdown -= deltaTime;
        if (lifeTimeCountdown <= 0.0f)
        {
            if (IsHide())
            {
                EnemyManager.Instance.RecycleShadowFish(this);
                return;
            }

            if (IsShow())
                Fade(0.5f, 0.0f, 2.0f);

            return;
        }
    }

    public void ChangeImage(Sprite sprite)
    {
        spImage.sprite = sprite;
        spImage.sortingOrder = (Player.Instance.Level / Player.Instance.MajorTransformationIntervals) * Player.Instance.MajorTransformationIntervals;
        Fade(0.0f, 0.5f, 2.0f);
    }

    public void RandomLevel()
    {
        level = Random.Range(1, 30);

        transform.localScale = Vector3.one * level * Mathf.Pow(2, Player.Instance.Level % Player.Instance.MajorTransformationIntervals);
    }
    
    public void Fade(float from, float to, float duration)
    {
        Color color = new Color(0, 0, 0, from);
        spImage.color = color;

        spImage.DOFade(to, duration).OnStart(delegate { isFading = true; }).OnComplete(delegate { isFading = false; }).Play();
    }

    public bool IsShow()
    {
        return spImage.color.a == 0.5f;
    }

    public bool IsHide()
    {
        return spImage.color.a == 0.0f;
    }
}

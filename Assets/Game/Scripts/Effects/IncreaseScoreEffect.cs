﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncreaseScoreEffect : MonoSingleton<IncreaseScoreEffect>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Text originalText;
    #endregion

    #region Normal paramters
    private List<Text> activeTexts;
    private Queue<Text> inactiveTexts;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        activeTexts = new List<Text>();
        inactiveTexts = new Queue<Text>();
    }

    public void Release()
    {
    }

    public void Refresh()
    {
        for (int i = 0; i < activeTexts.Count; i++)
        {
            activeTexts[i].gameObject.SetActive(false);
            inactiveTexts.Enqueue(activeTexts[i]);
            activeTexts.RemoveAt(i);
            i--;
        }
    }

    public void UpdateStep(float deltaTime)
    {
    }

    public void ActiveEffect(int score, Vector3 position)
    {
        if (!gameObject.activeInHierarchy)
            return;

        Text text;

        if (inactiveTexts.Count > 0)
        {
            text = inactiveTexts.Dequeue();
        }
        else
        {
            text = Instantiate(originalText, transform);
        }

        text.gameObject.SetActive(true);

        text.text = "+" + score.ToString();
        text.color = Color.white;
        var rectTransform = text.gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = position;

        activeTexts.Add(text);

        StartCoroutine(C_Effect(text, rectTransform));
    }

    private IEnumerator C_Effect(Text text, RectTransform rectTransform)
    {
        var color = text.color;
        while (color.a > 0)
        {
            color.a -= Time.deltaTime;

            text.color = color;
            rectTransform.anchoredPosition += Vector2.up * 1000 * Time.deltaTime;

            yield return null;
        }

        text.gameObject.SetActive(false);

        inactiveTexts.Enqueue(text);
        activeTexts.Remove(text);
    }

    public void OnDisable()
    {
        Refresh();
    }
}

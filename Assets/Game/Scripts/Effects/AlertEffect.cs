﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertEffect : MonoSingleton<AlertEffect>
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private GameObject originalAlert;
    #endregion

    #region Normal paramters
    private List<GameObject> activeAlerts;
    private Queue<GameObject> inactiveAlerts;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        activeAlerts = new List<GameObject>();
        inactiveAlerts = new Queue<GameObject>();
    }

    public void Release()
    {
        for (int i = 0; i < activeAlerts.Count; i++)
        {
            activeAlerts[i].SetActive(false);
            inactiveAlerts.Enqueue(activeAlerts[i]);
            activeAlerts.RemoveAt(i);
            i--;
        }
    }

    public void Refresh()
    {
        for (int i = 0; i < activeAlerts.Count; i++)
        {
            activeAlerts[i].SetActive(false);
            inactiveAlerts.Enqueue(activeAlerts[i]);
            activeAlerts.RemoveAt(i);
            i--;
        }
    }

    public void UpdateStep(float deltaTime)
    {
    }

    public GameObject AddEffect()
    {
        if (!gameObject.activeInHierarchy)
            return null;

        GameObject alert;

        if(inactiveAlerts.Count > 0)
        {
            alert = inactiveAlerts.Dequeue();
        }
        else
        {
            alert = Instantiate<GameObject>(originalAlert, transform);
        }

        alert.SetActive(true);

        activeAlerts.Add(alert);

        return alert;
    }

    public void RemoveEffect(GameObject alert)
    {
        activeAlerts.Remove(alert);
        inactiveAlerts.Enqueue(alert);

        alert.SetActive(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionEffect : MonoSingleton<DirectionEffect>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private RectTransform rtDirection;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
    }

    public void Active()
    {
        rtDirection.gameObject.SetActive(true);
    }

    public void Deactive()
    {
        rtDirection.gameObject.SetActive(false);
    }

    public void SetRotation(Vector3 delta, float radian)
    {
        Vector3 newPosition = Vector3.zero;
        newPosition.x = 200.0f * delta.x;
        newPosition.y = 200.0f * delta.y;

        rtDirection.anchoredPosition = newPosition;
    }

}

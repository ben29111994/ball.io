﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(ParticleSystem))]
public class AutoRecycleWhenParticleSystemCompleted : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private ParticleType type;
    [SerializeField]
    private ParticleSystem system;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public void Initialize()
    {
        if (system == null)
            system = gameObject.GetComponent<ParticleSystem>();

        StartCoroutine(C_CheckIfRunning());
    }
    
    public void Release()
    {
        ParticleEffect.Instance.RecycleParticle(type, system);
    }

    private IEnumerator C_CheckIfRunning()
    {
        WaitForSeconds wait = new WaitForSeconds(0.1f);

        yield return new WaitForSeconds(5.0f);
        //yield return null;

        Release();
    }

    public void Awake()
    {
        Initialize();
    }

    public void OnEnable()
    {
        StartCoroutine(C_CheckIfRunning());
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ParticleType
{
    LevelUp,
    Heart,
    Dead,

    TOTAL,
    IDLE
}


public class ParticleEffect : MonoSingleton<ParticleEffect>
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private ParticleSystem levelUpParticle;
    [SerializeField]
    private ParticleSystem heartParticle;
    [SerializeField]
    private ParticleSystem deadParticle;
    #endregion

    #region Normal paramters
    private Dictionary<ParticleType, Queue<ParticleSystem>> dictionaryParticleSystemByType;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        dictionaryParticleSystemByType = new Dictionary<ParticleType, Queue<ParticleSystem>>();

        int count = (int)ParticleType.TOTAL;
        for (int i = 0; i < count; i++)
        {
            dictionaryParticleSystemByType.Add((ParticleType)i, new Queue<ParticleSystem>());
        }
    }

    public void Release()
    {
    }

    public void Refresh()
    {
    }

    public void UpdateStep(float deltaTime)
    {
    }

    public ParticleSystem GenerateParticle(ParticleType type)
    {
        var inactiveParticleSystem = dictionaryParticleSystemByType[type];

        ParticleSystem system = null;
        if(inactiveParticleSystem.Count > 0)
        {
            system = inactiveParticleSystem.Dequeue();
        }
        else
        {
            Debug.Log(type);
            switch (type)
            {
                case ParticleType.LevelUp:
                    system = Instantiate<ParticleSystem>(levelUpParticle, transform);
                    system.gameObject.SetActive(false);
                    break;
                case ParticleType.Heart:
                    system = Instantiate<ParticleSystem>(heartParticle, transform);
                    system.gameObject.SetActive(false);
                    break;
                case ParticleType.Dead:
                    system = Instantiate<ParticleSystem>(deadParticle, transform);
                    system.gameObject.SetActive(false);
                    break;
                default:
                    break;
            }
        }

        system.gameObject.SetActive(true);
        system.Play();

        return system;
    }

    public void RecycleParticle(ParticleType type, ParticleSystem system)
    {
        var inactiveParticleSystem = dictionaryParticleSystemByType[type];
        inactiveParticleSystem.Enqueue(system);

        system.transform.SetParent(transform);
        system.gameObject.SetActive(false);
        system.Stop();
    }

}

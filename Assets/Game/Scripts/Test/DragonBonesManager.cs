﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DragonBones;

[System.Serializable]
public class DragonBonesPlayerData
{
    public DragonBonesType type;
    public List<UnityArmatureComponent> levels;
}

[System.Serializable]
public class DragonBonesEnemyData
{
    public DragonBonesType type;
    public List<UnityArmatureComponent> objects;
}

public enum DragonBonesType
{
    // Player 
    Cattie,
    Fattie_Dino,
    Pilot_Fishie,
    Pig_Fish,
    Ocean_Mouse,
    Rabbie,
    Reddie_Horn,
    Cosplay_Finnie,
    Rockie,
    Uni_Piggie,
    // new player phase 2
    Penguins,
    Submarine,
    Dragonish,
    Dolphin,
    Beaver,
    Koi,
    Unicorn,
    Hippocampus,
    Carp,

    //Enemy
    Enemy_0,
    Enemy_1,
    Enemy_2,
    Enemy_3,
    Enemy_4,
    Enemy_5,
    Enemy_6,
    Enemy_7,
    Enemy_8,
    Enemy_9,
    Enemy_10,
    Enemy_11,
    Enemy_12,
    Enemy_13,
    Enemy_14,
    Enemy_15,
    Enemy_16,
    Enemy_17,
    Enemy_18,
    Enemy_19,
    Enemy_20,
    Enemy_21,
    Enemy_22,
    Enemy_23,
    Enemy_24,
    Enemy_25,
    Enemy_26,
    Enemy_27,
    // new enemy phase 2
    Enemy_28,
    Enemy_29,
    Enemy_30,
    Enemy_31,
    Enemy_32,
    Enemy_33,
    Enemy_34,
    Enemy_35,
    Enemy_36,
    Enemy_37,
}

public class DragonBonesManager : MonoSingleton<DragonBonesManager>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<UnityArmatureComponent> listPlayers;
    [SerializeField]
    private List<UnityArmatureComponent> listEnemies;

    [SerializeField]
    private List<DragonBonesPlayerData> listPlayerData;
    [SerializeField]
    private List<DragonBonesEnemyData> listEnemyData;
    [SerializeField]
    private List<UnityArmatureComponent> listDecorFish;
    #endregion

    #region Normal paramters
    private Dictionary<DragonBonesType, List<UnityArmatureComponent>> playerData;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        playerData = new Dictionary<DragonBonesType, List<UnityArmatureComponent>>();
        for (int i = 0; i < listPlayerData.Count; i++)
        {
            var data = listPlayerData[i];
            if (!playerData.ContainsKey(data.type))
            {
                playerData.Add(data.type, new List<UnityArmatureComponent>());
            }

            playerData[data.type] = data.levels;
        }
    }

    public void Release()
    {
    }

    public UnityArmatureComponent GeneratePlayer(int id)
    {
        return Instantiate<UnityArmatureComponent>(listPlayers[id]);
    }

    public UnityArmatureComponent GenerateEnemy(int id)
    {
        return Instantiate<UnityArmatureComponent>(listEnemies[id]);
    }

    public void Generate()
    {
        GeneratePlayer(0);
    }

    public UnityArmatureComponent GenerateDecorFish(string artName)
    {
        return null;
    }

    public UnityArmatureComponent GenerateDecorFish(int id)
    {
        if (id >= listDecorFish.Count)
            return null;
        else
            return Instantiate(listDecorFish[id]);
    }

    public UnityArmatureComponent GeneratePlayer(DragonBonesType type, int level)
    {
        if (playerData[type].Count > level)
            return Instantiate(playerData[type][level]);
        else
            return null;
    }

    public UnityArmatureComponent RandomEnemy()
    {
        int id = UnityEngine.Random.Range(15, 20);
        Debug.Log(id);
        return Instantiate<UnityArmatureComponent>(listEnemyData[id].objects[0]);
    }
}

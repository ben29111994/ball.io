﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public Vector3 velocity;
    // Use this for initialization
    void Start()
    {
        var radianZ = transform.localEulerAngles.z * Mathf.Deg2Rad;

        velocity.x = Mathf.Cos(radianZ);
        velocity.y = Mathf.Sin(radianZ);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition += velocity * Time.deltaTime;
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Collector"))
            Destroy(gameObject);
    }
}

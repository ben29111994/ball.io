﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestManager : MonoBehaviour
{
    public float scaleValue;
    public float scaleByLevel;

    public float sizeScale = 0.25f;
    public float worldScale = 0.5f;

    public GameObject player;
    public GameObject enemy;
    public Transform enemyHolder;

    public float screenHeight;
    public float screenWidth;
    public float screenLength;

    public int curLevel;
    public int minLevel;
    public int maxLevel;

    private List<GameObject> activeEnemies;

    void Start()
    {
        screenHeight = Camera.main.orthographicSize;
        screenWidth = Screen.width * screenHeight / Screen.height;

        screenLength = screenHeight * screenHeight + screenWidth * screenWidth;
        screenLength = Mathf.Sqrt(screenLength);
        
        curLevel = 0;

        minLevel = curLevel / 3  * 3;

        if (minLevel < 0)
            minLevel = 0;

        maxLevel = curLevel + 6;

        activeEnemies = new List<GameObject>();

        StartCoroutine(C_Random());
    }

    private IEnumerator C_Random()
    {
        WaitForSeconds seconds = new WaitForSeconds(0.5f);

        while (true)
        {
            for (int i = curLevel; i < curLevel + 3; i++)
            {

                int level = UnityEngine.Random.Range(minLevel, maxLevel);
                level = i;
                float levelScale = Mathf.Pow(1 / scaleValue, level);

                float scaleSizeByLevel = 1 + sizeScale * level;
                float scaleWorldByLevel = Mathf.Pow(1 / worldScale, level);

                float realScale = scaleSizeByLevel * scaleWorldByLevel;
                


                var e = Instantiate(enemy, enemyHolder, false);
                e.gameObject.SetActive(true);
                e.name += " " + (level);

                var eAngle = new Vector3(0, 0, UnityEngine.Random.Range(0.0f, 360.0f));

                var playerAngleZ = player.transform.localEulerAngles.z;
                if (playerAngleZ <= 0)
                    playerAngleZ += 360;

                eAngle.z = playerAngleZ - 180.0f;

                var ePositionRadian = UnityEngine.Random.Range(playerAngleZ - 10, playerAngleZ + 10) * Mathf.Deg2Rad;

                var ePosition = new Vector3(
                    Mathf.Cos(ePositionRadian) * screenLength + player.transform.localPosition.x,
                    Mathf.Sin(ePositionRadian) * screenLength + player.transform.localPosition.y
                    );

                ePosition *= level + 1;

                var eScale = Vector3.one;
                eScale *= realScale;
                //eScale *= scaleByLevel * level;

                e.transform.localPosition = ePosition;
                e.transform.localEulerAngles = eAngle;
                e.transform.localScale = eScale;

                activeEnemies.Add(e);
            }

            yield return seconds;
        }
    }

    public void Scale()
    {
        curLevel++;

        float scaleSizeByLevel = 1 + sizeScale * curLevel;
        float scaleWorldByLevel = Mathf.Pow(1 / worldScale, curLevel);

        float realScale = scaleSizeByLevel * scaleWorldByLevel;

        minLevel = curLevel / 3 * 3;

        if (minLevel < 0)
            minLevel = 0;

        maxLevel = curLevel + 6;

        enemyHolder.DOScale(enemyHolder.localScale.x * worldScale, 1.0f);
        player.transform.DOScale(realScale, 1.0f);

        var playerScale = player.transform.localScale.x * (1 / scaleValue);

        if (curLevel % 3 == 0)
        {
            for (int i = 0; i < activeEnemies.Count; i++)
            {
                var e = activeEnemies[i];
                if (e != null && e.transform.localScale.x < playerScale)
                {
                    if (activeEnemies.Remove(e))
                    {
                        DestroyObject(e);
                        i--;
                    }
                }
            }
        }

    }
}

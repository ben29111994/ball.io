﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ScaleTest : MonoBehaviour
{
    public float increaseSize;
    public int level = 0;
    public int majorLevel = 3;

    public GameObject player;
    public Camera mainCamera;

    public float playerSize;
    public float prevSize;
    public float nextSize;

    public float cameraSize;
    public float prevCameraSize;
    public float nextCameraSize;

    public bool isUpdate;

    public void Awake()
    {
        playerSize = transform.localScale.x;
        prevSize = nextSize = playerSize;

        cameraSize = mainCamera.orthographicSize;
        prevCameraSize = nextCameraSize = cameraSize;
    }

    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.L))
        {
            LevelUp();
        }

        if (isUpdate)
        {
            Debug.Log(mainCamera.orthographicSize);
        }
    }

    public void LevelUp()
    {
        level++;
        playerSize += increaseSize;
        cameraSize *= 2;

        if (level % majorLevel == 0)
        {
            prevSize = (playerSize - increaseSize) / 2;
            nextSize = playerSize;

            player.transform.DOScale(nextSize, 1.0f)
                .OnStart(delegate
                {
                    player.transform.localScale = Vector3.one * prevSize;
                })
                .Play();

            cameraSize = 5;
            prevCameraSize = 2.5f;
            nextCameraSize = 5;
            mainCamera.DOOrthoSize(nextCameraSize, 1.0f)
                .OnStart(delegate
                {
                    mainCamera.orthographicSize = prevCameraSize;
                    isUpdate = true;
                })
                .OnComplete(delegate
                {
                    isUpdate = false;
                })
                .Play();
        }
        else
        {
            prevSize = nextSize;
            nextSize = playerSize * Mathf.Pow(2, level % majorLevel);
            player.transform.DOScale(nextSize, 1.0f);

            prevCameraSize = nextCameraSize;
            nextCameraSize = cameraSize;

            mainCamera.DOOrthoSize(nextCameraSize, 1.0f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyTest : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("UI references")]
    [SerializeField]
    private TMPro.TextMeshProUGUI tmpMoney;

    [Header("Debug only")]
    [SerializeField]
    private string prevCollectTimeString;
    [SerializeField]
    private string nextCollectTimeString;
    [SerializeField]
    private int bonusMoney = 60;
    [SerializeField]
    private int bonusMoneyCount;
    [SerializeField]
    private int feedTime = 60 * 60 * 6;
    [SerializeField]
    private float feedTimeCountdown;
    #endregion

    #region Normal paramters
    private System.DateTime prevCollectTime;
    private System.DateTime nextCollectTime;
    #endregion

    #region Encapsulate
    #endregion

    public void Initialize()
    {
        prevCollectTime = System.DateTime.Now.AddDays(-1);
        nextCollectTime = System.DateTime.Now;

        nextCollectTimeString = nextCollectTime.ToString();
        prevCollectTimeString = prevCollectTime.ToString();


        bonusMoneyCount = 0;
        feedTimeCountdown = feedTime;
    }

    public void Release()
    {
    }

    public void Refresh()
    {
    }

    public void UpdateStep(float deltaTime)
    {
        nextCollectTime = System.DateTime.Now;
        nextCollectTimeString = nextCollectTime.ToString();

        var timeSpan = nextCollectTime - prevCollectTime;
        if(timeSpan.TotalMinutes >= 1)
        {
            prevCollectTime = prevCollectTime.AddMinutes(timeSpan.TotalMinutes);
            prevCollectTimeString = prevCollectTime.ToString();

            int count = (int)timeSpan.TotalMinutes;
            for (int i = 0; i < count; i++)
            {
                feedTimeCountdown = Mathf.Max(feedTimeCountdown - 60, 0);

                bonusMoneyCount += Mathf.Max((int)((bonusMoney * (feedTimeCountdown / feedTime))), 0);
                Debug.Log(string.Format("Money: {0} Minutes: {1}", bonusMoneyCount, i));
            }
            tmpMoney.text = string.Format("Money: {0}", bonusMoneyCount);
        }
    }

    public void Awake()
    {
        Initialize();
    }

    public void Update()
    {
        UpdateStep(Time.deltaTime);
    }
}

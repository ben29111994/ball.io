﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPButton : ShopButton
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Information")]
    [SerializeField]
    private string iapID;
    [SerializeField]
    private string iapName;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        RefreshUI();
    }

    public override void OnBtnBuyClicked()
    {
        base.OnBtnBuyClicked();

        UnityIAPManager.Instance.BuyProductID(iapID, new System.Action<string>(PurchaseSuccesful), new System.Action<string>(PurchaseFailed));
    }

    public override void OnBtnHelpClicked()
    {
        base.OnBtnHelpClicked();
    }

    public override void RefreshUI()
    {
        base.RefreshUI();

        var product = UnityIAPManager.Instance.GetProductID(iapID);

        tmpTitle.text = iapName;
        tmpPrice.text = "";

        btnBuy.interactable = false;

        if (product != null)
        {
            //tmpTitle.text = product.metadata.localizedTitle;
            tmpPrice.text = product.metadata.localizedPriceString;

            btnBuy.interactable = true;
        }
    }

    private void PurchaseSuccesful(string result)
    {
        GameManager.Instance.HandleIAPPurchase(result);
    }

    private void PurchaseFailed(string result)
    {
        Debug.Log(result);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggButton : ShopButton
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Information")]
    [SerializeField]
    private EggType eggType;
    [SerializeField]
    private string eggName;
    [SerializeField]
    private int eggPrice;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        RefreshUI();
    }

    public override void OnBtnBuyClicked()
    {
        base.OnBtnBuyClicked();

        if (DataManager.Instance.Money > eggPrice)
        {
            if (CharacterManager.Instance.HasLockedCharacter(eggType))
            {
                DataManager.Instance.Money -= eggPrice;
                var characterID = CharacterManager.Instance.UnlockCharacter(eggType);
                UIUnlockFishController.Instance.Show(characterID);
            }
            else
            {
                // show panel no more fish
                Debug.Log("ShopItem: No more fish for egg: " + eggType);
                PopupController.Instance.ShowNotifyPopup("Shop", "No more fish for this egg");
            }
        }
        else
        {
            Debug.Log("ShopItem: Not enoug money.");
            PopupController.Instance.ShowNotifyPopup("Shop", "Not enough money");

        }
    }

    public override void OnBtnHelpClicked()
    {
        base.OnBtnHelpClicked();
    }

    public override void RefreshUI()
    {
        base.RefreshUI();

        tmpTitle.text = eggName;
        tmpPrice.text = eggPrice.ToString();
    }

}

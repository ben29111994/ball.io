﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopButton : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("UI references")]
    [SerializeField]
    protected TextMeshProUGUI tmpTitle;
    [SerializeField]
    protected TextMeshProUGUI tmpPrice;
    [SerializeField]
    protected Image imgBackground;
    [SerializeField]
    protected Image imgItem;
    [SerializeField]
    protected Button btnBuy;
    [SerializeField]
    protected Button btnHelp;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public virtual void Initialize()
    {
        btnBuy.onClick.AddListener(OnBtnBuyClicked);
        btnHelp.onClick.AddListener(OnBtnHelpClicked);
    }

    public virtual void OnBtnBuyClicked()
    {
        SoundManager.Instance.PlaySFX(SFXType.Purchase);
    }

    public virtual void OnBtnHelpClicked()
    {

    }

    public virtual void RefreshUI()
    {

    }

    public void Awake()
    {
        Initialize();

        UIShopController.Instance.AddShopButton(this);
    }

}

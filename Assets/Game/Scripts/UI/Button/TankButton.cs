﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankButton : ShopButton
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Button btnSelect;
    [SerializeField]
    private Button btnSelected;

    [Header("Information")]
    [SerializeField]
    private int tankID;
    [SerializeField]
    private string tankName;
    [SerializeField]
    private int tankPrice;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        btnSelect.onClick.AddListener(OnBtnSelectClicked);

        RefreshUI();
    }

    public override void OnBtnBuyClicked()
    {
        base.OnBtnBuyClicked();

        if (DataManager.Instance.Money >= tankPrice)
        {
            Tank.Instance.UnlockTank(tankID);
            DataManager.Instance.Money -= tankPrice;

            PopupController.Instance.ShowNotifyPopup("Shop", "Buy completed");
        }
        else
        {
            PopupController.Instance.ShowNotifyPopup("Shop", "Not enough money");
        }

        RefreshUI();
    }

    public override void OnBtnHelpClicked()
    {
        base.OnBtnHelpClicked();
    }

    public override void RefreshUI()
    {
        base.RefreshUI();

        tmpPrice.text = tankPrice.ToString();
        tmpTitle.text = tankName;

        if(Tank.Instance.IsUnlockedTank(tankID))
        {
            btnBuy.gameObject.SetActive(false);

            bool isSelected = Tank.Instance.ActiveTankID == tankID;
            btnSelect.gameObject.SetActive(!isSelected);
            btnSelected.gameObject.SetActive(isSelected);
        }
        else
        {
            btnBuy.gameObject.SetActive(true);
            btnSelect.gameObject.SetActive(false);
            btnSelected.gameObject.SetActive(false);
        }           
    }

    private void OnBtnSelectClicked()
    {
        Tank.Instance.ChangeTank(tankID);
        UIShopController.Instance.RefreshUI();
    }

    public void OnEnable()
    {
        RefreshUI();
    }

}

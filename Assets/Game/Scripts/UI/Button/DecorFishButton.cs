﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorFishButton : ShopButton
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Information")]
    [SerializeField]
    private int fishID;
    [SerializeField]
    private string fishName;
    [SerializeField]
    private int fishPrice;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        RefreshUI();
    }

    public override void OnBtnBuyClicked()
    {
        base.OnBtnBuyClicked();

        if (DataManager.Instance.Money >= fishPrice)
        {
            DataManager.Instance.Money -= fishPrice;
            Tank.Instance.UnlockedDecorFish(fishID);
            PopupController.Instance.ShowNotifyPopup("Shop", "Done");

        }
        else
        {
            Debug.Log("ShopItem: Not enoug money.");
            PopupController.Instance.ShowNotifyPopup("Shop", "Not enough money");

        }
    }

    public override void OnBtnHelpClicked()
    {
        base.OnBtnHelpClicked();
        PopupController.Instance.ShowNotifyPopup("Decor Fish", "Gain 1 money per minute");

    }

    public override void RefreshUI()
    {
        base.RefreshUI();

        tmpTitle.text = fishName;
        tmpPrice.text = fishPrice.ToString();
    }

}

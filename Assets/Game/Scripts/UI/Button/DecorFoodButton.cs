﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorFoodButton : ShopButton
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Information")]
    [SerializeField]
    private int foodID;
    [SerializeField]
    private string foodName;
    [SerializeField]
    private int foodPrice;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        RefreshUI();
    }

    public override void OnBtnBuyClicked()
    {
        base.OnBtnBuyClicked();

        if (DataManager.Instance.Money >= foodPrice)
        {
            DataManager.Instance.Money -= foodPrice;
            DataManager.Instance.Food++;
            DataManager.Instance.Save();

            PopupController.Instance.ShowNotifyPopup("Shop", "Done");

        }
        else
        {
            Debug.Log("ShopItem: Not enoug money.");
            PopupController.Instance.ShowNotifyPopup("Shop", "Not enough money");

        }
    }

    public override void OnBtnHelpClicked()
    {
        base.OnBtnHelpClicked();
        PopupController.Instance.ShowNotifyPopup("Decor Fish", "Gain 1 money per minute");

    }

    public override void RefreshUI()
    {
        base.RefreshUI();

        tmpTitle.text = foodName;
        tmpPrice.text = foodPrice.ToString();
    }

}

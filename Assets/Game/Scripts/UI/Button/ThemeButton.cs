﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThemeButton : ShopButton
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Button btnActive;
    [SerializeField]
    private Button btnDeactive;

    [Header("Information")]
    [SerializeField]
    private int themeID;
    [SerializeField]
    private string themeName;
    [SerializeField]
    private int themePrice;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        base.Initialize();

        btnActive.onClick.AddListener(OnBtnActiveClicked);
        btnDeactive.onClick.AddListener(OnBtnDeactiveClicked);

        RefreshUI();
    }

    public override void OnBtnBuyClicked()
    {
        base.OnBtnBuyClicked();

        if(DataManager.Instance.Money >= themePrice)
        {
            DataManager.Instance.Money -= themePrice;
            Background.Instance.UnlockBackground(themeID);

            PopupController.Instance.ShowNotifyPopup("Shop", "Buy completed.");
        }
        else
        {
            PopupController.Instance.ShowNotifyPopup("Shop", "Not enough money.");
        }

        RefreshUI();
    }

    public override void OnBtnHelpClicked()
    {
        base.OnBtnHelpClicked();
    }

    public override void RefreshUI()
    {
        base.RefreshUI();

        tmpPrice.text = themePrice.ToString();
        tmpTitle.text = themeName;

        if (Background.Instance.IsBackgroundUnlocked(themeID))
        {
            btnActive.gameObject.SetActive(true);
            btnDeactive.gameObject.SetActive(false);
            btnBuy.gameObject.SetActive(false);
            btnHelp.gameObject.SetActive(false);
        }
        else
        {
            btnActive.gameObject.SetActive(false);
            btnDeactive.gameObject.SetActive(false);
            btnBuy.gameObject.SetActive(true);
            //btnHelp.gameObject.SetActive(true);
        }
    }

    private void OnBtnActiveClicked()
    {

    }

    private void OnBtnDeactiveClicked()
    {

    }

}


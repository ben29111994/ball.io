﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorFishNotificationPopup : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    DecorFishNotificationItem originalItem;
    [SerializeField]
    private List<DecorFishNotificationItem> listItems;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public void Show()
    {

    }

    public void Hide()
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupController : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private NotifyPopup notifyPopup;
    [SerializeField]
    private RevivePopup revivePopup;
    #endregion

    #region Normal paramters
    private static PopupController instance;
    #endregion

    #region Encapsulate
    public static PopupController Instance
    {
        get
        {
            return instance;
        }

        set
        {
            instance = value;
        }
    }
    #endregion

    public void ShowNotifyPopup(string title, string content)
    {
        notifyPopup.Show(title, content);
    }

    public void ShowRevivePopup(float times, bool activeWatchAds, bool activeXP, int reviveXP)
    {
        revivePopup.Show(times, activeWatchAds, activeXP, reviveXP);
    }

    public void Awake()
    {
        instance = this;
    }
}

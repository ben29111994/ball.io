﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RevivePopup : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Button btnWatchAds;
    [SerializeField]
    private Button btnXP;
    [SerializeField]
    private Text txtXP;
    [SerializeField]
    private TextMeshProUGUI tmpContent;
    #endregion

    #region Normal paramters
    private float countdownTime;
    #endregion

    #region Encapsulate
    #endregion

    public void Show(float times, bool activeWatchAds, bool activeXP, int reviveXP)
    {
        countdownTime = times;

        btnWatchAds.gameObject.SetActive(activeWatchAds);
        btnXP.gameObject.SetActive(activeXP);

        txtXP.text = string.Format("{0} XP", reviveXP);

        gameObject.SetActive(true);

        StartCoroutine(C_CheckAcive());
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private IEnumerator C_CheckAcive()
    {
        while (countdownTime > 0)
        {
            countdownTime -= Time.deltaTime;

            tmpContent.text = string.Format("{0} seconds", (int)countdownTime + 1);

            yield return null;
        }

        Hide();

        GameManager.Instance.EndGame();
    }

    private void OnBtnWatchAdsClicked()
    {
        GameManager.Instance.ReviveAds();
        Hide();
    }

    private void OnBtnXPClicked()
    {
        GameManager.Instance.ReviveXP();
        Hide();
    }

    public void Awake()
    {
        btnWatchAds.onClick.AddListener(OnBtnWatchAdsClicked);
        btnXP.onClick.AddListener(OnBtnXPClicked);
    }

}

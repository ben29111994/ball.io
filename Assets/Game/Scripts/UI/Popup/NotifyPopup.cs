﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NotifyPopup : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private TextMeshProUGUI txtTitle;
    [SerializeField]
    private TextMeshProUGUI txtContent;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public void Show(string title, string content)
    {
        txtTitle.text = title;
        txtContent.text = content;

        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}

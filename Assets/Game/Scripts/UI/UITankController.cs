﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class UITankController : MonoSingleton<UITankController>
{
    #region Const parameters
    private const string MONEY_PER_MIN_FORMAT = "{0}<sprite=0 tint=0>/Min";
    private const string FISH_COUNTER_FORMAT = "<sprite=0 tint=0>x{0}";
    private const string MONEY_EARN_FORMAT = "<sprite=0 tint=0>{0}/{1}";
    #endregion

    #region Editor paramters
    [Header("UI references")]
    [SerializeField]
    private TextMeshProUGUI tmpMoneyPerMin;
    [SerializeField]
    private TextMeshProUGUI tmpFishCounter;
    [SerializeField]
    private TextMeshProUGUI tmpMoneyEarn;
    [SerializeField]
    private Slider sliderStatus;
    [SerializeField]
    private Button btnFeed;
    [SerializeField]
    private Button btnCollect;

    [Header("Notify panel")]
    [SerializeField]
    private GameObject notifyPanel;
    [SerializeField]
    private TMPro.TextMeshProUGUI tmpHeader;
    [SerializeField]
    private TextMeshProUGUI tmpContent;
    [SerializeField]
    private Button btnConfirm;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        btnFeed.onClick.AddListener(OnBtnFeedClicked);
        btnCollect.onClick.AddListener(OnBtnCollectClicked);
        btnConfirm.onClick.AddListener(OnBtnConfirmClick);
    }

    private void OnBtnConfirmClick()
    {
        notifyPanel.SetActive(false);
        UIManager.Instance.OpenShop();
    }

    public void OnEnable()
    {
        Tank.Instance.ChangeState(TankState.TankUI);
    }

    public void OnDisable()
    {
        //Tank.Instance.ChangeState(TankState.IDLE);
    }

    public void LateUpdate()
    {
        var foodLeft = Tank.Instance.GetFoodLeft();
        sliderStatus.value = foodLeft;

        tmpMoneyPerMin.text = string.Format(MONEY_PER_MIN_FORMAT, Tank.Instance.GetMoneyPerMin());
        tmpFishCounter.text = string.Format(FISH_COUNTER_FORMAT, Tank.Instance.GetFishCounter());
        tmpMoneyEarn.text = string.Format(MONEY_EARN_FORMAT, Tank.Instance.GetMoneyEarn(), 1000);
    }

    private void OnBtnFeedClicked()
    {
        if(DataManager.Instance.Food > 0)
        {
            Tank.Instance.FeedTheFish();
        }
        else
        {
            notifyPanel.SetActive(true);

            tmpHeader.text = "Warning";
            tmpContent.text = "No more food.";

            //PopupController.Instance.ShowNotifyPopup("Warning", "No more food.");
        }
    }

    private void OnBtnCollectClicked()
    {
        Tank.Instance.CollectMoney();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIShopController : MonoSingleton<UIShopController>
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<ShopItem> listShopItems;
    [SerializeField]
    private List<ShopButton> listShopButtons;
    #endregion

    #region Normal paramters
    private bool isInitialized = false;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        for (int i = 0; i < listShopItems.Count; i++)
        {
            listShopItems[i].Initialize();
        }
    }

    public void OnEnable()
    {
        RefreshUI();
    }

    public void Refresh()
    {
        for (int i = 0; i < listShopItems.Count; i++)
        {
            if (!listShopItems[i].IsInitialized)
                listShopItems[i].Initialize();
        }
    }

    public void AddShopItem(ShopItem item)
    {
        listShopItems.Add(item);
    }

    public void AddShopButton(ShopButton button)
    {
        listShopButtons.Add(button);
    }

    public void RefreshUI()
    {
        for (int i = 0; i < listShopButtons.Count; i++)
        {
            listShopButtons[i].RefreshUI();
        }
    }

}

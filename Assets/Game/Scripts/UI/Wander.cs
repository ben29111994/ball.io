﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection
{
    Up,
    Down,
    Straight,
}


public class Wander : MonoBehaviour
{
    public float speed;
    public float range;

    public float moveTime;
    public AnimationCurve moveCurve;
    public float moveCurveTime;
    public MoveDirection moveDirection;

    public Vector3 startPosition;
    public Vector3 prevPosition;
    public Vector3 nextPosition;

    public void Awake()
    {
        startPosition = transform.position;
        prevPosition = transform.position;
        nextPosition = transform.position;
    }

    public void WanderUpdate(float deltaTime)
    {
        if (moveTime <= 0)
        {
            moveTime = 1;
            moveCurveTime = 0;
            moveDirection = (MoveDirection)UnityEngine.Random.Range(0, 3);
            speed = UnityEngine.Random.Range(0, 2) > 0 ? speed : -speed;

            prevPosition = transform.position;
            nextPosition = transform.position;
        }
        else
        {
            moveTime -= deltaTime;

            if (moveDirection == MoveDirection.Straight)
            {
                nextPosition.x += deltaTime * speed;
            }
            else if (moveDirection == MoveDirection.Up)
            {
                moveCurveTime += deltaTime;
                nextPosition.x += speed * deltaTime;
                nextPosition.y = prevPosition.y + moveCurve.Evaluate(moveCurveTime) * speed;
            }
            else if (moveDirection == MoveDirection.Down)
            {
                moveCurveTime += deltaTime;
                nextPosition.x += speed * deltaTime;
                nextPosition.y = prevPosition.y - moveCurve.Evaluate(moveCurveTime) * speed;
            }

            // make sure object move in range
            if ((nextPosition.x > startPosition.x + range || nextPosition.x < startPosition.x - range) ||
                (nextPosition.y > startPosition.y + range || nextPosition.y < startPosition.y - range))
            {
                speed = -speed;
                moveCurveTime = 0;

                prevPosition = transform.position;
                nextPosition = transform.position;

                return;
            }

            transform.position = nextPosition;
        }
    }

    public void Update()
    {
        WanderUpdate(Time.deltaTime);
    }
}

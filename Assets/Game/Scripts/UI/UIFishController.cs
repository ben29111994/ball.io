﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFishController : MonoSingleton<UIFishController>
{
    #region Const parameters

    #endregion

    #region Editor paramters

    [SerializeField] private Text txtFishName;
    [SerializeField] private Text txtLevel;
    [SerializeField] private Slider sliderLevelProgress;
    [SerializeField] private Button btnSelect;
    [SerializeField] private Button btnSelected;
    [SerializeField] private Button btnLocked;

    [SerializeField] private Button btnSpeedUp;
    [SerializeField] private Button btnHealthUp;
    [SerializeField] private Button btnExpUp;

    [SerializeField] private Color colorSkillButtonDisable;
    [SerializeField] private Color colorSkillButtonEnable;

    #endregion

    #region Normal paramters

    private Character currentCharacter;

    #endregion

    #region Encapsulate

    #endregion

    public override void Initialize()
    {
    }

    public void OnBtnSelectClicked()
    {
        var selectedCharacter = CharacterManager.Instance.SelectedCharacter;
        selectedCharacter.IsSelected = false;

        currentCharacter.IsSelected = true;
        CharacterManager.Instance.SelectedCharacter = currentCharacter;

        OnScrollCharacter(currentCharacter);
    }

    public void OnBtnSelectedClicked()
    {
    }

    public void OnBtnLockedClicked()
    {
    }

    public void OnScrollCharacter(Character character)
    {
        currentCharacter = character;

        txtFishName.text = character.FishName;
        txtLevel.text = "Level " + character.Level;

        btnSelect.gameObject.SetActive(false);
        btnSelected.gameObject.SetActive(false);
        btnLocked.gameObject.SetActive(false);

        switch (character.Abilities)
        {
            case CharacterAbilities.SpeedBoost:
                btnSpeedUp.GetComponent<Image>().color = colorSkillButtonEnable;
                break;
            case CharacterAbilities.HealthBoost:
                btnHealthUp.GetComponent<Image>().color = colorSkillButtonEnable;
                break;
            case CharacterAbilities.EggEXPBoost:
                btnExpUp.GetComponent<Image>().color = colorSkillButtonEnable;
                break;
            case CharacterAbilities.None:
                btnSpeedUp.GetComponent<Image>().color = colorSkillButtonDisable;
                btnHealthUp.GetComponent<Image>().color = colorSkillButtonDisable;
                btnExpUp.GetComponent<Image>().color = colorSkillButtonDisable;
                break;
        }

        if (character.IsBuy)
        {
            if (character.IsSelected)
            {
                btnSelected.gameObject.SetActive(true);
            }
            else
            {
                btnSelect.gameObject.SetActive(true);
            }
        }
        else
        {
            btnLocked.gameObject.SetActive(true);
        }

        sliderLevelProgress.value = (float) (character.CurExp / character.TotalExp);
    }

    public void OnEnable()
    {
        OnScrollCharacter(CharacterManager.Instance.SelectedCharacter);

        if (EnhanceScrollView.Instance)
        {
            EnhanceScrollView.Instance.SetCenter(CharacterManager.Instance.SelectedCharacter.Id);
        }
    }
}
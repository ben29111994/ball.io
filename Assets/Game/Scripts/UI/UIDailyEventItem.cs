﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDailyEventItem : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Information")]
    [SerializeField]
    private int eventID;
    [SerializeField]
    private string eventKey;
    [SerializeField]
    private bool isCompleted;

    [Header("UI references")]
    [SerializeField]
    private TMPro.TextMeshProUGUI tmpTitle;
    [SerializeField]
    private TMPro.TextMeshProUGUI tmpDescription;
    [SerializeField]
    private UnityEngine.UI.Button btnOnGoing;
    [SerializeField]
    private UnityEngine.UI.Button btnCollect;
    [SerializeField]
    private UnityEngine.UI.Button btnCompleted;
    [SerializeField]
    private UnityEngine.UI.Image imgUnComplete;
    [SerializeField]
    private UnityEngine.UI.Image imgComplete;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    public int EventID
    {
        get
        {
            return eventID;
        }

        set
        {
            eventID = value;
        }
    }

    public string EventKey
    {
        get
        {
            return eventKey;
        }

        set
        {
            eventKey = value;
        }
    }

    public bool IsCompleted
    {
        get
        {
            return isCompleted;
        }

        set
        {
            isCompleted = value;
        }
    }
    #endregion

    public void Initialize()
    {
        btnOnGoing.gameObject.SetActive(false);
        btnCollect.gameObject.SetActive(false);
        btnCompleted.gameObject.SetActive(false);

        imgUnComplete.gameObject.SetActive(false);
        imgComplete.gameObject.SetActive(false);

        btnOnGoing.onClick.AddListener(OnBtnOnGoingClicked);
        btnCollect.onClick.AddListener(OnBtnCollectClicked);
        btnCompleted.onClick.AddListener(OnBtnCompletedClicked);

        Refresh();
    }

    public void Release()
    {
    }

    public void Refresh()
    {
        var dailyEvent = Mission.MissionManager.Instance.GetMission(eventKey);
        tmpTitle.text = dailyEvent.Title;
        tmpDescription.text = dailyEvent.Description;

        if (dailyEvent.IsCompleted)
        {
            imgComplete.gameObject.SetActive(true);

            if (dailyEvent.IsCollected)
            {
                btnCompleted.gameObject.SetActive(true);
            }
            else
            {
                btnCollect.gameObject.SetActive(true);
            }
        }
        else
        {
            imgUnComplete.gameObject.SetActive(true);

            btnOnGoing.gameObject.SetActive(true);
        }
    }

    public void UpdateStep(float deltaTime)
    {
    }

    private void OnBtnOnGoingClicked()
    {
        Refresh();
    }

    private void OnBtnCollectClicked()
    {
        var dailyEvent = Mission.MissionManager.Instance.GetMission(eventKey);
        dailyEvent.CollectReward();

        Refresh();
    }

    private void OnBtnCompletedClicked()
    {
        Refresh();
    }

}

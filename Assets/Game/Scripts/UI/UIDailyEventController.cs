﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDailyEventController : MonoSingleton<UIDailyEventController>
{

    #region Const parameters
    private const string LIST_EVENT_KEY = "ListEvent";
    private const string LIST_COMPLETED_EVENT_KEY = "ListCompletedEvent";
    private const string LIST_COLLECTED_EVENT_KEY = "ListCollectedEvent";
    #endregion

    #region Editor paramters
    [SerializeField]
    private int activeEventCount = 3;

    [SerializeField]
    private UIDailyEventItem dailyEventItem;
    [SerializeField]
    private Transform dailyEventItemHolder;
    [SerializeField]
    private List<UIDailyEventItem> listDailyEventItem;
    #endregion

    #region Normal paramters
    private Dictionary<string, DailyEvent.DailyEvent> dailyEvents;
    private List<Mission.Mission> listMission;

    private List<string> listEvent;
    private List<string> listCompletedEvent;
    private List<string> listCollectedEvent;

    private Mission.MissionManager missionManager;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        //dailyEvents = DailyEventHelper.Instance.DailyEventManager.DailyEvents;
        missionManager = Mission.MissionManager.Instance;
        missionManager.OnMissionCompletedCallback += MissionManager_OnMissionCompletedCallback;
        missionManager.OnMissionCollectedCallback += MissionManager_OnMissionCollectedCallback;
        listMission = missionManager.GetAllMissions();

        listEvent = new List<string>();
        listCompletedEvent = new List<string>();
        listCollectedEvent = new List<string>();

        Load();

        InitializeEvent();
    }

    private void MissionManager_OnMissionCollectedCallback(Mission.Mission obj)
    {
        CollectedEvent(obj.MissionID);
    }

    private void MissionManager_OnMissionCompletedCallback(Mission.Mission obj)
    {
        CompletedEvent(obj.MissionID);
    }

    public void Load()
    {
        // load last session data
        var data = DataManager.Instance.GetString(LIST_EVENT_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if (!string.IsNullOrEmpty(array[i]))
                    listEvent.Add(array[i]);
            }
        }

        data = DataManager.Instance.GetString(LIST_COMPLETED_EVENT_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if (!string.IsNullOrEmpty(array[i]))
                    listCompletedEvent.Add(array[i]);
            }
        }

        data = DataManager.Instance.GetString(LIST_COLLECTED_EVENT_KEY, string.Empty);
        if (!string.IsNullOrEmpty(data))
        {
            var array = data.Split(';');
            for (int i = 0; i < array.Length; i++)
            {
                if (!string.IsNullOrEmpty(array[i]))
                    listCollectedEvent.Add(array[i]);
            }
        }

        //new day
        //Debug.Log("[DailyEvent] RefreshData : " + DataManager.Instance.RefreshData);
        if (DataManager.Instance.RefreshData)
        {
            listEvent.Clear();
            listCompletedEvent.Clear();
            listCollectedEvent.Clear();
        }

    }

    public void Save()
    {
        string data = string.Empty;
        for (int i = 0; i < listEvent.Count; i++)
        {
            data += listEvent[i] + ";";
        }
        DataManager.Instance.SetString(LIST_EVENT_KEY, data);

        data = string.Empty;
        for (int i = 0; i < listCompletedEvent.Count; i++)
        {
            data += listCompletedEvent[i] + ";";
        }
        DataManager.Instance.SetString(LIST_COMPLETED_EVENT_KEY, data);
    }

    public void CompletedEvent(int eventID)
    {
        Debug.LogError("NOT IMPLEMENT YET");
    }

    public void CompletedEvent(string eventKey)
    {
        if (!listCompletedEvent.Contains(eventKey))
        {
            listCompletedEvent.Add(eventKey);

            // save data
            var data = string.Empty;
            for (int i = 0; i < listCompletedEvent.Count; i++)
            {
                data += listCompletedEvent[i] + ";";
            }

            PlayerPrefs.SetString(LIST_COMPLETED_EVENT_KEY, data);
            PlayerPrefs.Save();
        }
    }

    public void CollectedEvent(string eventKey)
    {
        if (!listCollectedEvent.Contains(eventKey))
        {
            listCollectedEvent.Add(eventKey);
            // get reward prize 

            // hard code prize
            DataManager.Instance.Money += 1000;
			DataManager.Instance.Save();

            // save data
            var data = string.Empty;
            for (int i = 0; i < listCollectedEvent.Count; i++)
            {
                data += listCollectedEvent[i] + ";";
            }

            PlayerPrefs.SetString(LIST_COLLECTED_EVENT_KEY, data);
            PlayerPrefs.Save();
        }
    }

    public void InitializeEvent()
    {
        if (listEvent.Count == 0)
        {
            var list = missionManager.GetAllMissions();

            var index = (int)(UnityEngine.Random.value * (list.Count - 1));
            //Debug.Log("[DailyEvent] Index : " + index);
            for (int i = 0; i < 3; i++)
            {
                listEvent.Add(list[index].MissionID);

                index++;
                if (index >= list.Count)
                    index = 0;
            }
        }

        for (int i = 0; i < listEvent.Count; i++)
        {
            var eventKey = listEvent[i];
            var mission = missionManager.GetMission(eventKey);

            if (mission != null)
            {
                mission.IsCompleted = listCompletedEvent.Contains(eventKey);
                mission.IsCollected = listCollectedEvent.Contains(eventKey);

                var item = Instantiate(dailyEventItem, dailyEventItemHolder);
                item.gameObject.SetActive(true);

                item.EventID = 0;
                item.EventKey = eventKey;
                item.IsCompleted = listCompletedEvent.Contains(eventKey);

                item.Initialize();

                listDailyEventItem.Add(item);
            }
        }

        Save();
    }

    public void Show()
    {
        gameObject.SetActive(true);

        for (int i = 0; i < listDailyEventItem.Count; i++)
        {
            listDailyEventItem[i].Refresh();
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIHatchController : MonoSingleton<UIHatchController>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Text txtNumber;
    [SerializeField]
    private Slider sliderNumber;
    [SerializeField]
    private Button btnClaimEgg;
    [SerializeField]
    private Button btnZoom;
    [SerializeField]
    private Button btnBackHatch;

    [Header("Zoom information")]
    [SerializeField]
    private Text txtEggName;
    [SerializeField]
    private TextMeshProUGUI txtTime;
    [SerializeField]
    private Slider sliderTime;
    [SerializeField]
    private Button btnHatch;
    [SerializeField]
    private Button btnWatchAds;
    [SerializeField]
    private Button btnFill;
    #endregion

    #region Normal paramters
    private bool isZoom;
    private Podium lockedPodium;
    private Egg lockedEgg;

    private double neededSecond;
    private double neededXP;

    private Tank tank;
    private DataManager data;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        AdsManager.Instance.OnRewardVideoCallback += OnRewardVideoCallback;

        btnWatchAds.onClick.AddListener(OnBtnWatchAdsClicked);
        btnFill.onClick.AddListener(OnBtnFillClicked);

        tank = Tank.Instance;
        data = DataManager.Instance;
    }

    public void OnEnable()
    {
        var data = DataManager.Instance;
        var percent = data.EggEXP / data.TotalEggEXP;

        sliderNumber.gameObject.SetActive(false);
        // btnClaimEgg.gameObject.SetActive(false);
        btnClaimEgg.interactable = false;

        if (percent == 1.0f && EggManager.Instance.CanClaimEgg() && Tank.Instance.HasEmptyPodium())
        {
            // btnClaimEgg.gameObject.SetActive(true);
            btnClaimEgg.interactable = true;
        }
        else
        {
            txtNumber.text = string.Format("{0} / {1}", data.EggEXP, data.TotalEggEXP);
            sliderNumber.value = (float)(data.EggEXP * 1.0f / data.TotalEggEXP);
            sliderNumber.gameObject.SetActive(true);
        }

        Tank.Instance.ChangeState(TankState.HatchUI);

        isZoom = false;
    }

    public void OnDisable()
    {
        //if (tank != null)
        //    tank.ChangeState(TankState.IDLE);

        isZoom = false;
    }

    public void OnBtnClaimEggClicked()
    {
        Tank.Instance.SpawnEgg(EggManager.Instance.ClaimNextEgg());
        var data = DataManager.Instance;
        txtNumber.text = string.Format("{0} / {1}", data.EggEXP, data.TotalEggEXP);
        sliderNumber.value = (float)(data.EggEXP / data.TotalEggEXP);
        sliderNumber.gameObject.SetActive(true);
        // btnClaimEgg.gameObject.SetActive(false);
        btnClaimEgg.interactable = false;
    }

    public void OnBtnZoomClicked()
    {
        btnZoom.onClick.Invoke();
    }

    public void OnBtnBackHatchClicked()
    {
        isZoom = false;

        lockedPodium = null;
        lockedEgg = null;

        CameraController.Instance.ZoomOutPosition(Tank.Instance.GetPodiumContainerPosition());
    }

    public void OnBtnHatchClicked()
    {
        if(lockedEgg != null)
        {
            var characterID = CharacterManager.Instance.UnlockCharacter(lockedEgg.Type);
            UIUnlockFishController.Instance.Show(characterID);
            tank.RemoveEgg(lockedEgg);

            btnBackHatch.onClick.Invoke();

            CharacterManager.Instance.Save();
            tank.Save();
        }
    }

    public void OnBtnWatchAdsClicked()
    {
        if(AdsManager.Instance.IsRewardVideoAdReady())
        {
           AdsManager.Instance.ShowRewardVideoAd(RewardVideoType.Hatch);
        }
    }

    public void OnBtnFillClicked()
    {
        int fillAmount = Mathf.Min((int)neededXP, data.PlayerXP);
        float skipTime = fillAmount * XPSystem.XP2Second;

        Debug.Log(string.Format("fillAmount: {0}, skipTime: {1}", fillAmount, skipTime));

        lockedEgg.AddSkipTime(skipTime);

        data.PlayerXP -= fillAmount;

        Update();

        tank.Save();
        data.Save();
    }

    public void Update()
    {
        if (!isZoom)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D[] hits = Physics2D.RaycastAll(ray.origin, ray.direction);
                for (int i = 0; i < hits.Length; i++)
                {
                    var hit = hits[i];
                    if (hit.collider != null && hit.collider.gameObject.CompareTag("Podium"))
                    {
                        isZoom = true;
                        OnBtnZoomClicked();
                        lockedPodium = hit.collider.transform.parent.gameObject.GetComponent<Podium>();
                        lockedEgg = tank.GetEgg(lockedPodium.EggID);

                        txtEggName.text = lockedEgg.Type + " Egg";

                        CameraController.Instance.ZoomInPosition(lockedPodium.transform.position);
                    }
                }
            }
        }
        else
        {
            var timeLeft = lockedEgg.GetTimeSpanLeft();
            if (timeLeft.TotalSeconds > 0.0f)
            {
                neededSecond = (int)timeLeft.TotalSeconds;
                neededXP = neededSecond * XPSystem.Second2XP;

                txtTime.text = string.Concat(new string[]
                {
                    timeLeft.Hours.ToString("00"),
                    " : ",
                    timeLeft.Minutes.ToString("00"),
                    " : ",
                    timeLeft.Seconds.ToString("00")
                });

                btnHatch.gameObject.SetActive(false);

                btnFill.gameObject.SetActive(true);
                btnWatchAds.gameObject.SetActive(AdsManager.Instance.IsRewardVideoAdReady());
            }
            else
            {
                txtTime.text = "00 : 00 : 00";

                btnHatch.gameObject.SetActive(true);

                btnFill.gameObject.SetActive(false);
                btnWatchAds.gameObject.SetActive(false);
            }

            sliderTime.value = 1 - (float)(timeLeft.TotalSeconds / lockedEgg.HatchTimes);
        }
    }

    private void OnRewardVideoCallback(RewardVideoType type, RewardVideoResult result)
    {
        // still accept due to server side issues
        if ((result == RewardVideoResult.Finished || result == RewardVideoResult.Skipped) && type == RewardVideoType.Hatch)
        {
            lockedEgg.AddSkipTime(GameManager.Instance.SkipSeconds);
            Update();
            // add sound here
            //SoundManager.Instance.PlaySFX(SFXType.CollectCoin);
        }
    }

}

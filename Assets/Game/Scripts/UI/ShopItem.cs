﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Information")]
    [SerializeField]
    private string title;
    [SerializeField]
    private int price = 0;
    [SerializeField]
    private bool isIAPItem = false;
    [SerializeField]
    private string iapItemID = string.Empty;

    [SerializeField]
    private bool isHelpActive = false;
    [SerializeField]
    private string helpTiltle;
    [SerializeField]
    private string helpContent;

    [Header("UI references")]
    [SerializeField]
    private TextMeshProUGUI tmpTitle;
    [SerializeField]
    private TextMeshProUGUI tmpPrice;
    [SerializeField]
    private Image imgBackground;
    [SerializeField]
    private Image imgItem;
    [SerializeField]
    private Image imgMoney;
    [SerializeField]
    private Button btnBuy;
    [SerializeField]
    private Button btnHelp;
    #endregion

    #region Normal paramters
    private bool isInitialized = false;
    #endregion

    #region Encapsulate
    public bool IsInitialized
    {
        get
        {
            return isInitialized;
        }

        set
        {
            isInitialized = value;
        }
    }
    #endregion

    public void Initialize()
    {
        if (isIAPItem)
        {
            imgMoney.gameObject.SetActive(true);

            var product = UnityIAPManager.Instance.GetProductID(iapItemID);

            if (product != null)
            {
                isInitialized = true;

                tmpTitle.text = product.metadata.localizedTitle;
                tmpPrice.text = product.metadata.localizedPriceString;
            }

            imgMoney.gameObject.SetActive(false);

            btnHelp.gameObject.SetActive(isHelpActive);
            btnBuy.gameObject.SetActive(isInitialized);
        }
        else
        {
            isInitialized = true;

            tmpTitle.text = title;
            tmpPrice.text = price.ToString();

            imgMoney.gameObject.SetActive(true);

            btnHelp.gameObject.SetActive(isHelpActive);
            btnBuy.gameObject.SetActive(isInitialized);
        }
    }

    public void BuyItem()
    {

    }

    public void Help()
    {
        PopupController.Instance.ShowNotifyPopup(helpTiltle, helpContent);
    }

    public void Awake()
    {
        UIShopController.Instance.AddShopItem(this);
        Initialize();
    }

    public void BuyEgg(int id)
    {
        BuyEgg((EggType)id);
    }

    public void BuyEgg(EggType eggType)
    {
        if (DataManager.Instance.Money > price)
        {
            if (CharacterManager.Instance.HasLockedCharacter(eggType))
            {
                DataManager.Instance.Money -= price;
                var characterID = CharacterManager.Instance.UnlockCharacter(eggType);
                UIUnlockFishController.Instance.Show(characterID);
            }
            else
            {
                // show panel no more fish
                Debug.Log("ShopItem: No more fish for egg: " + eggType);
                PopupController.Instance.ShowNotifyPopup("Shop", "No more fish for this egg");
            }
        }
        else
        {
            Debug.Log("ShopItem: Not enoug money.");
            PopupController.Instance.ShowNotifyPopup("Shop", "Not enoug money");

        }


    }

}

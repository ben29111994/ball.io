﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUnlockFishController : MonoSingleton<UIUnlockFishController>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Text txtFishName;
    [SerializeField]
    private Text txtEggType;
    [SerializeField]
    private Button btnOkay;
    [SerializeField]
    private Button btnUseNow;

    [SerializeField]
    private List<GameObject> listCharacters;
    #endregion

    #region Normal paramters
    private int unlockCharacterID;

    private DragonBones.UnityArmatureComponent armatureComponent;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        btnOkay.onClick.AddListener(OnBtnOkayClicked);
        btnUseNow.onClick.AddListener(OnBtnUseNowClicked);

        for (int i = 0; i < listCharacters.Count; i++)
        {
            listCharacters[i].SetActive(false);
        }

        Hide();
    }

    public void Show(int characterID)
    {
        unlockCharacterID = characterID;
        //listCharacters[unlockCharacterID].SetActive(true);
        var character = CharacterManager.Instance.GetCharacterByID(unlockCharacterID);
        txtFishName.text = character.FishName;
        txtEggType.text = character.EggType.ToString();
        armatureComponent = DragonBonesManager.Instance.GeneratePlayer((DragonBonesType)character.Id, character.Level);
        armatureComponent.transform.SetParent(transform, false);
        armatureComponent.transform.localScale *= 1000;

        armatureComponent.sortingOrder = 9999;
        armatureComponent.animation.Stop();

        gameObject.SetActive(true);
    }

    public void Hide()
    {
        //listCharacters[unlockCharacterID].SetActive(false);
        if (armatureComponent != null)
            DestroyObject(armatureComponent.gameObject);

        gameObject.SetActive(false);
    }

    public void OnBtnOkayClicked()
    {
        Hide();
    }

    public void OnBtnUseNowClicked()
    {

    }
}

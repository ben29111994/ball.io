﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

[System.Serializable]
public class FlockingRuleConfig
{
    public int minDistance;
    public int maxDistance;
    public int scale;
}

public class FlockingController : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private float speed;
    [SerializeField]
    private FlockingRuleConfig alignmentConfig;
    [SerializeField]
    private FlockingRuleConfig cohesionConfig;
    [SerializeField]
    private FlockingRuleConfig seperationConfig;
    #endregion

    #region Normal paramters
    /// <summary>
    /// a list of boids is used to update the velocity of gameObjects that are subscribed in the list using flocking.
    /// </summary>
    private List<Boid> listBoids;

    /// <summary>
    /// The three elements that determine the velocity of boids.
    /// </summary>
    private Alignment alignment;
    private Cohesion cohesion;
    private Separation separation;
    #endregion

    #region Encapsulate
    #endregion

    public void Initialize()
    {
        listBoids = new List<Boid>();
        alignment = new Alignment();
        cohesion = new Cohesion();
        separation = new Separation();

        SetFlockingRuleValues();
    }

    public void Release()
    {
    }

    public void Refresh()
    {
    }

    public void UpdateStep(float deltaTime)
    {
        SetFlockingRuleValues();

        new Thread(() =>
        {
            //run through all boids.
            for (int i = listBoids.Count - 1; i >= 0; --i)
            {

                Boid b = listBoids[i];
                //get the boids current velocity.
                Vector3 velocity = b.velocity;

                //add the influences of neighboring boids to the velocity.
                velocity += alignment.getResult(listBoids, i);
                velocity += cohesion.getResult(listBoids, i);
                velocity += separation.getResult(listBoids, i);

                //normalize the velocity and make sure that the boids new velocity is updated.
                velocity.Normalize();
                b.velocity = velocity;

                b.lookat = b.position + velocity;
            }
        }).Start();

        for (int i = listBoids.Count - 1; i >= 0; --i)
        {
            //update the boids position in the mainthread.
            listBoids[i].transform.localPosition += listBoids[i].velocity * deltaTime * speed;   
            //listBoids[i].transform.localRotation = Quaternion.FromToRotation(Vector3.right, listBoids[i].velocity);
        }
    }

    /// <summary>
    /// update the flocking rule values from the uiManager.
    /// </summary>
    public void SetFlockingRuleValues()
    {
        alignment.minDist = alignmentConfig.minDistance;
        alignment.maxDist = alignmentConfig.maxDistance;
        alignment.scalar = alignmentConfig.scale;

        cohesion.minDist = cohesionConfig.minDistance;
        cohesion.maxDist = cohesionConfig.maxDistance;
        cohesion.scalar = cohesionConfig.scale;

        separation.minDist = seperationConfig.minDistance;
        separation.maxDist = seperationConfig.maxDistance;
        separation.scalar = seperationConfig.scale;
    }

    public void AddBoid(Boid boid)
    {
        listBoids.Add(boid);
    }
}

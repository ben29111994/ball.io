﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitManager : MonoSingleton<KitManager>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<Kit> originalKits;

    [SerializeField]
    private Vector2 kitSize;
    [SerializeField]
    private Kit originalKit;
    #endregion

    #region Normal paramters
    private Dictionary<int, Dictionary<int, Kit>> kitLocations;
    private Dictionary<int, List<Kit>> kitPoolByLevel;
    private Dictionary<int, List<Kit>> kitPrefabsByLevel;

    private Dictionary<int, List<Kit>> inactiveKitsByLevel;
    private List<Kit> activeKits = new List<Kit>();
    private Location curLocation = new Location(0, 0);

    private int spawnKitLevel = 0;
    private int spawnKitMaximumLevel = 0;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        kitLocations = new Dictionary<int, Dictionary<int, Kit>>();
        //kitPoolByLevel = new Dictionary<int, List<Kit>>();
        //kitPrefabsByLevel = new Dictionary<int, List<Kit>>();

        //for (int i = 0; i < originalKits.Count; i++)
        //{
        //    var kit = originalKits[i];

        //    if (!kitPrefabsByLevel.ContainsKey(kit.Level))
        //    {
        //        kitPrefabsByLevel.Add(kit.Level, new List<Kit>());
        //    }
        //    kitPrefabsByLevel[kit.Level].Add(kit);

        //    if (!kitPoolByLevel.ContainsKey(kit.Level))
        //    {
        //        kitPoolByLevel.Add(kit.Level, new List<Kit>());
        //    }

        //    for (int j = 0; j < 3; j++)
        //    {
        //        var newKit = Instantiate(kit, transform);
        //        newKit.Initialize();
        //        newKit.gameObject.SetActive(false);

        //        kitPoolByLevel[kit.Level].Add(newKit);
        //    }

        //    if (kit.Level > spawnKitMaximumLevel)
        //    {
        //        spawnKitMaximumLevel = kit.Level;
        //    }
        //}
        inactiveKitsByLevel = new Dictionary<int, List<Kit>>();
    }

    public void Release()
    {
        curLocation.x = 0;
        curLocation.y = 0;
        spawnKitLevel = 0;

        foreach (int key in this.kitLocations.Keys)
        {
            foreach (Kit kit in this.kitLocations[key].Values)
            {
                kit.DeactiveAllSpawners();
                Recycle(kit);
            }
            this.kitLocations[key].Clear();
        }
    }

    public void Refresh()
    {
        curLocation.x = 0;
        curLocation.y = 0;
        spawnKitLevel = 0;
        kitSize = new Vector2(CameraController.Instance.TargetOrthographicSize, CameraController.Instance.TargetOrthographicSize);

        foreach (int key in this.kitLocations.Keys)
        {
            foreach (Kit kit in this.kitLocations[key].Values)
            {
                kit.DeactiveAllSpawners();
                Recycle(kit);
            }
            this.kitLocations[key].Clear();
        }

        kitLocations.Clear();
        kitLocations[curLocation.x] = new Dictionary<int, Kit>();
        kitLocations[curLocation.x][curLocation.y] = Generate(spawnKitLevel);
        kitLocations[curLocation.x][curLocation.y].SetLocationRaw(curLocation, Player.Instance.transform.position);
    }

    public void UpdateStep(float deltaTime)
    {
        for (int i = 0; i < activeKits.Count; i++)
        {
            var kit = activeKits[i];
            kit.UpdateStep(deltaTime);

            if (CameraController.Instance.CheckContainsInVirtualCamera(kit.transform.position))
            {
                GenerateSurrounding(kit);
            }
        }
    }

    public void EnterKit(Kit kit)
    {
        curLocation = kit.Location;
        GenerateSurroundingKits(kit);
    }

    public void ExitKit(Kit kit)
    {
        //RecycleKit(kit);
    }

    public void GenerateSurroundingKits(Kit kit)
    {
        // x = kit.Location.x;
        // y = kit.Location.y

        // [(x - 1), (y + 1)]   - [(x), (y + 1)]    - [(x + 1), (y + 1)]
        // [(x - 1), (y)]       - [(x), (y)]        - [(x + 1), (y)]
        // [(x - 1), (y - 1)]   - [(x), (y - 1)]    - [(x + 1), (y - 1)]

        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                Location location = kit.Location;
                location.x += i;
                location.y += j;

                if (!kitLocations.ContainsKey(location.x))
                {
                    kitLocations[location.x] = new Dictionary<int, Kit>();
                }
                if (!kitLocations[location.x].ContainsKey(location.y))
                {
                    kitLocations[location.x][location.y] = GenerateKit(spawnKitLevel);
                    kitLocations[location.x][location.y].SetLocationWithOffset(location, kit.Location, kit.transform.position + (Vector3)kit.LocationRect.center);
                }
            }
        }
    }

    public Kit GenerateKit(int level)
    {
        var pool = kitPoolByLevel[level];
        var prefabs = kitPrefabsByLevel[level];

        Kit kit;

        if (pool.Count > 0)
        {
            kit = pool[UnityEngine.Random.Range(0, pool.Count)];
            pool.Remove(kit);
        }
        else
        {
            kit = Instantiate(prefabs[UnityEngine.Random.Range(0, prefabs.Count)], transform);
            kit.Initialize();
        }

        kit.gameObject.SetActive(true);
        kit.Refresh();

        activeKits.Add(kit);

        return kit;
    }

    public void RecycleKit(Kit kit)
    {
        kit.gameObject.SetActive(false);
        kit.Release();

        var pool = kitPoolByLevel[kit.Level];
        pool.Add(kit);

        if (activeKits.Contains(kit))
        {
            activeKits.Remove(kit);
        }
    }

    public void SpawnLeveledUpKits()
    {
        spawnKitLevel++;
        kitSize = new Vector2(CameraController.Instance.TargetOrthographicSize, CameraController.Instance.TargetOrthographicSize);
        //if (spawnKitLevel > spawnKitMaximumLevel)
        //{
        //    spawnKitLevel = spawnKitMaximumLevel;
        //}

        foreach (int key in this.kitLocations.Keys)
        {
            foreach (Kit kit in this.kitLocations[key].Values)
            {
                kit.DeactiveAllSpawners();
                Recycle(kit);
            }
            this.kitLocations[key].Clear();
        }

        kitLocations.Clear();
        kitLocations[curLocation.x] = new Dictionary<int, Kit>();
        kitLocations[curLocation.x][curLocation.y] = Generate(spawnKitLevel);
        kitLocations[curLocation.x][curLocation.y].SetLocationRaw(curLocation, Player.Instance.transform.position);
    }

    public Kit Generate(int level)
    {
        if (!inactiveKitsByLevel.ContainsKey(level))
        {
            inactiveKitsByLevel.Add(level, new List<Kit>());
            return Generate(level);
        }

        Kit kit;

        var kits = inactiveKitsByLevel[level];
        if (kits.Count > 0)
        {
            kit = kits[0];
            kits.Remove(kit);
        }
        else
        {
            kit = Instantiate<Kit>(originalKit, transform);
            kit.Initialize();

            var locationRect = new Rect(Vector2.zero, kitSize * 2);
            locationRect.center = Vector2.zero;

            kit.LocationRect = locationRect;
            kit.Level = level;
        }

        kit.Refresh();
        activeKits.Add(kit);

        kit.gameObject.SetActive(true);

        return kit;
    }

    public void Recycle(Kit kit)
    {
        var kits = inactiveKitsByLevel[kit.Level];
        kits.Add(kit);

        kit.Release();
        activeKits.Remove(kit);

        kit.gameObject.SetActive(false);
    }

    public void GenerateSurrounding(Kit kit)
    {
        // x = kit.Location.x;
        // y = kit.Location.y

        // [(x - 1), (y + 1)]   - [(x), (y + 1)]    - [(x + 1), (y + 1)]
        // [(x - 1), (y)]       - [(x), (y)]        - [(x + 1), (y)]
        // [(x - 1), (y - 1)]   - [(x), (y - 1)]    - [(x + 1), (y - 1)]

        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                Location location = kit.Location;
                location.x += i;
                location.y += j;

                if (!kitLocations.ContainsKey(location.x))
                {
                    kitLocations[location.x] = new Dictionary<int, Kit>();
                }
                if (!kitLocations[location.x].ContainsKey(location.y))
                {
                    kitLocations[location.x][location.y] = Generate(spawnKitLevel);
                    kitLocations[location.x][location.y].SetLocationWithOffset(location, kit.Location, kit.transform.position + (Vector3)kit.LocationRect.center);
                }
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    protected int level;
    [SerializeField]
    [Tooltip("Real swim range = Mathf.Pow(2, level) * swimRange")]
    [Range(1.0f, 4.0f)]
    private float swimRange;
    [SerializeField]
    [Tooltip("Real swim speed = Mathf.Pow(2, level) * swimSpeed * swimCurve")]
    [Range(1.0f, 4.0f)]
    private float swimSpeed;
    [SerializeField]
    private Vector3 swimVelocity;
    [SerializeField]
    private AnimationCurve swimCurve;
    [SerializeField]
    private float swimCurveValue;

    [SerializeField]
    private TrailRenderer trail;

    [SerializeField]
    private float bonusLifeTime;
    [SerializeField]
    private int bonusScore;

    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;

    [SerializeField]
    private float moveTime = 1.0f;
    [SerializeField]
    private AnimationCurve moveCurve;
    [SerializeField]
    private float moveCurveTime;
    [SerializeField]
    private MoveDirection moveDirection;
    #endregion

    #region Normal paramters
    private Vector3 startPosition;
    private Vector3 startEulerAngles;
    private Vector3 prevPosition;
    private Vector3 nextPosition;

    private float realSwimRange;
    private float realSwimSpeed;
    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    public Vector3 StartPosition
    {
        get
        {
            return startPosition;
        }

        set
        {
            startPosition = value;
        }
    }

    public Vector3 StartEulerAngles
    {
        get
        {
            return startEulerAngles;
        }

        set
        {
            startEulerAngles = value;
        }
    }

    public float BonusLifeTime
    {
        get
        {
            return bonusLifeTime;
        }

        set
        {
            bonusLifeTime = value;
        }
    }

    public int BonusScore
    {
        get
        {
            return bonusScore;
        }

        set
        {
            bonusScore = value;
        }
    }
    #endregion

    public void Initialize()
    {

    }

    public void Release()
    {
    }

    public void Refresh()
    {
        startPosition = Vector3.zero;
    }

    public void UpdateStep(float deltaTime)
    {
        if (!CameraController.Instance.CheckContainsInVirtualCamera(transform.position))
        {
            Recycle();
        }
        else
        {
            swimCurveValue = swimCurve.Evaluate(Mathf.Pow(2, Player.Instance.Level - level));
            //Wander(deltaTime);
            var newPosition = transform.position;
            newPosition += swimVelocity * deltaTime * (realSwimSpeed * swimCurveValue);

            //if ((newPosition.x > startPosition.x + swimRange) ||
            //    (newPosition.x < startPosition.x - swimRange) ||
            //    (newPosition.y > startPosition.y + swimRange) ||
            //    (newPosition.y < startPosition.y - swimRange))
            //{
            //    startEulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));

            //    var radianZ = startEulerAngles.z * Mathf.Deg2Rad;
            //    swimVelocity = new Vector3(Mathf.Cos(radianZ), Mathf.Sin(radianZ));

            //    transform.eulerAngles = startEulerAngles;
            //}
            //else
            {
                transform.position = newPosition;
            }

            var angleZ = transform.localEulerAngles.z;
            if (angleZ < 0)
                angleZ += 360;

            if (angleZ > 90 && angleZ < 270)
            {
                armatureComponent.armature.flipY = true;
            }
            else
            {
                armatureComponent.armature.flipY = false;
            }

            if(Player.Instance.Level > level + 1)
            {
                Recycle();
            }
        }
    }

    public void Recycle()
    {
        EnemyManager.Instance.Recycle(this);
    }

    public void ApplyChanges()
    {

        var playerSize = Player.Instance.GetSizeAtLevel(level);

        transform.position = startPosition;
        transform.eulerAngles = startEulerAngles;
        transform.localScale = Vector3.one * playerSize;

        var radianZ = transform.eulerAngles.z * Mathf.Deg2Rad;
        swimVelocity = new Vector3(Mathf.Cos(radianZ), Mathf.Sin(radianZ));

        //trail.time = 0.5f * Mathf.Pow(2, level);
        //trail.startWidth = 0.25f * Mathf.Pow(2, level);
        //trail.Clear();

        realSwimRange = swimRange * playerSize;
        realSwimSpeed = swimSpeed * playerSize;

        prevPosition = transform.position;
        nextPosition = transform.position;

        armatureComponent.sortingOrder = level;
    }

    public void Wander(float deltaTime)
    {         
        if (moveTime <= 0)
        {
            moveTime = 1;
            moveCurveTime = 0;
            moveDirection = (MoveDirection)UnityEngine.Random.Range(0, 3);
            realSwimSpeed = UnityEngine.Random.Range(0, 2) > 0 ? realSwimSpeed : -realSwimSpeed;

            prevPosition = transform.position;
            nextPosition = transform.position;
        }
        else
        {
            moveTime -= deltaTime;

            if (moveDirection == MoveDirection.Straight)
            {
                nextPosition.x += deltaTime * realSwimSpeed * swimCurveValue;
            }
            else if (moveDirection == MoveDirection.Up)
            {
                moveCurveTime += deltaTime;
                nextPosition.x += realSwimSpeed * swimCurveValue * deltaTime;
                nextPosition.y = prevPosition.y + moveCurve.Evaluate(moveCurveTime) * realSwimSpeed * swimCurveValue;
            }
            else if (moveDirection == MoveDirection.Down)
            {
                moveCurveTime += deltaTime;
                nextPosition.x += realSwimSpeed * swimCurveValue * deltaTime;
                nextPosition.y = prevPosition.y - moveCurve.Evaluate(moveCurveTime) * realSwimSpeed * swimCurveValue;
            }

            // make sure object move in range
            if ((nextPosition.x > startPosition.x + realSwimRange || nextPosition.x < startPosition.x - realSwimRange) ||
                (nextPosition.y > startPosition.y + realSwimRange || nextPosition.y < startPosition.y - realSwimRange))
            {
                realSwimSpeed = -realSwimSpeed;
                moveCurveTime = 0;
                Debug.Log("A");

                prevPosition = transform.position;
                nextPosition = transform.position;

                return;
            }

            transform.position = nextPosition;
        }
    }

    public void Resize()
    {
        var playerSize = Player.Instance.GetSizeAtLevel(level);

        transform.localScale = Vector3.one * playerSize;

        realSwimRange = swimRange * playerSize;
        realSwimSpeed = swimSpeed * playerSize;
    }
}

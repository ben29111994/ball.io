﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoSingleton<SpawnerManager>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Spawner originalSpawner;
    [SerializeField]
    private int preInitCount;
    #endregion

    #region Normal paramters
    private Queue<Spawner> inactiveSpawners;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        inactiveSpawners = new Queue<Spawner>();

        for (int i = 0; i < preInitCount; i++)
        {
            var s = Instantiate<Spawner>(originalSpawner, transform);
            s.Initialize();
            s.gameObject.SetActive(false);

            inactiveSpawners.Enqueue(s);
        }
    }

    public Spawner GenerateSpawner()
    {
        Spawner s = null;

        if(inactiveSpawners.Count >0)
        {
            s = inactiveSpawners.Dequeue();
        }
        else
        {
            s = Instantiate<Spawner>(originalSpawner, transform);
            s.Initialize();
        }

        s.Refresh();
        s.gameObject.SetActive(true);

        return s;
    }

    public void RecycleSpawner(Spawner s)
    {
        s.Release();
        s.gameObject.SetActive(false);

        inactiveSpawners.Enqueue(s);
    }

}

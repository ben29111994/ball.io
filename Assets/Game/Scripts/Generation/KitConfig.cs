﻿using UnityEngine;

[System.Serializable]
class KitConfig
{
    public const int TOTAL_LEVEL = 7;

    [EnumFlags]
    public EnemyLevel spawnLevels;

    [Range(0, 100)]
    public int[] spawnPercent = new int[TOTAL_LEVEL];

    public float[] spawnRadius = new float[TOTAL_LEVEL];

    public int GetTotalLevel()
    {
        return TOTAL_LEVEL;
    }

}
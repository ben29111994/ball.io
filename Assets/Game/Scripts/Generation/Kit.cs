﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kit : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Location location;
    [SerializeField]
    private Rect locationRect;
    [SerializeField]
    private int level;

    [SerializeField]
    private KitConfig config;
    [SerializeField]
    private List<Spawner> spawners;

    [SerializeField]
    private AnimationCurve difficultCurve;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    public Location Location
    {
        get
        {
            return location;
        }

        set
        {
            location = value;
        }
    }

    public Rect LocationRect
    {
        get
        {
            return locationRect;
        }

        set
        {
            locationRect = value;
        }
    }

    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }
    #endregion

    public void Initialize()
    {
        // modify percent to fit 100
        int levels = (int)config.spawnLevels;
        int totalLevel = config.GetTotalLevel();

        int totalPercent = 0;
        for (int j = 0; j < totalLevel; j++)
        {
            if ((levels & 1 << j) != 0)
            {
                totalPercent += this.config.spawnPercent[j];
            }
        }
        if (totalPercent != 100)
        {
            int bonus = (totalPercent <= 100) ? 1 : -1;
            for (int k = 0; k < totalLevel; k++)
            {
                if ((levels & 1 << k) != 0 && this.config.spawnPercent[k] > 1)
                {
                    totalPercent += bonus;
                    this.config.spawnPercent[k] += bonus;
                    if (totalPercent == 100)
                    {
                        break;
                    }
                }
            }
        }

        for (int i = 0; i < spawners.Count; i++)
        {
            var s = spawners[i];
            s.Initialize();

            int level = 0;
            float radius = 0.0f;
            level = RollSpawner(out radius);

            s.SpawnLevel = level;
            s.SpawnRadius = radius;
        }
    }

    public void Release()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            SpawnerManager.Instance.RecycleSpawner(spawners[i]);
        }

        spawners.Clear();
    }

    public void Refresh()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            SpawnerManager.Instance.RecycleSpawner(spawners[i]);
        }

        spawners.Clear();
    }

    public void UpdateStep(float deltaTime)
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            var s = spawners[i];
            if (CameraController.Instance.CheckContainsInVirtualCamera(s.transform.position))
            {
                if (s.CanSpawn)
                {
                    s.Active();
                }
            }
            else
            {
                s.Deactive();
            }
        }
    }

    public void SetLocationRaw(Location location, Vector2 centerPosition)
    {
        this.location = location;

        centerPosition -= locationRect.center;

        transform.position = centerPosition;

        GenerateSpawners();
    }

    public void SetLocationWithOffset(Location location, Location spawnedByLocation, Vector2 spawnPosition)
    {
        this.location = location;

        Vector2 position = spawnPosition + (location - spawnedByLocation) * locationRect.size;
        position -= locationRect.center;

        transform.position = position;

        GenerateSpawners();
    }

    public int RollSpawner(out float spawnRadius)
    {
        int levels = (int)config.spawnLevels;
        int totalLevel = config.GetTotalLevel();

        int randomRercent = UnityEngine.Random.Range(1, 101);
        int totalPercent = 0;
        for (int i = 0; i < totalLevel; i++)
        {
            if ((levels & 1 << i) != 0)
            {
                totalPercent += config.spawnPercent[i];
                if (randomRercent <= totalPercent)
                {
                    spawnRadius = config.spawnRadius[i];
                    return i;
                }
            }
        }

        spawnRadius = 0f;
        Debug.LogError("SPAWNER ROLL FAILED!");
        return 0;
    }

    public void DeactiveAllSpawners()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].CanSpawn = false;
        }
    }

    public void ActiveAllSpawners()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            spawners[i].CanSpawn = true;
        }
    }

    public void GenerateSpawners()
    {
        GenerateSpawners(Player.Instance.Level % Player.Instance.MajorTransformationIntervals);

        //// difficult = 0 1 2 3 4 5 4 3 2 1 0 1 2 3 4 5 ...
        //int difficult = (int)difficultCurve.Evaluate(Mathf.Max(Mathf.Abs(location.x), Mathf.Abs(location.y)));

        //int minimumSpawnerLevel = level * Player.Instance.MajorTransformationIntervals;
        //int maximumSpawnerLevel = minimumSpawnerLevel + difficult;

        //float percent = 100.0f / (maximumSpawnerLevel - minimumSpawnerLevel + 1);

        //int totalSpawner = 30 - 10 * level;
        //if (totalSpawner < 10)
        //{
        //    totalSpawner = 10;
        //}
        //for (int i = 0; i < totalSpawner; i++)
        //{
        //    float radianZ = UnityEngine.Random.Range(0, 360) * Mathf.Deg2Rad;
        //    float distance = UnityEngine.Random.Range(0.25f, 1.0f);
        //    Vector3 position = UnityEngine.Random.insideUnitCircle;
        //    position.x = Mathf.Cos(radianZ) * distance;
        //    position.y = Mathf.Sin(radianZ) * distance;


        //    position.x *= locationRect.size.x / 2;
        //    position.y *= locationRect.size.y / 2;

        //    position += transform.position;

        //    Spawner s = SpawnerManager.Instance.GenerateSpawner();
        //    s.transform.SetParent(transform);

        //    s.transform.position = position;
        //    s.transform.rotation = Quaternion.identity;
        //    s.transform.localScale = Vector3.one;

        //    s.SpawnLevel = UnityEngine.Random.Range(minimumSpawnerLevel, maximumSpawnerLevel + 1);

        //    spawners.Add(s);
        //}

    }

    public void GenerateSpawners(int difficult)
    {
        int minimumSpawnerLevel = (Player.Instance.Level / Player.Instance.MajorTransformationIntervals) * Player.Instance.MajorTransformationIntervals;
        int maximumSpawnerLevel = minimumSpawnerLevel + difficult;

        int fish1 = 0; //
        int fish2 = 0;
        int fish3 = 0;

        //if (Player.Instance.Level == 0) // small size
        {
            fish1 = Player.Instance.Level;
            fish2 = fish1 + 1;
            fish3 = fish1 + 2;
        }
        //else // medium size
        //{
        //    fish1 = Player.Instance.Level;
        //    fish2 = fish1 - 1;
        //    fish3 = fish1 + 1;
        //}
        //else if (difficult == 2) // lagre size
        //{
        //    fish1 = Player.Instance.Level;
        //    fish2 = fish1 - 1;
        //    fish3 = fish1 + 1;
        //}

        int numberSpawnerInKit = 6; // can be seen in camera

        for (int i = 0; i < numberSpawnerInKit; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                float radianZ = UnityEngine.Random.Range(0, 360) * Mathf.Deg2Rad;
                float distance = UnityEngine.Random.Range(0.5f, 1.0f);
                Vector3 position = UnityEngine.Random.insideUnitCircle;
                position.x = Mathf.Cos(radianZ) * distance;
                position.y = Mathf.Sin(radianZ) * distance;


                position.x *= locationRect.size.x / 2;
                position.y *= locationRect.size.y / 2;

                position += transform.position;

                Spawner s = SpawnerManager.Instance.GenerateSpawner();
                s.transform.SetParent(transform);

                s.transform.position = position;
                s.transform.rotation = Quaternion.identity;
                s.transform.localScale = Vector3.one;

                int level = -1;
                int randomValue = UnityEngine.Random.Range(0, 100);
                if (randomValue < 70)
                {
                    level = fish1;
                }
                else if (randomValue < 85)
                {
                    level = fish2;
                }
                else if (randomValue < 100)
                {
                    level = fish3;
                }

                s.SpawnLevel = level;

                spawners.Add(s);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color32(240, 248, 255, 255);
        Gizmos.DrawWireCube(transform.position + (Vector3)locationRect.center, locationRect.size);
        Gizmos.color = new Color32(50, 205, 50, 255);
        Gizmos.DrawWireCube(transform.position + (Vector3)locationRect.center, Vector3.one * 0.5f);

        Gizmos.color = Color.red;
        for (int i = 0; i < spawners.Count; i++)
        {
            Gizmos.DrawWireSphere(spawners[i].transform.position, 0.1f);
        }
    }

}

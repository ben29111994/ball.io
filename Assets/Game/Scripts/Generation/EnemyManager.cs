﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoSingleton<EnemyManager>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private List<Enemy> originalEnemies;

    [SerializeField]
    private SwordFish originalSwordFish;
    [SerializeField]
    private ShadowFish originalShadowFish;
    [SerializeField]
    private GoldenFish originalGoldenFish;
    [SerializeField]
    private ChasingFish originalChasingFish;

    [SerializeField]
    private List<Sprite> listShadowFishSprites;
    [SerializeField]
    private int preInitCount = 3;
    #endregion

    #region Normal paramters
    private Dictionary<int, List<Enemy>> enemyPoolByLevel;
    private Dictionary<int, List<Enemy>> enemyPrefabsByLevel;

    private List<Enemy> activeEnemies;
    private List<Enemy> fadedEnemies;

    private List<SwordFish> activeSwordFishes;
    private Queue<SwordFish> inactiveSwordFishes;

    private List<ShadowFish> activeShadowFishes;
    private Queue<ShadowFish> inactiveShadowFishes;

    private List<GoldenFish> activeGoldenFishes;
    private Queue<GoldenFish> inactiveGoldenFishes;

    private List<ChasingFish> activeChasingFishes;
    private Queue<ChasingFish> inactiveChasingFishes;

    private int maxEnemyLevel;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        enemyPoolByLevel = new Dictionary<int, List<Enemy>>();
        enemyPrefabsByLevel = new Dictionary<int, List<Enemy>>();

        for (int i = 0; i < originalEnemies.Count; i++)
        {
            var e = originalEnemies[i];

            if (!enemyPrefabsByLevel.ContainsKey(e.Level))
            {
                enemyPrefabsByLevel.Add(e.Level, new List<Enemy>());
            }
            enemyPrefabsByLevel[e.Level].Add(e);

            if (!enemyPoolByLevel.ContainsKey(e.Level))
            {
                enemyPoolByLevel.Add(e.Level, new List<Enemy>());
            }
            for (int j = 0; j < preInitCount; j++)
            {
                var newEnemy = Instantiate(e, transform);
                newEnemy.Initialize();
                newEnemy.gameObject.SetActive(false);
                enemyPoolByLevel[e.Level].Add(newEnemy);
            }

            if(e.Level >= maxEnemyLevel)
            {
                maxEnemyLevel = e.Level + 1;
            }
        }

        activeEnemies = new List<Enemy>();
        fadedEnemies = new List<Enemy>();

        activeSwordFishes = new List<SwordFish>();
        inactiveSwordFishes = new Queue<SwordFish>();

        activeShadowFishes = new List<ShadowFish>();
        inactiveShadowFishes = new Queue<ShadowFish>();

        activeGoldenFishes = new List<GoldenFish>();
        inactiveGoldenFishes = new Queue<GoldenFish>();

        activeChasingFishes = new List<ChasingFish>();
        inactiveChasingFishes = new Queue<ChasingFish>();
    }

    public void Release()
    {
        while (activeEnemies.Count > 0)
        {
            Recycle(activeEnemies[0]);
        }

        activeEnemies.Clear();

        while (activeSwordFishes.Count > 0)
        {
            RecycleSwordFish(activeSwordFishes[0]);
        }

        activeSwordFishes.Clear();

        while (activeShadowFishes.Count > 0)
        {
            RecycleShadowFish(activeShadowFishes[0]);
        }

        activeShadowFishes.Clear();

        while (activeGoldenFishes.Count > 0)
        {
            RecycleGoldenFish(activeGoldenFishes[0]);
        }

        activeShadowFishes.Clear();

        while(activeChasingFishes.Count > 0)
        {
            RecycleChasingFish(activeChasingFishes[0]);
        }

        activeChasingFishes.Clear();
    }

    public void Refresh()
    {
        while (activeEnemies.Count > 0)
        {
            Recycle(activeEnemies[0]);
        }

        activeEnemies.Clear();

        while (activeSwordFishes.Count > 0)
        {
            RecycleSwordFish(activeSwordFishes[0]);
        }

        activeSwordFishes.Clear();

        while (activeShadowFishes.Count > 0)
        {
            RecycleShadowFish(activeShadowFishes[0]);
        }

        activeShadowFishes.Clear();

        while (activeGoldenFishes.Count > 0)
        {
            RecycleGoldenFish(activeGoldenFishes[0]);
        }

        activeShadowFishes.Clear();

        while (activeChasingFishes.Count > 0)
        {
            RecycleChasingFish(activeChasingFishes[0]);
        }

        activeChasingFishes.Clear();
    }

    public void UpdateStep(float deltaTime)
    {
        for (int i = 0; i < activeEnemies.Count; i++)
        {
            var e = activeEnemies[i];
            e.UpdateStep(deltaTime);
        }

        for (int i = 0; i < activeSwordFishes.Count; i++)
        {
            var e = activeSwordFishes[i];
            e.UpdateStep(deltaTime);
        }

        for (int i = 0; i < activeShadowFishes.Count; i++)
        {
            var f = activeShadowFishes[i];
            f.UpdateStep(deltaTime);
        }

        for (int i = 0; i < activeGoldenFishes.Count; i++)
        {
            var f = activeGoldenFishes[i];
            f.UpdateStep(deltaTime);
        }

        for (int i = 0; i < activeChasingFishes.Count; i++)
        {
            var f = activeChasingFishes[i];
            f.UpdateStep(deltaTime);
        }
    }

    public Enemy Generate(int level)
    {
        var pool = enemyPoolByLevel[level % maxEnemyLevel];
        var prefabs = enemyPrefabsByLevel[level % maxEnemyLevel];

        Enemy e;

        if (pool.Count > 0)
        {
            e = pool[UnityEngine.Random.Range(0, pool.Count)];
            pool.Remove(e);
        }
        else
        {
            e = Instantiate(prefabs[UnityEngine.Random.Range(0, prefabs.Count)], transform);
            e.Initialize();
        }

        e.Refresh();
        e.gameObject.SetActive(true);

        activeEnemies.Add(e);

        return e;
    }

    public void Recycle(Enemy e)
    {
        var pool = enemyPoolByLevel[e.Level % maxEnemyLevel];
        pool.Add(e);

        e.Release();
        e.gameObject.SetActive(false);

        if (activeEnemies.Contains(e))
        {
            activeEnemies.Remove(e);
        }
    }

    public void FadedEnemiesUnderLevel(int level)
    {
        for (int i = 0; i < activeEnemies.Count; i++)
        {
            var e = activeEnemies[i];
            if (e.Level < level)
            {
                fadedEnemies.Add(e);
                Recycle(e);
                i--;
            }
        }
    }

    public void Faded()
    {

    }

    public SwordFish GenerateSwordFish()
    {
        SwordFish f;
        if (inactiveSwordFishes.Count > 0)
        {
            f = inactiveSwordFishes.Dequeue();
        }
        else
        {
            f = Instantiate<SwordFish>(originalSwordFish, transform);
            f.Initialize();
        }

        f.gameObject.SetActive(true);
        f.Refresh();

        activeSwordFishes.Add(f);

        return f;
    }

    public void RecycleSwordFish(SwordFish f)
    {
        f.gameObject.SetActive(false);
        f.Release();

        inactiveSwordFishes.Enqueue(f);
        activeSwordFishes.Remove(f);
    }

    public float GetBonusLifeTime(int level)
    {
        if (level < 3)
            return 3.0f;
        else if (level < 6)
            return 2.0f;
        else
            return 1.0f;
    }

    public int GetBonusScore(int level)
    {
        switch (level)
        {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 5;
            case 3:
                return 10;
            case 4:
                return 15;
            case 5:
                return 20;
            default:
                return (level - 3) * 10;
        }
    }

    public void ResizeAll()
    {
        for (int i = 0; i < activeEnemies.Count; i++)
        {
            activeEnemies[i].Resize();
        }
    }

    public ShadowFish GenerateShadowFish()
    {
        ShadowFish f;
        if (inactiveShadowFishes.Count > 0)
        {
            f = inactiveShadowFishes.Dequeue();
        }
        else
        {
            f = Instantiate<ShadowFish>(originalShadowFish, transform);
            f.Initialize();
        }

        f.RandomLevel();
        f.ChangeImage(listShadowFishSprites[UnityEngine.Random.Range(0, listShadowFishSprites.Count)]);

        f.gameObject.SetActive(true);
        f.Refresh();

        activeShadowFishes.Add(f);

        return f;
    }

    public void RecycleShadowFish(ShadowFish f)
    {
        f.gameObject.SetActive(false);
        f.Release();

        inactiveShadowFishes.Enqueue(f);
        activeShadowFishes.Remove(f);
    }

    public GoldenFish GenerateGoldenFish()
    {
        GoldenFish f;
        if (inactiveGoldenFishes.Count > 0)
        {
            f = inactiveGoldenFishes.Dequeue();
        }
        else
        {
            f = Instantiate<GoldenFish>(originalGoldenFish, transform);
            f.Initialize();
        }

        f.gameObject.SetActive(true);
        f.Refresh();

        activeGoldenFishes.Add(f);

        return f;
    }

    public void RecycleGoldenFish(GoldenFish f)
    {
        f.gameObject.SetActive(false);
        f.Release();

        inactiveGoldenFishes.Enqueue(f);
        activeGoldenFishes.Remove(f);
    }

    public ChasingFish GenerateChasingFish()
    {
        ChasingFish f;
        if (inactiveChasingFishes.Count > 0)
        {
            f = inactiveChasingFishes.Dequeue();
        }
        else
        {
            f = Instantiate<ChasingFish>(originalChasingFish, transform);
            f.Initialize();
        }

        f.gameObject.SetActive(true);
        f.Refresh();

        activeChasingFishes.Add(f);

        return f;
    }

    public void RecycleChasingFish(ChasingFish f)
    {
        f.gameObject.SetActive(false);
        f.Release();

        inactiveChasingFishes.Enqueue(f);
        activeChasingFishes.Remove(f);
    }

}

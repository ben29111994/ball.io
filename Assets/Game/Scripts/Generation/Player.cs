﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DragonBones;

public enum PlayerState
{
    Swim,
    Eat,

    TOTAL,
    IDLE
}


public class Player : MonoBehaviour
{
    #region Const parameters
    #endregion

    #region Editor paramters
    [Header("Status")]
    [SerializeField]
    private PlayerState state = PlayerState.IDLE;
    [SerializeField]
    private int level;
    [SerializeField]
    private int majorTransformationIntervals = 3;
    [SerializeField]
    private int maximumLevel = 6;
    [SerializeField]
    [Tooltip("Eat fish and earn more time.")]
    private float lifeTime;
    [SerializeField]
    [Tooltip("When life time reach level up time, player will up level")]
    private float levelUpTime = 60.0f;

    [Header("Handling XP")]
    [SerializeField]
    private float currentXP;
    [SerializeField]
    private float levelUpXP;
    [SerializeField]
    private float degradeXP;
    [SerializeField]
    private int reviveXP;

    [Header("Handling speed")]
    [SerializeField]
    private Vector3 velocity;
    [SerializeField]
    private float baseSpeed = 4.0f;
    [SerializeField]
    [Tooltip("Real speed = Base speed * player scale")]
    private float realSpeed = 0.0f;

    [Header("Handling input")]
    [SerializeField]
    private bool isTouch;
    [SerializeField]
    private Vector3 touchPosition;
    [SerializeField]
    private Vector3 deltaPosition;

    [Header("Dragon bones")]
    [SerializeField]
    private DragonBones.UnityArmatureComponent armatureComponent;
    [SerializeField]
    private string swimAnimation;
    [SerializeField]
    private string eatAnimation;
    [SerializeField]
    private ArmatureController armatureController;

    [Header("Handling scale")]
    [SerializeField]
    private float increaseSizeByLevel;
    [SerializeField]
    private float currentSize;
    [SerializeField]
    private float prevSize;
    [SerializeField]
    private float nextSize;
    #endregion

    #region Normal paramters
    private float velocityLength;

    private static Player instance;
    BoxCollider2D playerBoxCollider;
    private float delayRevive = 4.0f;
    #endregion

    #region Encapsulate
    public int Level
    {
        get
        {
            return level;
        }

        set
        {
            level = value;
        }
    }

    public int MajorTransformationIntervals
    {
        get
        {
            return majorTransformationIntervals;
        }

        set
        {
            majorTransformationIntervals = value;
        }
    }

    public static Player Instance
    {
        get
        {
            return instance;
        }

        set
        {
            instance = value;
        }
    }
    #endregion

    public void Initialize()
    {
        playerBoxCollider = GetComponent<BoxCollider2D>();
        instance = this;

        level = 0;

        lifeTime = levelUpTime / 2;

        velocity = Vector3.zero;
        realSpeed = baseSpeed;

        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        velocityLength = 0;

        armatureController.Initialize();
        armatureController.SetSortingOrder(level);

        //armatureComponent.AddEventListener(EventObject.COMPLETE, HandlerAnimationEvent);
        //armatureComponent.AddEventListener(EventObject.LOOP_COMPLETE, HandlerAnimationEvent);

        ChangeState(PlayerState.Swim);
    }

    public void Release()
    {
        armatureController.Release();
    }

    public void Refresh()
    {
        level = 0;

        lifeTime = levelUpTime / 2;

        velocity = Vector3.zero;
        realSpeed = baseSpeed;

        transform.localPosition = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        transform.localScale = Vector3.one;

        velocityLength = 0;

        armatureController.Refresh();
        armatureController.SetSortingOrder(level);

        currentSize = 1.0f;
        prevSize = nextSize = currentSize;

        RefreshXP();
    }

    public void UpdateStep(float deltaTime)
    {
        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == null)
            UpdateInput();

        transform.localPosition += velocity * Time.deltaTime;

        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360f;

        if (angleZ > 90 && angleZ < 270)
        {
            armatureController.SetFlipY(true);
        }
        else
        {
            armatureController.SetFlipY(false);
        }

        if (!isTouch && velocityLength != 0)
        {
            velocityLength -= Time.deltaTime * realSpeed;
            if (velocityLength < 0)
                velocityLength = 0;

            velocity = Vector3.ClampMagnitude(velocity, velocityLength);
        }

        //if (lifeTime <= 0)
        //{
        //    Dead();
        //}
        //else
        //{
        //    lifeTime -= deltaTime;
        //}

        if(currentXP <= 0)
        {
            Dead();
        }
        else
        {
            currentXP -= degradeXP * deltaTime;
        }

    }

    private void UpdateInput()
    {
        if (Input.GetMouseButtonDown(0) && !isTouch)
        {
            isTouch = true;
            touchPosition = Input.mousePosition;
            velocityLength = 0.0f;

            DirectionEffect.Instance.Active();
            return;
        }

        if (Input.GetMouseButtonUp(0) && isTouch)
        {
            isTouch = false;
            touchPosition = Vector3.zero;
            velocityLength = velocity.magnitude;

            DirectionEffect.Instance.Deactive();

            return;
        }

        if (Input.GetMouseButton(0) && isTouch)
        {
            Vector3 delta = Input.mousePosition - touchPosition;
            delta.x /= Screen.width * 0.1f;
            delta.y /= Screen.height * 0.1f;
            delta = Vector3.ClampMagnitude(delta, 1);

            velocity = delta * realSpeed;
            velocity.z = 0;

            float radian = Mathf.Atan2(delta.y, delta.x);
            DirectionEffect.Instance.SetRotation(delta, radian);

            transform.eulerAngles = new Vector3(0, 0, radian * Mathf.Rad2Deg);
        }
    }

    public void Eat(Enemy e)
    {
        if (level < e.Level)
        {
            Dead();
        }
        else
        {
            e.Recycle();
            ChangeState(PlayerState.Eat);

            //lifeTime += e.BonusLifeTime / Mathf.Pow(2, level - e.Level);
            int enemyBonusXP = XPSystem.Instance.GetEnemyBonusXP(e.Level + 1); // enemy level start at 0
            currentXP += enemyBonusXP;
            GameManager.Instance.IncreaseScore(enemyBonusXP);

            float radianZ = transform.eulerAngles.z * Mathf.Deg2Rad;
            Vector3 scoreEffectPosition = new Vector3(Mathf.Cos(radianZ) * 100, Mathf.Sin(radianZ) * 100);
            IncreaseScoreEffect.Instance.ActiveEffect(enemyBonusXP, scoreEffectPosition);
            SoundManager.Instance.PlaySFX(SFXType.EatFish);

            if (CheckLevelUp())
            {
                LevelUp();
            }

            //CameraController.Instance.Shake(0.1f, 0.03f);
			CameraController.Instance.Shake(0.2f, 0.08f);

            // tracking data
            DataManager.Instance.IncreaseTrackingData("FishEaten", 1);
            DataManager.Instance.IncreaseTrackingData("DailyFishEaten", 1);
            DataManager.Instance.IncreaseTrackingData("TotalFishEaten", 1);
        }
    }

    public void Dead()
    {
        SoundManager.Instance.PlaySFX(SFXType.Die);
        GameManager.Instance.EndGame();
    }

    public bool CheckLevelUp()
    {
        //if (lifeTime >= levelUpTime)
        //    return true;
        if (currentXP >= levelUpXP)
            return true;
        return false;
    }

    public void LevelUp()
    {
        var effect = ParticleEffect.Instance.GenerateParticle(ParticleType.LevelUp);
        //effect.gameObject.transform.position = transform.position;

        level++;
        //if(level > maximumLevel)
        //{
        //    level = maximumLevel;
        //}

        lifeTime = levelUpTime / 2;

        //var currentIncreaseSize = 1 + CameraController.Instance.IncreaseSizeByLevel * level;
        //realSpeed = baseSpeed * Mathf.Pow(2, level) / currentIncreaseSize;

        //transform.DOScale(Vector3.one * Mathf.Pow(2, level), 1.0f).Play();
        //CameraController.Instance.ScaleUp(level);

        //if (level % majorTransformationIntervals == 0)
        //{
        //    //EnemyManager.Instance.FadedEnemiesUnderLevel(level);
        //    KitManager.Instance.SpawnLeveledUpKits();
        //    GameManager.Instance.ChangeBackground();
        //    armatureController.UpgradeArmature();
        //}

        currentSize += increaseSizeByLevel;

        bool isMajorLevel = level % majorTransformationIntervals == 0;

        if (isMajorLevel)
        {
            prevSize = (currentSize - increaseSizeByLevel) / 2.0f;
            nextSize = currentSize;
            transform.localScale = Vector3.one * prevSize;

            EnemyManager.Instance.ResizeAll();
            //KitManager.Instance.SpawnLeveledUpKits();
            GameManager.Instance.ChangeBackground();
            armatureController.UpgradeArmature();

            CameraController.Instance.Shake(1.0f, 0.075f);
        }
        else
        {
            prevSize = nextSize;
            nextSize = currentSize * Mathf.Pow(2, level % majorTransformationIntervals);

            CameraController.Instance.Shake(0.5f, 0.025f);
        }

        transform.DOScale(nextSize, 1.0f).OnStart(delegate
        {
            transform.localScale = Vector3.one * prevSize;
        })
        .Play();
        CameraController.Instance.ScaleUp(isMajorLevel);
        SoundManager.Instance.PlaySFX(SFXType.LevelUp);
        KitManager.Instance.SpawnLeveledUpKits();

        realSpeed = baseSpeed * Mathf.Pow(2, level % majorTransformationIntervals);

        RefreshXP();

        armatureController.SetSortingOrder(level);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Enemy e = collision.gameObject.GetComponent<Enemy>();
            Eat(e);
        }
    }

    public float GetLevelUpPercent()
    {
        return lifeTime / levelUpTime;
    }

    public float GetRealSpeed()
    {
        return realSpeed;
    }

    public void ChangeState(PlayerState nextState)
    {
        if (state == nextState)
            return;

        state = nextState;

        //if (armatureComponent == null)
        //    return;

        switch (state)
        {
            case PlayerState.Swim:
                armatureController.Swim();
                break;
            case PlayerState.Eat:
                armatureController.Eat();
                break;
            default:
                break;
        }
    }

    public void HandlerAnimationEvent(string type, EventObject eventObject)
    {
        switch (type)
        {
            case EventObject.COMPLETE:
                if (state == PlayerState.Eat)
                {
                    ChangeState(PlayerState.Swim);
                }
                break;
            case EventObject.LOOP_COMPLETE:
                if (state == PlayerState.Eat)
                {
                    ChangeState(PlayerState.Swim);
                }
                break;
            default:
                break;
        }
    }

    public float GetSizeAtLevel(int level)
    {
        return (1.0f + increaseSizeByLevel * level) * Mathf.Pow(2, level) / Mathf.Pow(8, this.level / majorTransformationIntervals);
    }

    public void RefreshXP()
    {
        // player level start at 0
        int nextLevel = level + 1;

        currentXP = 0;
        levelUpXP = XPSystem.Instance.GetPlayerLevelUpXP(nextLevel);
        degradeXP = XPSystem.Instance.GetPlayerDegradeXP(nextLevel);
        reviveXP = XPSystem.Instance.GetPlayerReviveXP(nextLevel);

        currentXP = levelUpXP / 2;
    }

    public float GetXPPercent()
    {
        return currentXP / levelUpXP;
    }

    public int GetReviveXP()
    {
        return reviveXP;
    }

    public void Revive()
    {
        RefreshXP();

        velocityLength = 0;

        isTouch = false;
        touchPosition = Vector3.zero;
        deltaPosition = Vector3.zero;

        velocity = Vector3.zero;
    }

    public void ResetRotation()
    {
        transform.localEulerAngles = Vector3.zero;
        var angleZ = transform.localEulerAngles.z;
        if (angleZ < 0)
            angleZ += 360f;

        if (angleZ > 90 && angleZ < 270)
        {
            armatureController.SetFlipY(true);
        }
        else
        {
            armatureController.SetFlipY(false);
        }

        armatureController.SetSortingOrder(9999);
    }

    public void EatGoldenFish(int level)
    {
        ChangeState(PlayerState.Eat);

        int enemyBonusXP = XPSystem.Instance.GetEnemyBonusXP(level + 1) * 4; // enemy level start at 0
        //currentXP += enemyBonusXP;
        GameManager.Instance.IncreaseScore(enemyBonusXP);

        float radianZ = transform.eulerAngles.z * Mathf.Deg2Rad;
        Vector3 scoreEffectPosition = new Vector3(Mathf.Cos(radianZ) * 100, Mathf.Sin(radianZ) * 100);
        IncreaseScoreEffect.Instance.ActiveEffect(enemyBonusXP, scoreEffectPosition);
        SoundManager.Instance.PlaySFX(SFXType.EatFish);
    }

    public void EatChasingFish(int level)
    {
        if (this.level < level)
            Dead();
    }

    public void ReenableCollison(){
        StartCoroutine(C_ReenableCollison(delayRevive));
    }
    public IEnumerator C_ReenableCollison(float delayTime)
    {
        var shieldGO = Instantiate(Resources.Load("Shield"), gameObject.transform.localPosition, Quaternion.identity, gameObject.transform) as GameObject;
        playerBoxCollider.enabled = false;
        yield return new WaitForSeconds(delayTime);
        playerBoxCollider.enabled = true;
        Destroy(shieldGO.gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Location
{
    public int x;
    public int y;

    public Location(int a_x, int a_y)
    {
        this.x = a_x;
        this.y = a_y;
    }

    public static Location operator -(Location lhs, Location rhs)
    {
        return new Location(lhs.x - rhs.x, lhs.y - rhs.y);
    }

    public static Vector2 operator *(Location lhs, Vector2 rhs)
    {
        return new Vector2((float)lhs.x * rhs.x, (float)lhs.y * rhs.y);
    }

    public override string ToString()
    {
        return "(" + x + " , " + y + ")";
    }
}

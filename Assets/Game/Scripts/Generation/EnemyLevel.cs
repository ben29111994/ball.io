﻿using System;

[Flags]
public enum EnemyLevel
{
    Zero = 1,
    One = 2,
    Two = 4,
    Three = 8,
    Four = 16,
    Five = 32,
    Six = 64,
    //Seven = 128,
    //Eight = 256,
    //Nine = 1024,
    //Ten = 2048,
    //Eleven = 4096,
    //Twleve = 8192,
    //Thirteen = 16384,
    //Fourteen = 32768,
    //Fifteen = 65536,
}
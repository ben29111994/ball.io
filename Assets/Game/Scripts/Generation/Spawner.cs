﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private bool canSpawn;
    [SerializeField]
    private int spawnLevel;
    [SerializeField]
    private float spawnRadius;
    #endregion

    #region Normal paramters
    #endregion

    #region Encapsulate
    public bool CanSpawn
    {
        get
        {
            return canSpawn;
        }

        set
        {
            canSpawn = value;
        }
    }

    public int SpawnLevel
    {
        get
        {
            return spawnLevel;
        }

        set
        {
            spawnLevel = value;
        }
    }

    public float SpawnRadius
    {
        get
        {
            return spawnRadius;
        }

        set
        {
            spawnRadius = value;
        }
    }
    #endregion

    public void Initialize()
    {
        canSpawn = true;
    }

    public void Release()
    {
        canSpawn = true;
    }

    public void Refresh()
    {
        canSpawn = true;
    }

    public void Active()
    {
        if(canSpawn)
        {
            canSpawn = false;
            var enemy = EnemyManager.Instance.Generate(spawnLevel);
            enemy.StartPosition = transform.position;
            enemy.StartEulerAngles = new Vector3(0, 0, UnityEngine.Random.Range(0, 360));
            enemy.Level = spawnLevel;
            enemy.BonusLifeTime = EnemyManager.Instance.GetBonusLifeTime(spawnLevel);
            enemy.BonusScore = EnemyManager.Instance.GetBonusScore(spawnLevel);

            enemy.ApplyChanges();
        }
    }

    public void Deactive()
    {
        canSpawn = true;
    }

}

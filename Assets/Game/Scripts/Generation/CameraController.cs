﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoSingleton<CameraController>
{

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private Rect cameraRect;
    [SerializeField]
    private Rect cameraVirtualRect;
    [SerializeField]
    private float increaseSizeByLevel;
    [SerializeField]
    private float targetOrthographicSize = 5;

    [Header("Handling scale")]
    [SerializeField]
    private float originalSize = 5.0f;
    [SerializeField]
    private float currentSize;
    [SerializeField]
    private float prevSize;
    [SerializeField]
    private float nextSize;

    [Header("Handling shake")]
    [SerializeField]
    private float shakeTime;
    [SerializeField]
    private float shakeMagnitude;
    #endregion

    #region Normal paramters
    private float currentIncreaseSize;
    #endregion

    #region Encapsulate
    public float TargetOrthographicSize
    {
        get
        {
            return targetOrthographicSize;
        }

        set
        {
            targetOrthographicSize = value;
        }
    }

    public float IncreaseSizeByLevel
    {
        get
        {
            return increaseSizeByLevel;
        }

        set
        {
            increaseSizeByLevel = value;
        }
    }
    #endregion

    public override void Initialize()
    {

        float height = Camera.main.orthographicSize;
        float width = Screen.width * height / Screen.height;

        cameraRect.width = width * 2;
        cameraRect.height = height * 2;
        cameraRect.center = transform.position;

        cameraVirtualRect.size = cameraRect.size * 2.0f;
        cameraVirtualRect.center = cameraRect.center;

    }

    public void Release()
    {
    }

    public void Refresh()
    {
        mainCamera.orthographicSize = 4.8f;
        targetOrthographicSize = 4.8f;

        currentSize = originalSize;
        prevSize = nextSize = currentSize;

        var position = mainCamera.transform.position;
        position.x = 0;
        position.y = 0;

        mainCamera.transform.position = position;
    }

    public void UpdateStep(float deltaTime)
    {
        float height = Camera.main.orthographicSize;
        float width = Screen.width * height / Screen.height;

        cameraRect.width = width * 2;
        cameraRect.height = height * 2;
        cameraRect.center = transform.position;

        cameraVirtualRect.size = cameraRect.size * 2.0f;
        cameraVirtualRect.center = cameraRect.center;

        transform.position = Player.Instance.transform.position + new Vector3(0, 0, transform.position.z);

        if (shakeTime > 0f)
        {
            shakeTime -= deltaTime;

            Vector3 offset = UnityEngine.Random.insideUnitCircle;
            offset *= this.shakeMagnitude;

            Vector3 localPosition = transform.position + offset;
            localPosition.z = transform.position.z;

            transform.localPosition = localPosition;
        }
    }

    public bool CheckContainsInRealCamera(Vector3 position)
    {
        return cameraRect.Contains(position);
    }

    public bool CheckContainsInVirtualCamera(Vector3 position)
    {
        return cameraVirtualRect.Contains(position);
    }

    public bool CheckOverlaps(Rect rect)
    {
        return cameraRect.Overlaps(rect);
    }

    public void ScaleUp(int level)
    {
        currentIncreaseSize = 1 + increaseSizeByLevel * level;
        targetOrthographicSize = 4.8f * Mathf.Pow(2, level) / currentIncreaseSize;
        mainCamera.DOOrthoSize(targetOrthographicSize, 1.0f).Play();
    }

    public void ScaleUp(bool isMajorLevelUp)
    {
        currentSize *= 2;

        if (isMajorLevelUp)
        {
            currentSize = originalSize;

            prevSize = currentSize / 2.0f;
            nextSize = currentSize;

            //mainCamera.orthographicSize = prevSize;
        }
        else
        {
            prevSize = nextSize;
            nextSize = currentSize;
        }

		mainCamera.orthographicSize = prevSize;
        mainCamera.DOOrthoSize(nextSize, 1.0f).Play();
        targetOrthographicSize = nextSize;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color32(240, 248, 255, 255);
        Gizmos.DrawWireCube((Vector3)this.cameraRect.center, this.cameraRect.size);
        Gizmos.color = new Color32(50, 205, 50, 255);
        Gizmos.DrawWireCube(cameraVirtualRect.center, cameraVirtualRect.size);
    }

    public void Shake(float time, float magnitude)
    {
        if (shakeTime < time)
        {
            shakeTime = time;
        }
        if (shakeMagnitude < magnitude)
        {
            shakeMagnitude = magnitude;
        }
    }

    public void ZoomInPosition(Vector3 position)
    {
        var newPos = transform.position;
        newPos.x = position.x;
        newPos.y = position.y;

        mainCamera.transform.DOMove(newPos, 0.5f).Play();
        mainCamera.DOOrthoSize(mainCamera.orthographicSize / 2, 0.5f).Play();
    }

    public void ZoomOutPosition(Vector3 position)
    {
        var newPos = transform.position;
        newPos.x = position.x;
        newPos.y = position.y;

        mainCamera.transform.DOMove(newPos, 0.5f).Play();
        mainCamera.DOOrthoSize(mainCamera.orthographicSize * 2, 0.5f).Play();
    }

    public void LookPosition(Vector3 position)
    {
        var newPos = transform.position;
        newPos.x = position.x;
        newPos.y = position.y;

        mainCamera.transform.DOMove(newPos, 0.5f).Play();
    }
}

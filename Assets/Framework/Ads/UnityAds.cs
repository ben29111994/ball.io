﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class UnityAds : Ads
{

#if UNITY_ADS

    #region Const parameters
    #endregion

    #region Editor paramters
    [SerializeField]
    private string appStoreID;
    [SerializeField]
    private string playStoreID;

    #endregion

    #region Normal paramters
    public string AppStoreID
    {
        get
        {
            return appStoreID;
        }

        set
        {
            appStoreID = value;
        }
    }

    public string PlayStoreID
    {
        get
        {
            return playStoreID;
        }

        set
        {
            playStoreID = value;
        }
    }
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
#if UNITY_ANDROID
        Advertisement.Initialize(playStoreID);
#elif UNITY_IOS
        Advertisement.Initialize(appStoreID);        
#endif
    }

    public override void Release()
    {

    }

    public override bool IsInterstitialAdReady()
    {
        return Advertisement.IsReady("video");
    }

    public override void ShowInterstitialAd()
    {
        if (IsInterstitialAdReady())
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = ShowInterstitialAdCallback;

            Advertisement.Show("video", options);
        }
    }

    public override bool IsRewardVideoAdReady()
    {
        return Advertisement.IsReady("rewardedVideo");
    }

    public override void ShowRewardVideoAd()
    {
        if (IsRewardVideoAdReady())
        {
            ShowOptions options = new ShowOptions();
            options.resultCallback = ShowRewardVideoAdCallback;

            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void ShowInterstitialAdCallback(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video finished - Offer a reward to the player");
            onInterstitialCompleted(InterstitialResult.Finished);
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");
            onInterstitialCompleted(InterstitialResult.Skipped);
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
            onInterstitialCompleted(InterstitialResult.Failed);
        }
    }

    private void ShowRewardVideoAdCallback(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video finished - Offer a reward to the player");
            onRewardVideoCompleted(RewardVideoResult.Finished);
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");
            onRewardVideoCompleted(RewardVideoResult.Skipped);
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
            onRewardVideoCompleted(RewardVideoResult.Failed);
        }
    }

#endif

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InterstitialResult
{
    Failed,
    Skipped,
    Finished,
}

public enum RewardVideoResult
{
    Failed,
    Skipped,
    Finished,
}

public class Ads : MonoBehaviour
{
    public System.Action<InterstitialResult> onInterstitialCompleted;

    public System.Action<RewardVideoResult> onRewardVideoCompleted;

    public virtual void Initialize()
    {        
    }

    public virtual void Release()
    {

    }

    public virtual bool IsBannerAdReady()
    {
        return false;
    }

    public virtual void ShowBannerAd()
    {

    }

    public virtual void HideBannerAd()
    {

    }

    public virtual void DestroyBannerAd()
    {

    }

    public virtual bool IsInterstitialAdReady()
    {
        return false;
    }

    public virtual void ShowInterstitialAd()
    {

    }

    public virtual bool IsRewardVideoAdReady()
    {
        return false;
    }

    public virtual void ShowRewardVideoAd()
    {

    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DelayMode
{
    RealTime,
    DateTime,

    TOTAL,
    IDLE
}

public enum AdsMode
{
    UnityAds,
    MopubAds,

    TOTAL,
    IDLE
}

[System.Serializable]
public class MopubAdIds
{
    public string appStoreID;
    public string playStoreID;
}

public enum RewardVideoType
{
    Money,
    Revive,
    Unlock,
    Hatch,

    TOTAL,
    IDLE,
}


public class AdsManager : MonoSingleton<AdsManager>
{

    #region Const parameters
    public const string ADS_SYMBOL = "ENABLE_ADS";
    public const string UNITY_ADS_SYMBOL = "UNITY_ADS";
    public const string MOPUB_ADS_SYMBOL = "MOPUB_ADS";
    public const string MICROSOFT_ADS_SYMBOL = "MICROSOFT_ADS";
    #endregion

    #region Editor paramters
    [Header("Symbol for quick access")]
    [SerializeField]
    private string adsSymbol = ADS_SYMBOL;
    [SerializeField]
    private string unityAdsSymbol = UNITY_ADS_SYMBOL;
    [SerializeField]
    private string mopubAdsSymbol = MOPUB_ADS_SYMBOL;

    [Header("Delay Config")]
    [SerializeField]
    private bool enableDelayBetweenAds = false;
    [SerializeField]
    private float secondsBetweenAds = 120;
    [SerializeField]
    private DelayMode delayMode = DelayMode.RealTime;


    [Header("Ads Config")]
    [SerializeField]
    private AdsMode adsMode = AdsMode.UnityAds;
    [SerializeField]
    private string appStoreID = "";
    [SerializeField]
    private string playStoreID = "";
    [SerializeField]
    private MopubAdIds bannerAdIds;
    [SerializeField]
    private MopubAdIds interstitialAdIds;
    [SerializeField]
    private MopubAdIds rewardVideoAdIds;
    #endregion

    #region Normal paramters
    private Ads ads;

    private float lastTime;
    private System.DateTime lastDateTime;

    private Queue<RewardVideoType> rewardVideoTypeQueue;

    public event System.Action<InterstitialResult> OnInterstitialResult;
    public event System.Action<RewardVideoResult> OnRewardVideoResult;

    public event System.Action<RewardVideoType, RewardVideoResult> OnRewardVideoCallback;
    #endregion

    #region Encapsulate
    #endregion

    public override void Initialize()
    {
        if (adsMode == AdsMode.UnityAds)
        {
#if UNITY_ADS
            var unityAds = gameObject.AddComponent<UnityAds>();
            unityAds.AppStoreID = appStoreID;
            unityAds.PlayStoreID = playStoreID;

            unityAds.name = "Unity Ads";

            ads = unityAds; 
#endif
        }
        else if (adsMode == AdsMode.MopubAds)
        {
#if MOPUB_ADS
            var mopubAds = gameObject.AddComponent<MopubAds>();
#if UNITY_ANDROID
            mopubAds.BannerAdUnitId = bannerAdIds.playStoreID;
            mopubAds.InterstitialAdUnitId = interstitialAdIds.playStoreID;
            mopubAds.RewardedVideoAdUnitId = rewardVideoAdIds.playStoreID;
#elif UNITY_IOS
            mopubAds.BannerAdUnitId = bannerAdIds.appStoreID;
            mopubAds.InterstitialAdUnitId = interstitialAdIds.appStoreID;
            mopubAds.RewardedVideoAdUnitId = rewardVideoAdIds.appStoreID;
#endif
            mopubAds.name = "Mopub Ads";

            ads = mopubAds; 
#endif
        }

        ads.Initialize();
        ads.onInterstitialCompleted += OnInterstitialCompleted;
        ads.onRewardVideoCompleted += OnRewardVideoCompleted;

        lastTime -= secondsBetweenAds;
        lastDateTime = System.DateTime.Now.AddMilliseconds(-secondsBetweenAds);

        rewardVideoTypeQueue = new Queue<RewardVideoType>();
    }

    public void Release()
    {
        ads.Release();
    }

    public bool IsBannerAdReady()
    {
        return false;
    }

    public void ShowBannerAd()
    {
        ads.ShowBannerAd();
    }

    public void HideBannerAd()
    {
        ads.HideBannerAd();
    }

    public void DestroyBannerAd()
    {
        ads.DestroyBannerAd();
    }

    public bool IsInterstitialAdReady()
    {
        return ads.IsInterstitialAdReady();
    }

    public void ShowInterstitialAd()
    {
        if (enableDelayBetweenAds)
        {
            if (CanShowAds())
            {
                ads.ShowInterstitialAd();
            }
        }
        else
        {
            ads.ShowInterstitialAd();
        }
    }

    public bool IsRewardVideoAdReady()
    {
        return ads.IsRewardVideoAdReady();
    }

    //public void ShowRewardVideoAd()
    //{
    //    ads.ShowRewardVideoAd();
    //}

    private bool CanShowAds()
    {
        if (delayMode == DelayMode.RealTime)
        {
            if (Time.realtimeSinceStartup - lastTime >= secondsBetweenAds)
            {
                lastTime = Time.realtimeSinceStartup;
                return true;
            }
        }
        else if (delayMode == DelayMode.DateTime)
        {
            var span = System.DateTime.Now - lastDateTime;

            if (span.TotalSeconds >= secondsBetweenAds)
            {
                lastDateTime = System.DateTime.Now;
                return true;
            }
        }

        return false;
    }

    private void OnInterstitialCompleted(InterstitialResult result)
    {
        if(OnInterstitialResult != null)
            OnInterstitialResult(result);
    }

    private void OnRewardVideoCompleted(RewardVideoResult result)
    {
        if (OnRewardVideoResult != null)
            OnRewardVideoResult(result);

        var type = rewardVideoTypeQueue.Dequeue();

        if(OnRewardVideoCallback != null)
            OnRewardVideoCallback(type, result);
    }

    public void ShowRewardVideoAd(RewardVideoType type)
    {
        if(ads.IsRewardVideoAdReady())
        {
            ads.ShowRewardVideoAd();

            rewardVideoTypeQueue.Enqueue(type);
        }
    }

}
